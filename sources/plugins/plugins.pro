MODULES = widgets bluetooth serialport
for(MODULE, MODULES) : !qtHaveModule($${MODULE}): error("Qt-Module $${MODULE} not found, please install and try again!")

unix:!macx {
    !packagesExist(libudev)						: error("Package libudev not found, please install and try again!")

    contains(DEFINES, EXTHIDAPI) {
		!packagesExist(hidapi-hidraw)			: error("Package libhidapi not found, please install and try again!")
    }
}

TEMPLATE = subdirs
SUBDIRS  = vendor/beurer vendor/generic vendor/hartmann vendor/omron

TRANSLATIONS = shared/plugin/lng/plugins_de.ts shared/plugin/lng/plugins_es.ts shared/plugin/lng/plugins_fr.ts shared/plugin/lng/plugins_hu.ts shared/plugin/lng/plugins_it.ts shared/plugin/lng/plugins_nb_NO.ts shared/plugin/lng/plugins_nl.ts shared/plugin/lng/plugins_pl.ts

appbundle.target = appbundle
flatpak.target   = flatpak
snap.target      = snap
msix.target      = msix

QMAKE_EXTRA_TARGETS += appbundle flatpak snap msix
