<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>DialogImport</name>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="14"/>
        <source>Device Import</source>
        <translation>Importation depuis l&apos;Appareil</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="84"/>
        <source>Plugin Options</source>
        <translation>Options du Plugin</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="110"/>
        <source>Import Measurements automatically</source>
        <translation>Importer Automatiquement les Mesures</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="375"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="120"/>
        <source>Device Information</source>
        <translation>Information sur l&apos;Appareil</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="14"/>
        <source>Generic Bluetooth</source>
        <translation>Bluetooth générique</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="36"/>
        <source>Automatically discover devices on startup</source>
        <translation>Découverte automatique des appareils au démarrage</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="39"/>
        <source>Auto Discover</source>
        <translation>Découverte automatique</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="74"/>
        <source>Automatically connect to device on startup</source>
        <translation>Connexion automatique à l&apos;appareil au démarrage</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="77"/>
        <source>Auto Connect</source>
        <translation>Connexion automatique</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="106"/>
        <source>Automatically import measurements on startup</source>
        <translation>Importation automatique des mesures au démarrage</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="109"/>
        <source>Auto Import</source>
        <translation>Importation automatique</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="137"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="24"/>
        <source>Bluetooth Controller</source>
        <translation>Contrôleur Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="143"/>
        <source>Select Bluetooth controller</source>
        <translation>Sélectionner le contrôleur Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="161"/>
        <source>Setup discovery timeout</source>
        <translation>Configurer le délai de découverte</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="207"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="37"/>
        <source>Discover Device</source>
        <translation>Découvrir l&apos;appareil</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="259"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="51"/>
        <source>Bluetooth Device</source>
        <translation>Périphérique Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="268"/>
        <source>Select Bluetooth device</source>
        <translation>Sélectionner un appareil Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="283"/>
        <source>Enter devicename for auto connection</source>
        <translation>Entrer le nom de l&apos;appareil pour la connexion automatique</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="296"/>
        <source>Copy devicename</source>
        <translation>Copier le nom de l&apos;appareil</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="321"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="70"/>
        <source>Connect Device</source>
        <translation>Connecter l&apos;appareil</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="384"/>
        <source>Current State of the Device: Unconnected</source>
        <translation>État actuel de l&apos;appareil&#x202f;: Non connecté</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="435"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="69"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="69"/>
        <source>Serial</source>
        <translation>Port Série</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="463"/>
        <source>Manufacturer</source>
        <translation>Fabricant</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="477"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="542"/>
        <source>Hardware</source>
        <translation>Matériel</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="549"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="149"/>
        <source>Firmware</source>
        <translation>Micrologiciel</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="556"/>
        <source>Software</source>
        <translation>Logiciel</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="630"/>
        <source>Device Measurements</source>
        <translation>Mesures de l&apos;appareil</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="654"/>
        <source>ID for User 1</source>
        <translation>ID de l&apos;utilisateur 1</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="702"/>
        <source>ID for User 2</source>
        <translation>ID de l&apos;utilisateur 2</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="790"/>
        <source>enable/disable Logfile</source>
        <translation>activer/désactiver le fichier journal</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="62"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="62"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="76"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="76"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="156"/>
        <source>Producer</source>
        <translation>Fabricant</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="83"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="83"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="163"/>
        <source>Product</source>
        <translation>Produit</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="55"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="86"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="55"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="86"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="86"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="86"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="158"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="182"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="111"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="135"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="770"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="188"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="135"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="173"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="132"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="262"/>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="175"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="199"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="128"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="152"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="205"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="152"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="190"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="149"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="279"/>
        <source>Write Logfile</source>
        <translation>Écrire un Fichier de Log</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="202"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="226"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="155"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="179"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="817"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="232"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="179"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="217"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="176"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="329"/>
        <source>Cancel</source>
        <translation>Annulé</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="24"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="24"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="24"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="24"/>
        <source>Could not find a serial port.

Is the usb2serial driver installed and the device connected?</source>
        <translation>Ne peu pas trouver le port serie.

Le pilote usb2serial est-il installé et l&apos;appareil est-il connecté ?</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="76"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="76"/>
        <source>Could not read data.

%1</source>
        <translation>Impossible de lire les données.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="85"/>
        <source>The device doesn&apos;t respond.</source>
        <translation>L&apos;appareil ne répond pas.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="316"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="270"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="318"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="222"/>
        <source>Could not open serial port &quot;%1&quot;.

%2

Try as root or create a udev rule.

Read the wiki for details on how to do this.</source>
        <translation>Impossible d&apos;ouvrir le port série &quot;%1&quot;.

%2

Essayez en tant que root ou créez une règle udev.

Lisez le wiki pour plus de détails sur la façon de procéder.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="212"/>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="253"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="244"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="286"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="203"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="240"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="417"/>
        <source>The import was canceled.</source>
        <translation>L&apos;importation a été annulée.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="102"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="102"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="75"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="159"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="112"/>
        <source>Could not write data.

%1</source>
        <translation>Impossible d&apos;écrire des données.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="85"/>
        <source>The device doesn&apos;t respond.

Press &quot;MEM&quot;, select U1/U2 with &quot;POWER&quot; and wait until the user data is shown.

Then try again…</source>
        <translation>L&apos;appareil ne répond pas.

Appuyez sur &quot;MEM&quot;, sélectionnez U1/U2 avec &quot;POWER&quot; et attendez que les données de l&apos;utilisateur soient affichées.

Alors essayez à nouveau…</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="115"/>
        <source>Was &quot;U1&quot; selected on the device?

For &quot;U2&quot; answer with No.</source>
        <translation>L&apos; &quot;U1&quot; a-t-elle été sélectionnée sur l&apos;appareil ?

Pour &quot;U2&quot;, répondre par Non.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="180"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="200"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="173"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="198"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="796"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="252"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="187"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="279"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="177"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="688"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="816"/>
        <source>Could not open the logfile %1.

%2</source>
        <translation>Impossible d&apos;ouvrir le fichier journal %1.

%2</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="38"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="38"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="45"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="40"/>
        <source>Could not open usb device %1:%2.

Try as root or create a udev rule.

Read the wiki for details on how to do this.</source>
        <translation>Impossible d&apos;ouvrir le périphérique usb %1:%2.

Essayez en tant que root ou créez une règle udev.

Lisez le wiki pour plus de détails sur la façon de procéder.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="190"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="183"/>
        <source>The device doesn&apos;t respond.

Disconnect the USB cable and reconnect it.

Then try again…</source>
        <translation>L&apos;appareil ne répond pas.

Débranchez le câble USB et rebranchez-le.

Essayez à nouveau…</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="201"/>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="242"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="192"/>
        <source>Could not read measurement count.</source>
        <translation>Impossible de lire le nombre de mesures.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="223"/>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="264"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="214"/>
        <source>Could not read measurement %1.</source>
        <translation>Impossible de lire la mesure %1.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="286"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="330"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="236"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="284"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="817"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="839"/>
        <source>Cancel import?</source>
        <translation>Annuler import ?</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="297"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="341"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="247"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="295"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="847"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="343"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="247"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="488"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="244"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="850"/>
        <source>Import in progress…</source>
        <translation>Importation des données en progression…</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="286"/>
        <source>Can&apos;t set device into info mode.</source>
        <translation>Impossible de mettre l&apos;appareil en mode info.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="299"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="313"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="338"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="355"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="372"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="409"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="448"/>
        <source>Send %1 command failed.</source>
        <translation>L&apos;envoi de la commande %1 a échoué.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="325"/>
        <source>Can&apos;t set device into data mode.</source>
        <translation>Impossible de mettre l&apos;appareil en mode donnés.</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="117"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="70"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="393"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="435"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="202"/>
        <source>Import aborted by user.</source>
        <translation>importation interrompue par l&apos;utilisateur.</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="152"/>
        <source>Acknowledge failed.</source>
        <translation>Échec de l&apos;acquittement.</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="84"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="100"/>
        <source>Could not read data.

Is the device powered on?

%1</source>
        <translation>Impossible de lire les données.

L&apos;appareil est-il sous tension ?

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="163"/>
        <source>Device doesn&apos;t respond, try again?</source>
        <translation>L&apos;Appareil ne répond pas, réessayez ?</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="187"/>
        <source>Press START/STOP on device and try again…</source>
        <translation>Appuyez sur la touche START/STOP de l&apos;appareil et réessayez…</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="332"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="236"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="477"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="233"/>
        <source>Really abort import?</source>
        <translation>L&apos;importation est vraiment interrompue ?</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="117"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="61"/>
        <source>No Bluetooth controller found.</source>
        <translation>Aucun contrôleur Bluetooth n&apos;a été trouvé.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="182"/>
        <source>An error occurred during device discovery.

%1</source>
        <translation>Une erreur s&apos;est produite lors de la découverte de l&apos;appareil.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="198"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="125"/>
        <source>No device discovered.

Check the connection in operating system or press Bluetooth button on device and try again…</source>
        <translation>Aucun appareil n&apos;a été découvert.

Vérifiez la connexion dans le système d&apos;exploitation ou appuyez sur le bouton Bluetooth de l&apos;appareil et réessayez…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="229"/>
        <source>The selected device is not supported.</source>
        <translation>L&apos;appareil sélectionné n&apos;est pas pris en charge.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="254"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="182"/>
        <source>Could not connect to the device.

Check the connection in operating system or press Bluetooth button on device and try again…

%1</source>
        <translation>Impossible de se connecter à l&apos;appareil.

Vérifiez la connexion dans le système d&apos;exploitation ou appuyez sur le bouton Bluetooth de l&apos;appareil et réessayez...

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="259"/>
        <source>Unconnected</source>
        <translation>Non connecté</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="259"/>
        <source>Connecting…</source>
        <translation>Connexion…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="259"/>
        <source>Connected</source>
        <translation>Connecté</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="259"/>
        <source>Discovering…</source>
        <translation>Découvrir…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="259"/>
        <source>Discovered</source>
        <translation>Découvert</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="259"/>
        <source>Closing…</source>
        <translation>Fermeture…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="259"/>
        <source>Advertising…</source>
        <translation>Publicités …</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="261"/>
        <source>Current State of the Device: %1</source>
        <translation>État actuel du dispositif&#x202f;: %1</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="297"/>
        <source>Unknown UID &quot;%1&quot; detected, transfer aborted.

Change UID settings and try again…</source>
        <translation>UID inconnu &quot;%1&quot; détecté, transfert interrompu.

Modifiez les paramètres de l&apos;UID et réessayez…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="419"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="483"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="311"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="364"/>
        <source>Lost connection to device.</source>
        <translation>Perte de connexion à l&apos;appareil.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="519"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="467"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="727"/>
        <source>Could not access the Bluetooth service %1.</source>
        <translation>Impossible d&apos;accéder au service Bluetooth %1.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="680"/>
        <source>Auto connection requires a model name.</source>
        <translation>La connexion automatique nécessite un nom de modèle.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="734"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="763"/>
        <source>The selected Bluetooth controller is not available.</source>
        <translation>Le contrôleur Bluetooth sélectionné n&apos;est pas disponible.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="30"/>
        <source>Select Bluetooth Controller</source>
        <translation>Sélectionner le contrôleur Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="60"/>
        <source>Select Bluetooth Device</source>
        <translation>Sélectionner le périphérique Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="90"/>
        <source>Discover Device automatically</source>
        <translation>Découvrir automatiquement l&apos;appareil</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="103"/>
        <source>Connect Device automatically</source>
        <translation>Connecter automatiquement l&apos;appareil</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="306"/>
        <source>Write Pairing Key</source>
        <translation>Écrire la clé d&apos;appariement</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="110"/>
        <source>Bluetooth error.

%1</source>
        <translation>Erreur Bluetooth.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="158"/>
        <source>The selected device is not a %1.</source>
        <translation>Le dispositif sélectionné n&apos;est pas un %1.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="284"/>
        <source>No answer from the device.

Did you use the Bluetooth controller with the cloned MAC address or set a pairing key?

Read the wiki for details on how to do this.</source>
        <translation>Pas de réponse de l&apos;appareil.

Avez-vous utilisé le contrôleur Bluetooth avec l&apos;adresse MAC clonée ou défini une clé d&apos;appariement&#x202f;?

Lisez le wiki pour plus de détails sur la façon de procéder.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="673"/>
        <source>Omron devices require a special pairing key to read data, which only needs to be set once.

The device must be connected in pairing mode: press and hold the Bluetooth button until a &quot;P&quot; flashes on the display.

Was pairing mode enabled before you started the plugin and do you want to send the key now?</source>
        <translation>Les appareils Omron nécessitent une clé d&apos;appariement spéciale pour lire les données, qui ne doit être réglée qu&apos;une seule fois.

appareil doit être connecté en mode d&apos;appairage&#xa0;: appuyez sur le bouton Bluetooth et maintenez-le enfoncé jusqu&apos;à ce qu&apos;un « P » clignote sur l&apos;écran.

Le mode d&apos;appairage était-il activé avant le démarrage du plugin et souhaitez-vous envoyer la clé maintenant&#x202f;?</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="718"/>
        <source>The pairing process has completed.

The display should no longer show the flashing &quot;P&quot; if the key was successfully sent.

Read the wiki for details on troubleshooting.</source>
        <translation>Le processus d&apos;appariement est terminé.

L&apos;écran ne doit plus afficher le « P » clignotant si la clé a été envoyée avec succès.

Lisez le wiki pour plus de détails sur le dépannage.</translation>
    </message>
</context>
</TS>
