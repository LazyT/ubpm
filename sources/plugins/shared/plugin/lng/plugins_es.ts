<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>DialogImport</name>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="14"/>
        <source>Device Import</source>
        <translation>Importación desde dispositivo</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="84"/>
        <source>Plugin Options</source>
        <translation>Opciones del complemento</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="110"/>
        <source>Import Measurements automatically</source>
        <translation>Importar registros automáticamente</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="375"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="120"/>
        <source>Device Information</source>
        <translation>Información del dispositivo</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="14"/>
        <source>Generic Bluetooth</source>
        <translation>Bluetooth genérico</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="36"/>
        <source>Automatically discover devices on startup</source>
        <translation>Detección automática de los dispositivos al iniciar</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="39"/>
        <source>Auto Discover</source>
        <translation>Reconocimiento automático</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="74"/>
        <source>Automatically connect to device on startup</source>
        <translation>Conexión automática al dispositivo al iniciarse</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="77"/>
        <source>Auto Connect</source>
        <translation>Conexión automática</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="106"/>
        <source>Automatically import measurements on startup</source>
        <translation>Importación automática de las mediciones al inicio</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="109"/>
        <source>Auto Import</source>
        <translation>Importación automática</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="137"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="24"/>
        <source>Bluetooth Controller</source>
        <translation>Controlador del Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="143"/>
        <source>Select Bluetooth controller</source>
        <translation>Selecciona el controlador Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="161"/>
        <source>Setup discovery timeout</source>
        <translation>Configurar el tiempo de espera de detección</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="207"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="37"/>
        <source>Discover Device</source>
        <translation>Descubrir el dispositivo</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="259"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="51"/>
        <source>Bluetooth Device</source>
        <translation>Dispositivo Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="268"/>
        <source>Select Bluetooth device</source>
        <translation>Selecciona el dispositivo Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="283"/>
        <source>Enter devicename for auto connection</source>
        <translation>Introduzca el nombre del dispositivo para la conexión automática</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="296"/>
        <source>Copy devicename</source>
        <translation>Copiar el nombre del dispositivo</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="321"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="70"/>
        <source>Connect Device</source>
        <translation>Conectar el dispositivo</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="384"/>
        <source>Current State of the Device: Unconnected</source>
        <translation>Estado actual del dispositivo: Desconectado</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="435"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="69"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="69"/>
        <source>Serial</source>
        <translation>Serie</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="463"/>
        <source>Manufacturer</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="477"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="542"/>
        <source>Hardware</source>
        <translation>Hardware</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="549"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="149"/>
        <source>Firmware</source>
        <translation>Firmware</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="556"/>
        <source>Software</source>
        <translation>Software</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="630"/>
        <source>Device Measurements</source>
        <translation>Mediciones de los dispositivos</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="654"/>
        <source>ID for User 1</source>
        <translation>ID del usuario 1</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="702"/>
        <source>ID for User 2</source>
        <translation>ID del usuario 2</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="790"/>
        <source>enable/disable Logfile</source>
        <translation>activar/desactivar Archivo de registro</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="62"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="62"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="76"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="76"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="156"/>
        <source>Producer</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="83"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="83"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="163"/>
        <source>Product</source>
        <translation>Producto</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="55"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="86"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="55"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="86"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="86"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="86"/>
        <source>Description</source>
        <translation>Descripción</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="158"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="182"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="111"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="135"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="770"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="188"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="135"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="173"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="132"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="262"/>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="175"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="199"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="128"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="152"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="205"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="152"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="190"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="149"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="279"/>
        <source>Write Logfile</source>
        <translation>Excribir archivo de registro</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="202"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="226"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="155"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="179"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="817"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="232"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="179"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="217"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="176"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="329"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="24"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="24"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="24"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="24"/>
        <source>Could not find a serial port.

Is the usb2serial driver installed and the device connected?</source>
        <translation>No se pudo encontrar un puerto serie.

¿Están el controlador usb2serial instalado y el dispositivo conectado?</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="76"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="76"/>
        <source>Could not read data.

%1</source>
        <translation>No se pudo leer la información.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="85"/>
        <source>The device doesn&apos;t respond.</source>
        <translation>El dispositivo no responde.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="316"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="270"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="318"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="222"/>
        <source>Could not open serial port &quot;%1&quot;.

%2

Try as root or create a udev rule.

Read the wiki for details on how to do this.</source>
        <translation>No se ha podido abrir el puerto serie &quot;%1&quot;.

%2

Inténtalo como root o crea una regla udev.

Lea la wiki para más detalles sobre cómo hacerlo.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="212"/>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="253"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="244"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="286"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="203"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="240"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="417"/>
        <source>The import was canceled.</source>
        <translation>La importación fue cancelada.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="102"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="102"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="75"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="159"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="112"/>
        <source>Could not write data.

%1</source>
        <translation>No se pudo escribir la información.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="85"/>
        <source>The device doesn&apos;t respond.

Press &quot;MEM&quot;, select U1/U2 with &quot;POWER&quot; and wait until the user data is shown.

Then try again…</source>
        <translation>El dispositivo no responde.

Presione &quot;MEM&quot;, seleccione U1/U2 pulsando &quot;POWER&quot; y espere hasta que se muestren los datos del usuario.

Entonces, inténtelo de nuevo…</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="115"/>
        <source>Was &quot;U1&quot; selected on the device?

For &quot;U2&quot; answer with No.</source>
        <translation>¿Estaba &quot;U1&quot; seleccionado en el dispositivo?

Para &quot;U2&quot; responda No.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="180"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="200"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="173"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="198"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="796"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="252"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="187"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="279"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="177"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="688"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="816"/>
        <source>Could not open the logfile %1.

%2</source>
        <translation>No se pudo abrir el archivo de registro %1.

%2</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="38"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="38"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="45"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="40"/>
        <source>Could not open usb device %1:%2.

Try as root or create a udev rule.

Read the wiki for details on how to do this.</source>
        <translation>No se pudo abrir el dispositivo usb %1:%2.

Inténtelo como root o cree una regla udev.

Lea la wiki para más detalles sobre cómo hacerlo.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="190"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="183"/>
        <source>The device doesn&apos;t respond.

Disconnect the USB cable and reconnect it.

Then try again…</source>
        <translation>El dispositivo no responde.

Desconecte el cable USB y vuélvalo a conectar.

Después inténtelo de nuevo…</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="201"/>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="242"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="192"/>
        <source>Could not read measurement count.</source>
        <translation>No se pudo leer el recuento de mediciones.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="223"/>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="264"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="214"/>
        <source>Could not read measurement %1.</source>
        <translation>No se ha podido leer la medición %1.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="286"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="330"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="236"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="284"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="817"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="839"/>
        <source>Cancel import?</source>
        <translation>¿Cancelar importación?</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="297"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="341"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="247"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="295"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="847"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="343"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="247"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="488"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="244"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="850"/>
        <source>Import in progress…</source>
        <translation>Importación en progreso…</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="286"/>
        <source>Can&apos;t set device into info mode.</source>
        <translation>No se pudo poner el dispositivo en modo info.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="299"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="313"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="338"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="355"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="372"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="409"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="448"/>
        <source>Send %1 command failed.</source>
        <translation>Falló al enviar el comando %1.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="325"/>
        <source>Can&apos;t set device into data mode.</source>
        <translation>No se pudo poner el dispositivo en modo data.</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="117"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="70"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="393"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="435"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="202"/>
        <source>Import aborted by user.</source>
        <translation>Importación cancelada por el usuario.</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="152"/>
        <source>Acknowledge failed.</source>
        <translation>Reconocimiento fallido.</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="84"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="100"/>
        <source>Could not read data.

Is the device powered on?

%1</source>
        <translation>No se pudo leer la información.

¿Está el dispositivo encendido?

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="163"/>
        <source>Device doesn&apos;t respond, try again?</source>
        <translation>El dispositivo no responde, ¿intentar nuevamente?</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="187"/>
        <source>Press START/STOP on device and try again…</source>
        <translation>Pulse START/STOP en el dispositivo e inténtelo nuevamente…</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="332"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="236"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="477"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="233"/>
        <source>Really abort import?</source>
        <translation>¿Realmente cancelar la importación?</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="117"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="61"/>
        <source>No Bluetooth controller found.</source>
        <translation>No se ha encontrado ningún controlador Bluetooth.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="182"/>
        <source>An error occurred during device discovery.

%1</source>
        <translation>Se ha producido un error durante la detección de los dispositivos.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="198"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="125"/>
        <source>No device discovered.

Check the connection in operating system or press Bluetooth button on device and try again…</source>
        <translation>No se ha detectado ningún dispositivo.

Comprueba la conexión en el sistema operativo o pulsa el botón Bluetooth del dispositivo e inténtalo de nuevo…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="229"/>
        <source>The selected device is not supported.</source>
        <translation>El dispositivo seleccionado no es compatible.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="254"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="182"/>
        <source>Could not connect to the device.

Check the connection in operating system or press Bluetooth button on device and try again…

%1</source>
        <translation>No se ha podido conectar con el dispositivo.

Compruebe la conexión en el sistema operativo o pulse el botón Bluetooth del dispositivo e inténtelo de nuevo...

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="259"/>
        <source>Unconnected</source>
        <translation>Desconectado</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="259"/>
        <source>Connecting…</source>
        <translation>Conectando…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="259"/>
        <source>Connected</source>
        <translation>Conectado</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="259"/>
        <source>Discovering…</source>
        <translation>Descubriendo…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="259"/>
        <source>Discovered</source>
        <translation>Descubierto</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="259"/>
        <source>Closing…</source>
        <translation>Cerrando…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="259"/>
        <source>Advertising…</source>
        <translation>Anunciando…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="261"/>
        <source>Current State of the Device: %1</source>
        <translation>Estado actual del dispositivo: %1</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="297"/>
        <source>Unknown UID &quot;%1&quot; detected, transfer aborted.

Change UID settings and try again…</source>
        <translation>UID desconocido &quot;%1&quot; detectado, transferencia abortada.

Cambie la configuración del UID e inténtelo de nuevo…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="419"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="483"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="311"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="364"/>
        <source>Lost connection to device.</source>
        <translation>Se ha perdido la conexión con el dispositivo.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="519"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="467"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="727"/>
        <source>Could not access the Bluetooth service %1.</source>
        <translation>No se ha podido acceder al Bluetooth %1.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="680"/>
        <source>Auto connection requires a model name.</source>
        <translation>La conexión automática requiere un nombre de modelo.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="734"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="763"/>
        <source>The selected Bluetooth controller is not available.</source>
        <translation>El controlador Bluetooth seleccionado no está disponible.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="30"/>
        <source>Select Bluetooth Controller</source>
        <translation>Seleccionar controlador Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="60"/>
        <source>Select Bluetooth Device</source>
        <translation>Seleccionar dispositivo Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="90"/>
        <source>Discover Device automatically</source>
        <translation>Encontrar el dispositivo automáticamente</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="103"/>
        <source>Connect Device automatically</source>
        <translation>Conectar el dispositivo automáticamente</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="306"/>
        <source>Write Pairing Key</source>
        <translation>Escribir clave de emparejamiento</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="110"/>
        <source>Bluetooth error.

%1</source>
        <translation>Error del Bluetooth.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="158"/>
        <source>The selected device is not a %1.</source>
        <translation>El dispositivo seleccionado no es un %1.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="284"/>
        <source>No answer from the device.

Did you use the Bluetooth controller with the cloned MAC address or set a pairing key?

Read the wiki for details on how to do this.</source>
        <translation>No hay respuesta del dispositivo.

¿Utilizaste el controlador Bluetooth con la dirección MAC clonada o configuraste una clave de emparejamiento?

Lee la wiki para más detalles sobre cómo hacerlo.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="673"/>
        <source>Omron devices require a special pairing key to read data, which only needs to be set once.

The device must be connected in pairing mode: press and hold the Bluetooth button until a &quot;P&quot; flashes on the display.

Was pairing mode enabled before you started the plugin and do you want to send the key now?</source>
        <translation>Los dispositivos Omron requieren una clave de emparejamiento especial para leer los datos, que sólo debe configurarse una vez.

El dispositivo debe estar conectado en modo de emparejamiento: mantenga pulsado el botón Bluetooth hasta que parpadee una &quot;P&quot; en la pantalla.

¿Estaba activado el modo de emparejamiento antes de iniciar el plugin y desea enviar la clave ahora?</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="718"/>
        <source>The pairing process has completed.

The display should no longer show the flashing &quot;P&quot; if the key was successfully sent.

Read the wiki for details on troubleshooting.</source>
        <translation>El proceso de emparejamiento se ha completado.

La pantalla ya no debería mostrar la &quot;P&quot; parpadeante si la clave se envió correctamente.

Lea la wiki para obtener detalles sobre la solución de problemas.</translation>
    </message>
</context>
</TS>
