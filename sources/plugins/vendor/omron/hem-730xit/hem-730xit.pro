TEMPLATE	= lib
CONFIG		+= plugin no_plugin_name_prefix
QT			+= widgets
DEFINES		+= HEM730XIT
INCLUDEPATH	+= ../../../../ ../hem-7080it/
SOURCES		= ../hem-7080it/DialogImport.cpp ../../../shared/plugin/plugin.cpp
HEADERS		= ../hem-7080it/DialogImport.h   ../../../shared/plugin/plugin.h
FORMS		= ../hem-7080it/DialogImport.ui
RESOURCES	= ../../../shared/plugin/res/plugin.qrc
TARGET		= ../../../omron-hem730xit

unix:!macx {
contains(DEFINES, EXTHIDAPI) {
LIBS		+= -lhidapi-hidraw
} else {
INCLUDEPATH	+= ../../../shared/hidapi/hidapi
SOURCES		+= ../../../shared/hidapi/linux/hid.c
LIBS		+= -ludev
}
}

win32 {
contains(DEFINES, EXTHIDAPI) {
LIBS		+= -lhidapi
} else {
INCLUDEPATH	+= ../../../shared/hidapi/hidapi
SOURCES		+= ../../../shared/hidapi/windows/hid.c
LIBS		+= -lsetupapi
}
CONFIG		-= debug_and_release
}

macx {
contains(DEFINES, EXTHIDAPI) {
LIBS		+= -lhidapi
} else {
INCLUDEPATH	+= ../../../shared/hidapi/hidapi
SOURCES		+= ../../../shared/hidapi/mac/hid.c
}
}
