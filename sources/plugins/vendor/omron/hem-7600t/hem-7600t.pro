TEMPLATE	= lib
CONFIG		+= plugin no_plugin_name_prefix
QT			+= widgets bluetooth
DEFINES		+= HEM7600T
INCLUDEPATH	+= ../../../../ ../hem-7361t/
SOURCES		= ../hem-7361t/DialogImport.cpp ../../../shared/plugin/plugin.cpp
HEADERS		= ../hem-7361t/DialogImport.h   ../../../shared/plugin/plugin.h
FORMS		= ../hem-7361t/DialogImport.ui
RESOURCES	= ../../../shared/plugin/res/plugin.qrc
TARGET		= ../../../omron-hem7600t

unix:!macx {
QMAKE_RPATHDIR	+= $ORIGIN/../../lib
}

win32 {
CONFIG		-= debug_and_release
}

macx {
}
