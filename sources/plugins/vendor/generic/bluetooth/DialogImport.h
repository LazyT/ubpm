#ifndef DLGIMPORT_H
#define DLGIMPORT_H

#define MAINTAINER	QString("<a href='mailto:lazyt@mailbox.org?subject=UBPM Plugin %1'>Thomas Löwe</a>%2").arg(MODEL, HELPER)
#define VERSION		QString("1.4.0 [ BLUETOOTH ]")
#define PRODUCER	"<a href='https://www.bluetooth.com/specifications/specs/blood-pressure-service'>Generic</a>"
#define ICON		":/plugin/svg/bluetooth.svg"

#define MODEL	"Generic Bluetooth"
#define ALIAS	"0x1810 Bluetooth Devices"
#define HELPER	""

struct
{
	struct { int byte =  7; int bit = 0; int len = 16; } year;
	struct { int byte =  9; int bit = 0; int len =  8; } month;
	struct { int byte = 10; int bit = 0; int len =  8; } day;
	struct { int byte = 11; int bit = 0; int len =  8; } hour;
	struct { int byte = 12; int bit = 0; int len =  8; } minute;
	struct { int byte = 13; int bit = 0; int len =  8; } second;
	struct { int byte =  1; int bit = 0; int len = 16; } sys;
	struct { int byte =  3; int bit = 0; int len = 16; } dia;
	struct { int byte = 14; int bit = 0; int len = 16; } bpm;
	struct { int byte = 17; int bit = 2; int len =  1; } ihb;
	struct { int byte = 17; int bit = 0; int len =  1; } mov;

}devicedata;

#include "ui_DialogImport.h"

#include <QBluetoothLocalDevice>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothServiceDiscoveryAgent>
#include <QLowEnergyController>
#include <QBitArray>
#include <QDateTime>
#include <QFile>
#include <QMessageBox>
#include <QStandardPaths>
#include <QTimer>
#include <QThread>
#include <QTranslator>
#include <QResizeEvent>

#include "deviceinterface.h"

class DialogImport : public QDialog, private Ui::DialogImport
{
	Q_OBJECT

public:

	explicit DialogImport(QWidget*, QString, QVector <struct HEALTHDATA>*, QVector <struct HEALTHDATA>*, struct SETTINGS*);

	bool failed = false;

private:

	QBluetoothLocalDevice *bld;
	QBluetoothDeviceDiscoveryAgent *bdda;
	QLowEnergyController *lec;
	QLowEnergyService *les_info, *les_data;
	QList<QBluetoothDeviceInfo> bdi;

	QTimer *timerDiscover = new QTimer();
	QTimer *timerConnect = new QTimer();
	QTimer *timerMeasurement = new QTimer();
	QElapsedTimer timeout;

	QByteArray payloads[2];

	QFile log;

	QVector <struct HEALTHDATA> *u1, *u2;

	struct SETTINGS *settings;

	bool abort = false;
	bool finished = true;

	void enableControllerElements(bool);
	void enableDeviceElements(bool);

	int searchBtController();

	bool waitBT(qint64);
	void readBTInfo();
	bool readBTData();

	int bits2Value(QBitArray, int, int, int);
	void decryptPayload();

	void logRawData(bool, QLowEnergyCharacteristic, QByteArray);

private slots:

	void timeoutDiscover();
	void timeoutConnect();
	void timeoutMeasurement();

	void on_comboBox_controller_currentIndexChanged(int);

	void on_lineEdit_device_textChanged(const QString&);

	void on_horizontalSlider_discover_valueChanged(int);

	void on_toolButton_auto_discover_toggled(bool);
	void on_toolButton_auto_connect_toggled(bool);
	void on_toolButton_auto_import_toggled(bool);
	void on_toolButton_log_toggled(bool);

	void bddaCanceled();
	void bddaDeviceDiscovered(QBluetoothDeviceInfo);
	void bddaError(QBluetoothDeviceDiscoveryAgent::Error);
	void bddaFinished();

	void lecConnected();
	void lecDisconnected();
	void lecDiscoveryFinished();
	void lecErrorOccurred(QLowEnergyController::Error);
	void lecstateChanged(QLowEnergyController::ControllerState);

	void lesCharacteristicChanged(QLowEnergyCharacteristic, QByteArray);
	void lesCharacteristicRead(QLowEnergyCharacteristic, QByteArray);
//	void lesCharacteristicWritten(QLowEnergyCharacteristic, QByteArray);
	void lesDescriptorWritten(const QLowEnergyDescriptor&, const QByteArray&);
	void lesErrorOccurred(QLowEnergyService::ServiceError);
	void lesStateChanged(QLowEnergyService::ServiceState);

	void on_pushButton_discover_clicked();
	void on_pushButton_connect_clicked();
	void on_toolButton_copy_clicked();
	void on_pushButton_import_clicked();
	void on_pushButton_cancel_clicked();

	void resizeEvent(QResizeEvent*);

	void reject();
};

#endif // DLGIMPORT_H
