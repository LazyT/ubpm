TEMPLATE	= lib
CONFIG		+= plugin no_plugin_name_prefix
QT			+= widgets bluetooth
INCLUDEPATH	+= ../../../../
SOURCES		= DialogImport.cpp ../../../shared/plugin/plugin.cpp
HEADERS		= DialogImport.h   ../../../shared/plugin/plugin.h
FORMS		= DialogImport.ui
RESOURCES	= ../../../shared/plugin/res/plugin.qrc
TARGET		= ../../../generic-bluetooth

unix:!macx {
QMAKE_RPATHDIR	+= $ORIGIN/../../lib
}

win32 {
CONFIG		-= debug_and_release
}

macx {
}
