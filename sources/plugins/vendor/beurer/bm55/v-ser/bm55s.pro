TEMPLATE	= lib
CONFIG		+= plugin no_plugin_name_prefix
QT			+= widgets serialport
DEFINES		+= BM55
INCLUDEPATH	+= ../../../../../ ../../bm58/v-ser/
SOURCES		= ../../bm58/v-ser/DialogImport.cpp ../../../../shared/plugin/plugin.cpp
HEADERS		= ../../bm58/v-ser/DialogImport.h   ../../../../shared/plugin/plugin.h
FORMS		= ../../bm58/v-ser/DialogImport.ui
RESOURCES	= ../../../../shared/plugin/res/plugin.qrc
TARGET		= ../../../../beurer-bm55-s

unix:!macx {
QMAKE_RPATHDIR	+= $ORIGIN/../../lib
}

win32 {
CONFIG		-= debug_and_release
}

macx {
}
