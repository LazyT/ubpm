TEMPLATE	= lib
CONFIG		+= plugin no_plugin_name_prefix
QT			+= widgets
DEFINES		+= BC58
INCLUDEPATH	+= ../../../../../ ../../bm58/v-hid/
SOURCES		= ../../bm58/v-hid/DialogImport.cpp ../../../../shared/plugin/plugin.cpp
HEADERS		= ../../bm58/v-hid/DialogImport.h   ../../../../shared/plugin/plugin.h
FORMS		= ../../bm58/v-hid/DialogImport.ui
RESOURCES	= ../../../../shared/plugin/res/plugin.qrc
TARGET		= ../../../../beurer-bc58-h

unix:!macx {
contains(DEFINES, EXTHIDAPI) {
LIBS		+= -lhidapi-hidraw
} else {
INCLUDEPATH	+= ../../../../shared/hidapi/hidapi
SOURCES		+= ../../../../shared/hidapi/linux/hid.c
LIBS		+= -ludev
}
}

win32 {
contains(DEFINES, EXTHIDAPI) {
LIBS		+= -lhidapi
} else {
INCLUDEPATH	+= ../../../../shared/hidapi/hidapi
SOURCES		+= ../../../../shared/hidapi/windows/hid.c
LIBS		+= -lsetupapi
}
CONFIG		-= debug_and_release
}

macx {
contains(DEFINES, EXTHIDAPI) {
LIBS		+= -lhidapi
} else {
INCLUDEPATH	+= ../../../../shared/hidapi/hidapi
SOURCES		+= ../../../../shared/hidapi/mac/hid.c
}
}
