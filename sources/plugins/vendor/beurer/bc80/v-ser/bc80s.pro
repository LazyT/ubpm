TEMPLATE	= lib
CONFIG		+= plugin no_plugin_name_prefix
QT			+= widgets serialport
INCLUDEPATH	+= ../../../../../
SOURCES		= DialogImport.cpp ../../../../shared/plugin/plugin.cpp
HEADERS		= DialogImport.h   ../../../../shared/plugin/plugin.h
FORMS		= DialogImport.ui
RESOURCES	= ../../../../shared/plugin/res/plugin.qrc
TARGET		= ../../../../beurer-bc80-s

unix:!macx {
QMAKE_RPATHDIR	+= $ORIGIN/../../lib
}

win32 {
CONFIG		-= debug_and_release
}

macx {
}
