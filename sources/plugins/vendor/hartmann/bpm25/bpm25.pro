TEMPLATE	= lib
CONFIG		+= plugin no_plugin_name_prefix
QT			+= widgets serialport
DEFINES		+= BPM25
INCLUDEPATH	+= ../../../../ ../gce604/
SOURCES		= ../gce604/DialogImport.cpp ../../../shared/plugin/plugin.cpp
HEADERS		= ../gce604/DialogImport.h   ../../../shared/plugin/plugin.h
FORMS		= ../gce604/DialogImport.ui
RESOURCES	= ../../../shared/plugin/res/plugin.qrc
TARGET		= ../../../veroval-bpm25

unix:!macx {
QMAKE_RPATHDIR	+= $ORIGIN/../../lib
}

win32 {
CONFIG		-= debug_and_release
}

macx {
}
