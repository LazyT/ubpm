#ifndef DLGIMPORT_H
#define DLGIMPORT_H

#define VID 0x067B
#define PID 0x2303

#define MAINTAINER	QString("<a href='mailto:lazyt@mailbox.org?subject=UBPM Plugin %1'>Thomas Löwe</a>%2").arg(MODEL, HELPER)
#define VERSION		QString("1.4.0 [ USB2SERIAL ]")
#define PRODUCER	"<a href='https://www.veroval.info/en/products/bloodpressure'>Hartmann</a>"
#ifdef BPM25
	#define MODEL	"BPM25"
	#define ALIAS	"Veroval WR"
	#define HELPER	", Carsten Presser"
#else
	#define MODEL	"GCE604"
	#define ALIAS	"Veroval UA"
	#define HELPER	", Annett Heyder"
#endif
#define ICON		":/plugin/svg/usb-srl.svg"

#define TIMEOUT 1000

#include "ui_DialogImport.h"

#include <QDateTime>
#include <QDebug>
#include <QFile>
#include <QMessageBox>
#include <QStandardPaths>
#include <QSerialPortInfo>
#include <QSerialPort>
#include <QThread>
#include <QTimer>
#include <QTranslator>

#include "deviceinterface.h"

class DialogImport : public QDialog, private Ui::DialogImport
{
	Q_OBJECT

public:

	explicit DialogImport(QWidget*, QString, QVector <struct HEALTHDATA>*, QVector <struct HEALTHDATA>*, struct SETTINGS*);

	bool failed = false;

private:

	QByteArray cmd_init = QByteArray::fromHex("A5F90101FB");
	QByteArray cmd_exit = QByteArray::fromHex("A6F90101FB");

	QSerialPortInfo spi;
	QSerialPort sp;

	QByteArray payload;
	QFile log;

	QVector <struct HEALTHDATA> *u1, *u2;

	struct SETTINGS *settings;

	bool abort = false;
	bool finished = true;

	bool sendCMD(QByteArray);
	void decryptPayload();
	void logRawData(bool, int, QByteArray);

private slots:

	void on_comboBox_activated(int);

	void on_checkBox_auto_import_toggled(bool);

	void on_toolButton_toggled(bool);

	void on_pushButton_import_clicked();
	void on_pushButton_cancel_clicked();

	void reject();
};

#endif // DLGIMPORT_H
