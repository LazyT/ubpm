#include "DialogDistribution.h"

DialogDistribution::DialogDistribution(QWidget *parent, int User, QString BMI, QString Measurements, QString Average, QString Timespan, bool dark, struct SETTINGS *psettings, QVector <struct HEALTHDATA> *pview, int r1, int r2, int r3, int r4, int r5, int r6) : QDialog(parent)
{
	setupUi(this);

	setWindowFlags(Qt::Window | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint);

	user = User;
	darkTheme = dark;
	bmi = BMI;
	measurements = Measurements;
	average = Average;
	timespan = Timespan;
	settings = psettings;
	view = pview;

	groupBox->setTitle(QString("%1 [ %2 %3, %4 ]").arg(settings->user[user].name).arg(view->length()).arg(tr("Record(s)", "", view->length())).arg(timespan));

	if(darkTheme)
	{
		chartBP->setTheme(QChart::ChartThemeDark);
		chartHR->setTheme(QChart::ChartThemeDark);
	}

	for(int i = 0; i < view->length(); i++)
	{
		if(view->at(i).bpm < MAX_BPM)
		{
			percent[view->at(i).bpm]++;
		}
	}

	for(int i = 0; i < MAX_BPM; i++)
	{
		if(percent[i] > max_percent)
		{
			max_percent = percent[i];
		}
	}

	max_percent = max_percent * 100 / view->length();

	if(max_percent != 100)
	{
		max_percent += 10 - max_percent % 10;
	}

	connect(scatterSeriesBP, &QScatterSeries::hovered, this, &DialogDistribution::chartSeriesHovered);
	connect(scatterSeriesHR, &QScatterSeries::hovered, this, &DialogDistribution::chartSeriesHovered);

	prepareBP();
	prepareHR(settings->chart.dynamic, r1, r2, r3, r4, r5, r6);

	for(int i = 0; i < view->length(); i++)
	{
		scatterSeriesBP->append(view->at(i).sys, view->at(i).dia);
	}

	for(int i = 0; i < view->length(); i++)
	{
		scatterSeriesHR->append(view->at(i).bpm, percent[view->at(i).bpm] * 100 / view->length());
	}
}

void DialogDistribution::switchChartTheme(bool theme)
{
	chartBP->setTheme(theme ? QChart::ChartThemeDark : QChart::ChartThemeLight);
	chartHR->setTheme(theme ? QChart::ChartThemeDark : QChart::ChartThemeLight);

	areaSeriesBPR1->setColor(QColorConstants::Svg::lightskyblue);
	areaSeriesBPR2->setColor(QColorConstants::Svg::palegreen);
	areaSeriesBPR3->setColor(QColorConstants::Svg::yellowgreen);
	areaSeriesBPR4->setColor(QColorConstants::Svg::gold);
	areaSeriesBPR5->setColor(QColorConstants::Svg::orange);
	areaSeriesBPR6->setColor(QColorConstants::Svg::salmon);
	areaSeriesBPR7->setColor(QColorConstants::Svg::tomato);

	scatterSeriesBP->setBrush(QBrush(QIcon(":/svg/distribution-bp-black.svg").pixmap(settings->chart.symbolsize, settings->chart.symbolsize).toImage()));
	scatterSeriesBP->setPen(QPen(Qt::transparent));
	scatterSeriesBP->setMarkerSize(settings->chart.symbolsize);

	areaSeriesHRR1->setColor(QColorConstants::Svg::lightskyblue);
	areaSeriesHRR2->setColor(QColorConstants::Svg::palegreen);
	areaSeriesHRR3->setColor(QColorConstants::Svg::yellowgreen);
	areaSeriesHRR4->setColor(QColorConstants::Svg::gold);
	areaSeriesHRR5->setColor(QColorConstants::Svg::orange);
	areaSeriesHRR6->setColor(QColorConstants::Svg::salmon);
	areaSeriesHRR7->setColor(QColorConstants::Svg::tomato);

	scatterSeriesHR->setBrush(QBrush(QIcon(":/svg/distribution-hr-black.svg").pixmap(settings->chart.symbolsize, settings->chart.symbolsize).toImage()));
	scatterSeriesHR->setPen(QPen(Qt::transparent));
	scatterSeriesHR->setMarkerSize(settings->chart.symbolsize);
}

void DialogDistribution::prepareBP()
{
	lineSeriesBP00->append(QList<QPointF>() << QPointF(0, 0) << QPointF(SYS_R1, 0));
	lineSeriesBP01->append(QList<QPointF>() << QPointF(0, DIA_R1) << QPointF(SYS_R1, DIA_R1));
	lineSeriesBP02->append(QList<QPointF>() << QPointF(0, 0) << QPointF(SYS_R2, 0));
	lineSeriesBP03->append(QList<QPointF>() << QPointF(0, DIA_R2) << QPointF(SYS_R2, DIA_R2));
	lineSeriesBP04->append(QList<QPointF>() << QPointF(0, 0) << QPointF(SYS_R3, 0));
	lineSeriesBP05->append(QList<QPointF>() << QPointF(0, DIA_R3) << QPointF(SYS_R3, DIA_R3));
	lineSeriesBP06->append(QList<QPointF>() << QPointF(0, 0) << QPointF(SYS_R4, 0));
	lineSeriesBP07->append(QList<QPointF>() << QPointF(0, DIA_R4) << QPointF(SYS_R4, DIA_R4));
	lineSeriesBP08->append(QList<QPointF>() << QPointF(0, 0) << QPointF(SYS_R5, 0));
	lineSeriesBP09->append(QList<QPointF>() << QPointF(0, DIA_R5)<< QPointF(SYS_R5, DIA_R5));
	lineSeriesBP10->append(QList<QPointF>() << QPointF(0, 0) << QPointF(SYS_R6, 0));
	lineSeriesBP11->append(QList<QPointF>() << QPointF(0, DIA_R6)<< QPointF(SYS_R6, DIA_R6));
	lineSeriesBP12->append(QList<QPointF>() << QPointF(0, 0) << QPointF(SYS_R7, 0));
	lineSeriesBP13->append(QList<QPointF>() << QPointF(0, DIA_R7)<< QPointF(SYS_R7, DIA_R7));

	areaSeriesBPR1->setName(tr("Low"));
	areaSeriesBPR1->setColor(QColorConstants::Svg::lightskyblue);
	areaSeriesBPR2->setName(tr("Optimal"));
	areaSeriesBPR2->setColor(QColorConstants::Svg::palegreen);
	areaSeriesBPR3->setName(tr("Normal"));
	areaSeriesBPR3->setColor(QColorConstants::Svg::yellowgreen);
	areaSeriesBPR4->setName(tr("High Normal"));
	areaSeriesBPR4->setColor(QColorConstants::Svg::gold);
	areaSeriesBPR5->setName(tr("Hyper 1"));
	areaSeriesBPR5->setColor(QColorConstants::Svg::orange);
	areaSeriesBPR6->setName(tr("Hyper 2"));
	areaSeriesBPR6->setColor(QColorConstants::Svg::salmon);
	areaSeriesBPR7->setName(tr("Hyper 3"));
	areaSeriesBPR7->setColor(QColorConstants::Svg::tomato);

	scatterSeriesBP->setBrush(QBrush(QIcon(":/svg/distribution-bp-black.svg").pixmap(settings->chart.symbolsize, settings->chart.symbolsize).toImage()));
	scatterSeriesBP->setPen(QPen(Qt::transparent));
	scatterSeriesBP->setMarkerSize(settings->chart.symbolsize);

	axisBPX1->setRange(0, SYS_R7);
	axisBPX1->append(QString::number(SYS_R1), SYS_R1);
	axisBPX1->append(QString::number(SYS_R2), SYS_R2);
	axisBPX1->append(QString::number(SYS_R3), SYS_R3);
	axisBPX1->append(QString::number(SYS_R4), SYS_R4);
	axisBPX1->append(QString::number(SYS_R5), SYS_R5);
	axisBPX1->append(QString::number(SYS_R6), SYS_R6);
	axisBPX1->append(QString::number(SYS_R7), SYS_R7);

	axisBPX2->setRange(0, SYS_R7);
	axisBPX2->append(tr("SYS"), SYS_R7);

	axisBPY1->setRange(0, DIA_R7);
	axisBPY1->append(QString::number(DIA_R1), DIA_R1);
	axisBPY1->append(QString::number(DIA_R2), DIA_R2);
	axisBPY1->append(QString::number(DIA_R3), DIA_R3);
	axisBPY1->append(QString::number(DIA_R4), DIA_R4);
	axisBPY1->append(QString::number(DIA_R5), DIA_R5);
	axisBPY1->append(QString::number(DIA_R6), DIA_R6);
	axisBPY1->append(QString::number(DIA_R7), DIA_R7);

	axisBPY2->setRange(0, DIA_R7);
	axisBPY2->append(tr("DIA"), DIA_R7);

	chartBP->addSeries(areaSeriesBPR7);
	chartBP->addSeries(areaSeriesBPR6);
	chartBP->addSeries(areaSeriesBPR5);
	chartBP->addSeries(areaSeriesBPR4);
	chartBP->addSeries(areaSeriesBPR3);
	chartBP->addSeries(areaSeriesBPR2);
	chartBP->addSeries(areaSeriesBPR1);
	chartBP->addSeries(scatterSeriesBP);

	chartBP->setTitle(QString("<center><h2><u>%1</u></h2></center><p>").arg(tr("Blood Pressure")));

	chartBP->createDefaultAxes();
	chartBP->axes(Qt::Horizontal).first()->hide();
	chartBP->axes(Qt::Vertical).first()->hide();
	chartBP->addAxis(axisBPX1, Qt::AlignBottom);
	chartBP->addAxis(axisBPY1, Qt::AlignLeft);
	chartBP->addAxis(axisBPX2, Qt::AlignTop);
	chartBP->addAxis(axisBPY2, Qt::AlignRight);

	chartBP->legend()->setAlignment(Qt::AlignBottom);
	chartBP->legend()->markers().at(7)->setVisible(false);
	chartBP->legend()->setReverseMarkers(true);

	chartBP->setBackgroundRoundness(0);
	chartBP->layout()->setContentsMargins(0, 0, 0, 0);

	chartViewBP->setChart(chartBP);
	chartViewBP->setRenderHint(QPainter::Antialiasing);
}

void DialogDistribution::prepareHR(bool dynamic, int r1, int r2, int r3, int r4, int r5, int r6)
{
	lineSeriesHR00->append(QList<QPointF>() << QPointF(0, 0) << QPointF(r1, 0));
	lineSeriesHR01->append(QList<QPointF>() << QPointF(0, dynamic ? max_percent : 100) << QPointF(r1, dynamic ? max_percent : 100));
	lineSeriesHR02->append(QList<QPointF>() << QPointF(0, 0) << QPointF(r2, 0));
	lineSeriesHR03->append(QList<QPointF>() << QPointF(0, dynamic ? max_percent : 100) << QPointF(r2, dynamic ? max_percent : 100));
	lineSeriesHR04->append(QList<QPointF>() << QPointF(0, 0) << QPointF(r3, 0));
	lineSeriesHR05->append(QList<QPointF>() << QPointF(0, dynamic ? max_percent : 100) << QPointF(r3, dynamic ? max_percent : 100));
	lineSeriesHR06->append(QList<QPointF>() << QPointF(0, 0) << QPointF(r4, 0));
	lineSeriesHR07->append(QList<QPointF>() << QPointF(0, dynamic ? max_percent : 100) << QPointF(r4, dynamic ? max_percent : 100));
	lineSeriesHR08->append(QList<QPointF>() << QPointF(0, 0) << QPointF(r5, 0));
	lineSeriesHR09->append(QList<QPointF>() << QPointF(0, dynamic ? max_percent : 100) << QPointF(r5, dynamic ? max_percent : 100));
	lineSeriesHR10->append(QList<QPointF>() << QPointF(0, 0) << QPointF(r6, 0));
	lineSeriesHR11->append(QList<QPointF>() << QPointF(0, dynamic ? max_percent : 100) << QPointF(r6, dynamic ? max_percent : 100));
	lineSeriesHR12->append(QList<QPointF>() << QPointF(0, 0) << QPointF(MAX_BPM, 0));
	lineSeriesHR13->append(QList<QPointF>() << QPointF(0, dynamic ? max_percent : 100) << QPointF(MAX_BPM, dynamic ? max_percent : 100));

	areaSeriesHRR1->setName(tr("Athlete"));
	areaSeriesHRR1->setColor(QColorConstants::Svg::lightskyblue);
	areaSeriesHRR2->setName(tr("Excellent"));
	areaSeriesHRR2->setColor(QColorConstants::Svg::palegreen);
	areaSeriesHRR3->setName(tr("Great"));
	areaSeriesHRR3->setColor(QColorConstants::Svg::yellowgreen);
	areaSeriesHRR4->setName(tr("Good"));
	areaSeriesHRR4->setColor(QColorConstants::Svg::gold);
	areaSeriesHRR5->setName(tr("Average"));
	areaSeriesHRR5->setColor(QColorConstants::Svg::orange);
	areaSeriesHRR6->setName(tr("Below Average"));
	areaSeriesHRR6->setColor(QColorConstants::Svg::salmon);
	areaSeriesHRR7->setName(tr("Poor"));
	areaSeriesHRR7->setColor(QColorConstants::Svg::tomato);

	scatterSeriesHR->setBrush(QBrush(QIcon(":/svg/distribution-hr-black.svg").pixmap(settings->chart.symbolsize, settings->chart.symbolsize).toImage()));
	scatterSeriesHR->setPen(QPen(Qt::transparent));
	scatterSeriesHR->setMarkerSize(settings->chart.symbolsize);

	axisHRX1->setRange(0, MAX_BPM);
	axisHRX1->append(QString::number(r1), r1);
	axisHRX1->append(QString::number(r2), r2);
	axisHRX1->append(QString::number(r3), r3);
	axisHRX1->append(QString::number(r4), r4);
	axisHRX1->append(QString::number(r5), r5);
	axisHRX1->append(QString::number(r6), r6);
	axisHRX1->append(QString::number(MAX_BPM), MAX_BPM);

	axisHRX2->setRange(0, MAX_BPM);
	axisHRX2->append(tr("BPM"), MAX_BPM);

	axisHRY1->setRange(0, dynamic ? max_percent : 100);
	axisHRY1->append("10", 10);
	axisHRY1->append("20", 20);
	axisHRY1->append("30", 30);
	axisHRY1->append("40", 40);
	axisHRY1->append("50", 50);
	axisHRY1->append("60", 60);
	axisHRY1->append("70", 70);
	axisHRY1->append("80", 80);
	axisHRY1->append("90", 90);
	axisHRY1->append("100", 100);

	axisHRY2->setRange(0, dynamic ? max_percent : 100);
	axisHRY2->append("%", dynamic ? max_percent : 100);

	chartHR->addSeries(areaSeriesHRR7);
	chartHR->addSeries(areaSeriesHRR6);
	chartHR->addSeries(areaSeriesHRR5);
	chartHR->addSeries(areaSeriesHRR4);
	chartHR->addSeries(areaSeriesHRR3);
	chartHR->addSeries(areaSeriesHRR2);
	chartHR->addSeries(areaSeriesHRR1);
	chartHR->addSeries(scatterSeriesHR);

	chartHR->setTitle(QString("<center><h2><u>%1</u></h2></center><p>").arg(tr("Heart Rate")));

	chartHR->createDefaultAxes();
	chartHR->axes(Qt::Horizontal).first()->hide();
	chartHR->axes(Qt::Vertical).first()->hide();
	chartHR->addAxis(axisHRX1, Qt::AlignBottom);
	chartHR->addAxis(axisHRY1, Qt::AlignLeft);
	chartHR->addAxis(axisHRX2, Qt::AlignTop);
	chartHR->addAxis(axisHRY2, Qt::AlignRight);

	chartHR->legend()->setAlignment(Qt::AlignBottom);
	chartHR->legend()->markers().at(7)->setVisible(false);
	chartHR->legend()->setReverseMarkers(true);

	chartHR->setBackgroundRoundness(0);
	chartHR->layout()->setContentsMargins(0, 0, 0, 0);

	chartViewHR->setChart(chartHR);
	chartViewHR->setRenderHint(QPainter::Antialiasing);
}

void DialogDistribution::chartSeriesHovered(QPointF point, bool state)
{
	QChartView *chart;
	QString fmt;

	if(sender() == scatterSeriesBP)
	{
		chart = chartViewBP;

		fmt = "%1 : %2%3";
	}
	else
	{
		chart = chartViewHR;

		fmt = "%1 (%2 = %3%)";
	}

	if(state)
	{
		chart->setToolTip(fmt.arg(point.x()).arg(chart == chartViewHR ? QString::number(percent[(int)point.x()]) : "").arg(point.y()));
	}
	else
	{
		chart->setToolTip(nullptr);
	}
}

void DialogDistribution::printChart(QPrinter *printer)
{
	QPainter draw, painter(printer);
	QByteArray data;
	QBuffer buffer(&data);
	QSvgGenerator *generator = nullptr;
	QSvgRenderer renderer;
	QSize size;
	bool restore = false;
	int PRINT_WIDTH, PRINT_HEIGHT, PRINT_DIA_W, PRINT_DIA_H;

	if(printer->pageLayout().orientation() == QPageLayout::Landscape)
	{
		PRINT_WIDTH  = PRINT_LS_WIDTH;
		PRINT_HEIGHT = PRINT_LS_HEIGHT;
		PRINT_DIA_W  = PRINT_LS_DIA_W;
		PRINT_DIA_H  = PRINT_LS_DIA_H;
	}
	else
	{
		PRINT_WIDTH  = PRINT_PT_WIDTH;
		PRINT_HEIGHT = PRINT_PT_HEIGHT;
		PRINT_DIA_W  = PRINT_PT_DIA_W;
		PRINT_DIA_H  = PRINT_PT_DIA_H;
	}

	if(darkTheme)
	{
		restore = true;

		switchChartTheme(false);
	}

	reinterpret_cast<MainWindow*>(parent())->drawPrintLayout(&draw, &buffer, &data, generator, printer->pageLayout().orientation());

	draw.setPen(QPen(Qt::black, 1, Qt::DashLine));

	size = chartViewBP->size();

	if(printer->pageLayout().orientation() == QPageLayout::Landscape)
	{
		chartViewBP->resize(PRINT_WIDTH/2, PRINT_HEIGHT);
//		chartViewBP->render(&draw, QRect(PRINT_SPACE, PRINT_HEADER + 2*PRINT_SPACE, PRINT_DIA_W/2 - PRINT_SPACE, PRINT_DIA_H), QRect(0, 0, PRINT_WIDTH/2, PRINT_HEIGHT), Qt::IgnoreAspectRatio);
		draw.drawPixmap(QRect(PRINT_SPACE, PRINT_HEADER + 2*PRINT_SPACE, PRINT_DIA_W/2 - PRINT_SPACE, PRINT_DIA_H), chartViewBP->grab());

		chartViewHR->resize(PRINT_WIDTH/2, PRINT_HEIGHT);
//		chartViewHR->render(&draw, QRect(2*PRINT_SPACE + PRINT_DIA_W/2, PRINT_HEADER + 2*PRINT_SPACE, PRINT_DIA_W/2 - PRINT_SPACE, PRINT_DIA_H), QRect(0, 0, PRINT_WIDTH/2, PRINT_HEIGHT), Qt::IgnoreAspectRatio);
		draw.drawPixmap(QRect(2*PRINT_SPACE + PRINT_DIA_W/2, PRINT_HEADER + 2*PRINT_SPACE, PRINT_DIA_W/2 - PRINT_SPACE, PRINT_DIA_H), chartViewHR->grab());

		draw.drawLine(PRINT_WIDTH/2, PRINT_HEADER + 2*PRINT_SPACE, PRINT_WIDTH/2, PRINT_HEIGHT - PRINT_FOOTER - 2*PRINT_SPACE);
	}
	else
	{
		chartViewBP->resize(PRINT_WIDTH, PRINT_HEIGHT/2);
//		chartViewBP->render(&draw, QRect(PRINT_SPACE, PRINT_HEADER + 2*PRINT_SPACE, PRINT_DIA_W, PRINT_DIA_H/2 - PRINT_SPACE), QRect(0, 0, PRINT_WIDTH, PRINT_HEIGHT/2), Qt::IgnoreAspectRatio);
		draw.drawPixmap(QRect(PRINT_SPACE, PRINT_HEADER + 2*PRINT_SPACE, PRINT_DIA_W, PRINT_DIA_H/2 - PRINT_SPACE), chartViewBP->grab());

		chartViewHR->resize(PRINT_WIDTH, PRINT_HEIGHT/2);
//		chartViewHR->render(&draw, QRect(PRINT_SPACE, PRINT_HEADER + 3*PRINT_SPACE + PRINT_DIA_H/2, PRINT_DIA_W, PRINT_DIA_H/2 - PRINT_SPACE), QRect(0, 0, PRINT_WIDTH, PRINT_HEIGHT/2), Qt::IgnoreAspectRatio);
		draw.drawPixmap(QRect(PRINT_SPACE, PRINT_HEADER + 3*PRINT_SPACE + PRINT_DIA_H/2, PRINT_DIA_W, PRINT_DIA_H/2 - PRINT_SPACE), chartViewHR->grab());

		draw.drawLine(PRINT_SPACE, PRINT_HEADER + 2*PRINT_SPACE + PRINT_DIA_H/2, PRINT_WIDTH - PRINT_SPACE, PRINT_HEADER + 2*PRINT_SPACE + PRINT_DIA_H/2);
	}

	chartViewBP->resize(size);
	chartViewHR->resize(size);

	draw.end();

	renderer.load(data);
	renderer.render(&painter);

	if(restore)
	{
		switchChartTheme(true);
	}
}

void DialogDistribution::on_pushButton_preview_clicked()
{
	printPreviewDialog = new QPrintPreviewDialog(this, Qt::Window | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint);

	printPreviewDialog->printer()->setPageSize(QPageSize(QPageSize::A4));
	printPreviewDialog->printer()->setPageOrientation(settings->print.orientation ? QPageLayout::Landscape : QPageLayout::Portrait);
	printPreviewDialog->printer()->setColorMode(QPrinter::Color);

	connect(printPreviewDialog, &QPrintPreviewDialog::paintRequested, this, [this]{ DialogDistribution::printChart(printPreviewDialog->printer()); });

	printPreviewDialog->findChildren<QToolBar*>().at(0)->actions().at(8)->setChecked(settings->print.orientation);	// why setPageOrientation doesn't set this?
	printPreviewDialog->setWindowState(Qt::WindowMaximized);
	printPreviewDialog->exec();

	disconnect(printPreviewDialog);
}

void DialogDistribution::on_pushButton_print_clicked()
{
	printDialog = new QPrintDialog(this);

	printDialog->printer()->setPageSize(QPageSize(QPageSize::A4));
	printDialog->printer()->setPageOrientation(settings->print.orientation ? QPageLayout::Landscape : QPageLayout::Portrait);
	printDialog->printer()->setColorMode(QPrinter::Color);

	if(printDialog->exec() == QDialog::Accepted)
	{
		printChart(printDialog->printer());
	}
}

void DialogDistribution::on_pushButton_pdf_clicked()
{
	QPrinter printer;
	QString filename = QFileDialog::getSaveFileName(this, tr("Export To PDF"), settings->exp + reinterpret_cast<MainWindow*>(parent())->pdfFileName(false), tr("%1 File (*.%2)").arg("PDF", "pdf"));

	if(!filename.isEmpty())
	{
		printer.setPageSize(QPageSize(QPageSize::A4));
		printer.setPageOrientation(settings->print.orientation ? QPageLayout::Landscape : QPageLayout::Portrait);
		printer.setColorMode(QPrinter::Color);
		printer.setOutputFormat(QPrinter::PdfFormat);

		printer.setOutputFileName(filename);

		printChart(&printer);
	}
}

void DialogDistribution::on_pushButton_close_clicked()
{
	close();
}

void DialogDistribution::keyPressEvent(QKeyEvent *ke)
{
	if(ke->key() == Qt::Key_F1)
	{
		reinterpret_cast<MainWindow*>(parent())->help->showHelp("01-08");
	}

	QDialog::keyPressEvent(ke);
}
