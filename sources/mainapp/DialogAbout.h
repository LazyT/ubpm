#ifndef DLGABOUT_H
#define DLGABOUT_H

#include "MainWindow.h"
#include "ui_DialogAbout.h"

class DialogAbout : public QDialog, private Ui::DialogAbout
{
	Q_OBJECT

public:

	explicit DialogAbout(QWidget*);

private slots:

	void linkActivated(QString);
};

#endif // DLGABOUT_H
