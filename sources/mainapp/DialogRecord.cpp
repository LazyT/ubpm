#include "DialogRecord.h"

DialogRecord::DialogRecord(QWidget *parent, int user, struct SETTINGS settings, struct HEALTHDATA record) : QDialog(parent)
{
	setupUi(this);

	layout()->setSizeConstraint(QLayout::SetFixedSize);

	toolButton_user1->setToolTip(tr("Add Record For %1").arg(settings.user[0].name.isEmpty() ? tr("User 1") : settings.user[0].name));
	toolButton_user2->setToolTip(tr("Add Record For %1").arg(settings.user[1].name.isEmpty() ? tr("User 2") : settings.user[1].name));

	dateTimeEdit->setDisplayFormat("dddd, " + settings.formatdateshort + " @ " + settings.formattimelong);
	dateTimeEdit->calendarWidget()->setGridVisible(true);
	dateTimeEdit->calendarWidget()->setVerticalHeaderFormat(QCalendarWidget::ISOWeekNumbers);

	if(!record.dts)
	{
		dateTimeEdit->setDateTime(QDateTime::currentDateTime());
	}
	else
	{
		dateTimeEdit->setDateTime(QDateTime::fromMSecsSinceEpoch(record.dts));
		dateTimeEdit->setEnabled(false);

		user ? toolButton_user1->setEnabled(false) : toolButton_user2->setEnabled(false);

		spinBox_sys->setValue(record.sys);
		spinBox_dia->setValue(record.dia);
		spinBox_bpm->setValue(record.bpm);
		lineEdit_comment->setText(record.msg);
		toolButton_ihb->setChecked(record.ihb);
		toolButton_mov->setChecked(record.mov);

		pushButton_create->setText(tr("Modify"));
	}

	QSignalBlocker blocker(toolButton_user2);	// fixme: prevent crash for user2?
	toolButton_user2->setChecked(user);

	connect(qApp, &QApplication::focusChanged, this, &DialogRecord::focusChanged);

	spinBox_sys->setFocus();
}

void DialogRecord::focusChanged(QWidget */*old*/, QWidget *now)
{
	if(now == spinBox_sys || now == spinBox_dia || now == spinBox_bpm)
	{
		reinterpret_cast<QSpinBox*>(now)->selectAll();
		QTimer::singleShot(0, [=](){reinterpret_cast<QSpinBox*>(now)->selectAll();});	// fixme: above line doesn't work for mouse clicks?
	}
}

void DialogRecord::on_toolButton_user2_toggled(bool checked)
{
	reinterpret_cast<MainWindow*>(parent())->switchUser(checked);
}

void DialogRecord::on_spinBox_sys_textChanged(QString value)
{
	if(!spinBox_sys->findChild<QLineEdit*>()->hasSelectedText())
	{
		bool next = false;

		if((value.startsWith("1") || value.startsWith("2")) && value.toInt() > 99)
		{
			next = true;
		}
		else if(value.toInt() > 29)
		{
			next = true;
		}

		if(next)
		{
			spinBox_dia->setFocus();
			spinBox_dia->selectAll();
		}
	}
}

void DialogRecord::on_spinBox_dia_textChanged(QString value)
{
	if(!spinBox_dia->findChild<QLineEdit*>()->hasSelectedText())
	{
		bool next = false;

		if((value.startsWith("1") || value.startsWith("2")) && value.toInt() > 99)
		{
			next = true;
		}
		else if(value.toInt() > 29)
		{
			next = true;
		}

		if(next)
		{
			spinBox_bpm->setFocus();
			spinBox_bpm->selectAll();
		}
	}
}

void DialogRecord::on_spinBox_bpm_textChanged(QString value)
{
	if(!spinBox_bpm->findChild<QLineEdit*>()->hasSelectedText())
	{
		bool next = false;

		if((value.startsWith("1") || value.startsWith("2")) && value.toInt() > 99)
		{
			next = true;
		}
		else if(value.toInt() > 29)
		{
			next = true;
		}

		if(next)
		{
			lineEdit_comment->setFocus();
			lineEdit_comment->selectAll();
		}
	}
}

void DialogRecord::on_pushButton_undo_clicked()
{
	if(!reinterpret_cast<MainWindow*>(parent())->recordDel(toolButton_user2->isChecked(), dateTimeEdit->dateTime().toMSecsSinceEpoch()/1000 * 1000))
	{
		QMessageBox::warning(this, APPNAME, tr("The data record could not be deleted!\n\nAn entry for this date & time doesn't exist."));

		return;
	}
	else if(checkBox_info->isChecked())
	{
		QMessageBox::information(this, APPNAME, tr("Data record successfully deleted."));
	}

	pushButton_undo->setEnabled(false);

	spinBox_sys->setFocus();
}

void DialogRecord::on_pushButton_create_clicked()
{
	if(spinBox_sys->value() == 0)
	{
		QMessageBox::warning(this, APPNAME, tr("Please enter a valid value for \"SYS\" first!"));

		spinBox_sys->setFocus();

		return;
	}


	if(spinBox_dia->value() == 0)
	{
		QMessageBox::warning(this, APPNAME, tr("Please enter a valid value for \"DIA\" first!"));

		spinBox_dia->setFocus();

		return;
	}

	if(spinBox_bpm->value() == 0)
	{
		QMessageBox::warning(this, APPNAME, tr("Please enter a valid value for \"BPM\" first!"));

		spinBox_bpm->setFocus();

		return;
	}

	HEALTHDATA record;

	record.dts = dateTimeEdit->dateTime().toMSecsSinceEpoch()/1000 * 1000; // set ms to 000
	record.sys = spinBox_sys->value();
	record.dia = spinBox_dia->value();
	record.bpm = spinBox_bpm->value();
	record.ihb = toolButton_ihb->isChecked();
	record.mov = toolButton_mov->isChecked();
	record.inv = false;
	record.msg = lineEdit_comment->text();

	if(pushButton_create->text() == tr("Modify"))
	{
		if(!reinterpret_cast<MainWindow*>(parent())->recordMod(toolButton_user2->isChecked(), record))
		{
			QMessageBox::warning(this, APPNAME, tr("The data record could not be modified!\n\nAn entry for this date & time doesn't exist."));

			return;
		}
		else if(checkBox_info->isChecked())
		{
			QMessageBox::information(this, APPNAME, tr("Data Record successfully modified."));
		}

		close();
	}
	else
	{
		if(!reinterpret_cast<MainWindow*>(parent())->recordAdd(toolButton_user2->isChecked(), record))
		{
			QMessageBox::warning(this, APPNAME, tr("The data record could not be created!\n\nAn entry for this date & time already exist."));

			return;
		}
		else if(checkBox_info->isChecked())
		{
			QMessageBox::information(this, APPNAME, tr("Data Record successfully created."));
		}

		pushButton_undo->setEnabled(true);

		spinBox_sys->setFocus();
	}
}

void DialogRecord::on_pushButton_close_clicked()
{
	close();
}

void DialogRecord::keyPressEvent(QKeyEvent *ke)
{
	if(ke->key() == Qt::Key_F1)
	{
		reinterpret_cast<MainWindow*>(parent())->help->showHelp("01-04");
	}

	QDialog::keyPressEvent(ke);
}
