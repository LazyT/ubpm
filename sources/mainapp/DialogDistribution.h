#ifndef DLGDISTRIBUTION_H
#define DLGDISTRIBUTION_H

#include "MainWindow.h"
#include "ui_DialogDistribution.h"

#define MAX_BPM 120

class DialogDistribution : public QDialog, private Ui::DialogDistribution
{
	Q_OBJECT

public:

	explicit DialogDistribution(QWidget*, int, QString, QString, QString, QString, bool, struct SETTINGS*, QVector <struct HEALTHDATA>*, int, int, int, int, int, int);

private:

		QLineSeries *lineSeriesBP00 = new QLineSeries();
		QLineSeries *lineSeriesBP01 = new QLineSeries();
		QLineSeries *lineSeriesBP02 = new QLineSeries();
		QLineSeries *lineSeriesBP03 = new QLineSeries();
		QLineSeries *lineSeriesBP04 = new QLineSeries();
		QLineSeries *lineSeriesBP05 = new QLineSeries();
		QLineSeries *lineSeriesBP06 = new QLineSeries();
		QLineSeries *lineSeriesBP07 = new QLineSeries();
		QLineSeries *lineSeriesBP08 = new QLineSeries();
		QLineSeries *lineSeriesBP09 = new QLineSeries();
		QLineSeries *lineSeriesBP10 = new QLineSeries();
		QLineSeries *lineSeriesBP11 = new QLineSeries();
		QLineSeries *lineSeriesBP12 = new QLineSeries();
		QLineSeries *lineSeriesBP13 = new QLineSeries();

		QLineSeries *lineSeriesHR00 = new QLineSeries();
		QLineSeries *lineSeriesHR01 = new QLineSeries();
		QLineSeries *lineSeriesHR02 = new QLineSeries();
		QLineSeries *lineSeriesHR03 = new QLineSeries();
		QLineSeries *lineSeriesHR04 = new QLineSeries();
		QLineSeries *lineSeriesHR05 = new QLineSeries();
		QLineSeries *lineSeriesHR06 = new QLineSeries();
		QLineSeries *lineSeriesHR07 = new QLineSeries();
		QLineSeries *lineSeriesHR08 = new QLineSeries();
		QLineSeries *lineSeriesHR09 = new QLineSeries();
		QLineSeries *lineSeriesHR10 = new QLineSeries();
		QLineSeries *lineSeriesHR11 = new QLineSeries();
		QLineSeries *lineSeriesHR12 = new QLineSeries();
		QLineSeries *lineSeriesHR13 = new QLineSeries();

		QAreaSeries *areaSeriesBPR1 = new QAreaSeries(lineSeriesBP00, lineSeriesBP01);
		QAreaSeries *areaSeriesBPR2 = new QAreaSeries(lineSeriesBP02, lineSeriesBP03);
		QAreaSeries *areaSeriesBPR3 = new QAreaSeries(lineSeriesBP04, lineSeriesBP05);
		QAreaSeries *areaSeriesBPR4 = new QAreaSeries(lineSeriesBP06, lineSeriesBP07);
		QAreaSeries *areaSeriesBPR5 = new QAreaSeries(lineSeriesBP08, lineSeriesBP09);
		QAreaSeries *areaSeriesBPR6 = new QAreaSeries(lineSeriesBP10, lineSeriesBP11);
		QAreaSeries *areaSeriesBPR7 = new QAreaSeries(lineSeriesBP12, lineSeriesBP13);

		QAreaSeries *areaSeriesHRR1 = new QAreaSeries(lineSeriesHR00, lineSeriesHR01);
		QAreaSeries *areaSeriesHRR2 = new QAreaSeries(lineSeriesHR02, lineSeriesHR03);
		QAreaSeries *areaSeriesHRR3 = new QAreaSeries(lineSeriesHR04, lineSeriesHR05);
		QAreaSeries *areaSeriesHRR4 = new QAreaSeries(lineSeriesHR06, lineSeriesHR07);
		QAreaSeries *areaSeriesHRR5 = new QAreaSeries(lineSeriesHR08, lineSeriesHR09);
		QAreaSeries *areaSeriesHRR6 = new QAreaSeries(lineSeriesHR10, lineSeriesHR11);
		QAreaSeries *areaSeriesHRR7 = new QAreaSeries(lineSeriesHR12, lineSeriesHR13);

		QScatterSeries *scatterSeriesBP = new QScatterSeries();
		QScatterSeries *scatterSeriesHR = new QScatterSeries();

		QCategoryAxis *axisBPX1 = new QCategoryAxis();
		QCategoryAxis *axisBPY1 = new QCategoryAxis();
		QCategoryAxis *axisBPX2 = new QCategoryAxis();
		QCategoryAxis *axisBPY2 = new QCategoryAxis();

		QCategoryAxis *axisHRX1 = new QCategoryAxis();
		QCategoryAxis *axisHRY1 = new QCategoryAxis();
		QCategoryAxis *axisHRX2 = new QCategoryAxis();
		QCategoryAxis *axisHRY2 = new QCategoryAxis();

		QChart *chartBP = new QChart();
		QChart *chartHR = new QChart();

		QPrintPreviewDialog *printPreviewDialog;
		QPrintDialog *printDialog;

		int user;
		QString bmi;
		QString measurements, average, timespan;
		struct SETTINGS *settings;
		QVector <struct HEALTHDATA> *view;
		bool darkTheme;

		int percent[MAX_BPM] = { 0 };
		int max_percent = 0;

		void switchChartTheme(bool);

		void prepareBP();
		void prepareHR(bool, int, int, int, int, int, int);

		void printChart(QPrinter*);

private slots:

	void chartSeriesHovered(QPointF, bool);

	void on_pushButton_preview_clicked();
	void on_pushButton_print_clicked();
	void on_pushButton_pdf_clicked();
	void on_pushButton_close_clicked();

	void keyPressEvent(QKeyEvent*);
};

#endif // DLGDISTRIBUTION_H
