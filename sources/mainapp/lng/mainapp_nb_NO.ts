<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nb_NO">
<context>
    <name>DialogAbout</name>
    <message>
        <location filename="../DialogAbout.ui" line="14"/>
        <source>About UBPM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="41"/>
        <source>Version</source>
        <translation type="unfinished">Versjon</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="53"/>
        <source>Universal Blood Pressure Manager</source>
        <translation type="unfinished">Universell blodtrykkshåndterer</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="91"/>
        <source>Qt Framework</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="129"/>
        <source>Operating System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="220"/>
        <source>Environment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="231"/>
        <source>Settings</source>
        <translation type="unfinished">Innstillinger</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="266"/>
        <source>Database</source>
        <translation type="unfinished">Database</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="301"/>
        <source>Cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="336"/>
        <source>Guides</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="371"/>
        <source>Languages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="406"/>
        <source>Plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="441"/>
        <source>Themes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="469"/>
        <source>Translators</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.cpp" line="14"/>
        <source>The program is provided as is with no warranty of any kind, including the warranty of design, merchantability and fitness for a particular purpose.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.cpp" line="37"/>
        <source>Could not open &quot;%1&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DialogAnalysis</name>
    <message>
        <location filename="../DialogAnalysis.ui" line="14"/>
        <source>Data Analysis</source>
        <translation>Dataanalyse</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="24"/>
        <source>Query</source>
        <translation>Spørring</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="43"/>
        <location filename="../DialogAnalysis.cpp" line="209"/>
        <source>Results</source>
        <translation>Resultater</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="82"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="91"/>
        <source>Time</source>
        <translation>Tid</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="100"/>
        <source>Systolic</source>
        <translation>Systolisk</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="109"/>
        <source>Diastolic</source>
        <translation>Diastolisk</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="118"/>
        <source>P.Pressure</source>
        <translation>Pulstrykk</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="127"/>
        <source>Heartrate</source>
        <translation>Puls</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="136"/>
        <source>Irregular</source>
        <translation>Uregelmessig</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="145"/>
        <source>Movement</source>
        <translation>Bevegelse</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="154"/>
        <source>Invisible</source>
        <translation>Usynlig</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="163"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="193"/>
        <source>Close</source>
        <translation>Lukk</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="51"/>
        <source>Could not create memory database!

%1</source>
        <translation>Kunne ikke opprette minnedatabase!

%1</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>No results for this query found!</source>
        <translation>Resultatløst.</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>%1 for %2 [ %3 %4 | %5 %6 | %7% ]</source>
        <translation>%1 for %2 [ %3 %4 | %5 %6 | %7% ]</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Result(s)</source>
        <translation>
            <numerusform>Resultat</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Match(es)</source>
        <translation type="unfinished">
            <numerusform>Treff</numerusform>
            <numerusform>Treff</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Record(s)</source>
        <translation>
            <numerusform>Oppføring</numerusform>
            <numerusform>Oppføringer</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>User %1</source>
        <translation>Bruker %1</translation>
    </message>
</context>
<context>
    <name>DialogDistribution</name>
    <message>
        <location filename="../DialogDistribution.ui" line="20"/>
        <source>Data Distribution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="77"/>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="94"/>
        <source>Print</source>
        <translation type="unfinished">Skriv ut</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="128"/>
        <source>Close</source>
        <translation type="unfinished">Lukk</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogDistribution.cpp" line="18"/>
        <source>Record(s)</source>
        <translation type="unfinished">
            <numerusform>Oppføring</numerusform>
            <numerusform>Oppføringer</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="113"/>
        <source>Low</source>
        <translation type="unfinished">Lav</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="115"/>
        <source>Optimal</source>
        <translation type="unfinished">Optimalt</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="117"/>
        <source>Normal</source>
        <translation type="unfinished">Normalt</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="119"/>
        <source>High Normal</source>
        <translation type="unfinished">Høy normal</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="121"/>
        <source>Hyper 1</source>
        <translation type="unfinished">Hyper 1</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="123"/>
        <source>Hyper 2</source>
        <translation type="unfinished">Hyper 2</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="125"/>
        <source>Hyper 3</source>
        <translation type="unfinished">Hyper 3</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="142"/>
        <source>SYS</source>
        <translation type="unfinished">SYS</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="154"/>
        <source>DIA</source>
        <translation type="unfinished">DIA</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="165"/>
        <source>Blood Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="203"/>
        <source>Athlete</source>
        <translation type="unfinished">Atlet</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="205"/>
        <source>Excellent</source>
        <translation type="unfinished">Utmerket</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="207"/>
        <source>Great</source>
        <translation type="unfinished">Veldig bra</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="209"/>
        <source>Good</source>
        <translation type="unfinished">Bra</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="211"/>
        <source>Average</source>
        <translation type="unfinished">Gjennomsnitt</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="213"/>
        <source>Below Average</source>
        <translation type="unfinished">Under gjennomsnitt</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="215"/>
        <source>Poor</source>
        <translation type="unfinished">Dårlig</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="232"/>
        <source>BPM</source>
        <translation type="unfinished">SPM</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="258"/>
        <source>Heart Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="111"/>
        <location filename="../DialogDistribution.cpp" line="419"/>
        <source>Export To PDF</source>
        <translation type="unfinished">Eksporter som PDF</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="419"/>
        <source>%1 File (*.%2)</source>
        <translation type="unfinished">%1 fil (*.%2)</translation>
    </message>
</context>
<context>
    <name>DialogDonation</name>
    <message>
        <location filename="../DialogDonation.ui" line="14"/>
        <source>Donation</source>
        <translation type="unfinished">Donasjon</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="67"/>
        <source>Copy E-Mail and open Amazon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="154"/>
        <source>Open PayPal (+)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="171"/>
        <source>Open PayPal (-)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="46"/>
        <source>If you have an Amazon account, you can send money by gift card.

Click the button below to open the Amazon website in your browser and

- sign in to your account
- select or enter the amount you would like to send
- select e-mail delivery
- paste the e-mail address as the recipient
- add a message
- complete the donation

You can also scan the QR code with your mobile device.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="124"/>
        <source>If you have a PayPal account, you can send money using the &quot;Friends and Family&quot; feature.

Open your PayPal app and

- search for @LazyT (Thomas Löwe)
- enter the amount you would like to send
- add a message
- complete the donation

You can also click the first button below to do this in your browser without using the app.

If you do not have a PayPal account but do have a credit/debit card click the second button below to go to the PayPal donation page and

- select onetime or repeating donation
- select or enter the amount you would like to send
- optionally check the box to cover the fees
- complete the donation

You can also scan the QR code with your mobile device.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="230"/>
        <source>If you are in the Euro Payments Area, you can transfer money from your bank account.

Click the buttons below to copy the name/IBAN/BIC to the clipboard and

- sign in to your bank account
- paste the name/IBAN/BIC into the transfer form
- enter the amount you would like to send
- complete the donation

You can also scan the QR code with your mobile device.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="251"/>
        <source>Copy Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="268"/>
        <source>Copy IBAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="285"/>
        <source>Copy BIC</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DialogHelp</name>
    <message>
        <location filename="../DialogHelp.ui" line="20"/>
        <source>Guide</source>
        <translation>Veiledning</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="61"/>
        <source>%1 guide not found, showing EN guide instead.</source>
        <translation>%1-veiledning ble ikke funnet. Viser EN-versjon istedenfor.</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="53"/>
        <source>%1 guide not found!</source>
        <translation>Fant ikke %1-veiledning!</translation>
    </message>
</context>
<context>
    <name>DialogMigration</name>
    <message>
        <location filename="../DialogMigration.ui" line="14"/>
        <source>Data Migration</source>
        <translation>Dataflytting</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="24"/>
        <source>Source</source>
        <translation>Kilde</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="72"/>
        <source>Open Data Source</source>
        <translation>Åpne datakilde</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="97"/>
        <source>Parameters</source>
        <translation>Parameter</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="217"/>
        <source>Date Format</source>
        <translation>Dataformat</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="267"/>
        <source>Irregular Detection</source>
        <translation>Oppdagelse av uregelmessigheter</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="167"/>
        <source>Startline</source>
        <translation>Startlinje</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="183"/>
        <source>Elements per Line</source>
        <translation>Elementer per linje</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="111"/>
        <source>Delimiter</source>
        <translation>Skilletegn</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="303"/>
        <source>Time Format</source>
        <translation>Tidsformat</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="199"/>
        <source>Movement Detection</source>
        <translation>Bevegelsesoppdagelse</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="239"/>
        <source>Date Language</source>
        <translation type="unfinished">Datospråk</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="344"/>
        <source>Positions</source>
        <translation>Posisjoner</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="488"/>
        <source>Irregular</source>
        <translation type="unfinished">Uregelmessig</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="504"/>
        <source>Date</source>
        <translation type="unfinished">Dato</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="452"/>
        <source>Systolic</source>
        <translation type="unfinished">Systolisk</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="410"/>
        <source>Heartrate</source>
        <translation type="unfinished">Puls</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="394"/>
        <source>Movement</source>
        <translation type="unfinished">Bevegelse</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="329"/>
        <source>Ignore Comment</source>
        <translation>Ignorer kommentar</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="368"/>
        <source>Time</source>
        <translation type="unfinished">Tid</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="436"/>
        <source>Diastolic</source>
        <translation type="unfinished">Diastolisk</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="550"/>
        <source>Comment</source>
        <translation type="unfinished">Kommentar</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="567"/>
        <source>Result</source>
        <translation>Resultat</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="786"/>
        <source>Migrate User 1</source>
        <translation>Flytt bruker 1</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="815"/>
        <source>Migrate User 2</source>
        <translation>Flytt bruker 2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="841"/>
        <source>Copy to Custom</source>
        <translation>Kopier til egendefinert</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="861"/>
        <source>Select predefined settings</source>
        <translation>Velg forhåndsdefinerte innstillinger</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="895"/>
        <source>Custom</source>
        <translation>Egendefinert</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="906"/>
        <source>Test Data Migration</source>
        <translation>Test dataflytting</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="920"/>
        <source>Start Data Migration</source>
        <translation>Start dataflytting</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="251"/>
        <source>Choose Data Source for Migration</source>
        <translation>Velg datakilde å flytte</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="251"/>
        <source>CSV File (*.csv)</source>
        <translation type="unfinished">CSV-fil (*.csv)</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="304"/>
        <source>%L1 Bytes</source>
        <translation>%L1 byte</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="305"/>
        <source>%1 Lines</source>
        <translation>%1 linjer</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="319"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation type="unfinished">Kunne ikke åpne «%1».

Grunn: %2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="341"/>
        <source>Startline can&apos;t be empty.</source>
        <translation>Startlinjen kan ikke være tom.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="347"/>
        <source>Startline must be smaller than the number of lines.</source>
        <translation>Startlinjen må være mindre enn antall linjer.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="355"/>
        <source>Line %1 = %2</source>
        <translation>Linje %1 = %2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="360"/>
        <source>Delimiter can&apos;t be empty.</source>
        <translation>Skilletegn kan ikke være tomt.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="367"/>
        <source>Startline doesn&apos;t contain &quot;%1&quot; delimiter.</source>
        <translation>Startlinjen inneholder ikke et «%1»-skilletegn.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="378"/>
        <source>Elements can&apos;t be empty.</source>
        <translation>Elementer kan ikke være tomme.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="384"/>
        <source>Elements can&apos;t be smaller than 4.</source>
        <translation>Elementer kan ikke være mindre enn 4.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="390"/>
        <source>Elements doesn&apos;t match (found %1).</source>
        <translation>Elementer samsvarer ikke (fant %1).</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="397"/>
        <source>Date format can&apos;t be empty.</source>
        <translation>Datoformatet kan ikke være tomt.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="403"/>
        <source>Single date format must also contain a time format.</source>
        <translation type="unfinished">Enkeltvis datoformat må også inneholde et tidsformat.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="410"/>
        <source>Date position can&apos;t be empty.</source>
        <translation>Datoposisjon kan ikke være tom.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="416"/>
        <source>Date position must be smaller than elements.</source>
        <translation>Datoposisjon må være mindre enn elementene.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="423"/>
        <source>Time position requires also a time parameter.</source>
        <translation>Tidsposisjon krever også et tidsparameter.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="429"/>
        <source>Time format requires also a time position.</source>
        <translation type="unfinished">Tidsformatet krever også en tidsposisjon.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="435"/>
        <source>Time position must be smaller than elements.</source>
        <translation>Tidsposisjon må være mindre enn elementene.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="442"/>
        <source>Systolic position can&apos;t be empty.</source>
        <translation>Diastolisk posisjon kan ikke være tom.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="448"/>
        <source>Systolic position must be smaller than elements.</source>
        <translation>Systolisk posisjon må være mindre enn elementene.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="455"/>
        <source>Diastolic position can&apos;t be empty.</source>
        <translation>Diastolisk posisjon kan ikke være tom.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="461"/>
        <source>Diastolic position must be smaller than elements.</source>
        <translation>Diastolisk posisjon må være mindre enn elementene.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="468"/>
        <source>Heartrate position can&apos;t be empty.</source>
        <translation>Pulsposisjon kan ikke være tom.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="474"/>
        <source>Heartrate position must be smaller than elements.</source>
        <translation>Pulsposisjon må være mindre enn elementene.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="481"/>
        <source>Irregular position must be smaller than elements.</source>
        <translation>Uregelmessig-posisjon må være mindre enn elementene.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="487"/>
        <source>Irregular position requires also an irregular parameter.</source>
        <translation>Uregelmessig posisjon krever også et uregelmessig-parameter.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="494"/>
        <source>Movement position must be smaller than elements.</source>
        <translation>Bevegelsesposisjon må være mindre enn elementene.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="500"/>
        <source>Movement position requires also a movement parameter.</source>
        <translation>Bevegelsesposisjon krever også et bevegelsesparameter.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="507"/>
        <source>Comment position must be smaller than elements.</source>
        <translation type="unfinished">Kommentarposisjon må være mindre enn elementene.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="523"/>
        <source>Positions can&apos;t exist twice.</source>
        <translation type="unfinished">Posisjoner kan ikke finnes på flere steder.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="539"/>
        <source>Date format is invalid.</source>
        <translation>Datoformatet er ugyldig.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="546"/>
        <source>Time format is invalid.</source>
        <translation>Tidsformatet er ugyldig.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="556"/>
        <source>Date/Time format is invalid.</source>
        <translation>Dato/tids-formatet er ugyldig.</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="747"/>
        <source>Successfully migrated %n record(s) for user %1.</source>
        <translation type="unfinished">
            <numerusform>Flyttet %n oppføring for bruker %1.</numerusform>
            <numerusform>Flyttet %n oppføringer for bruker %1.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="751"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>Hoppet over %n ugyldig oppføring.</numerusform>
            <numerusform>Hoppet over %n ugyldige oppføringer.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="756"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>Hoppet over %n duplisert oppføring.</numerusform>
            <numerusform>Hoppet over %n dupliserte oppføring.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="761"/>
        <source>Skipped %n hidden record(s)!</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogRecord</name>
    <message>
        <location filename="../DialogRecord.ui" line="14"/>
        <source>Manual Record</source>
        <translation>Manuell oppføring</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="27"/>
        <source>Data Record</source>
        <translation>Dataoppføring</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="35"/>
        <location filename="../DialogRecord.ui" line="211"/>
        <location filename="../DialogRecord.cpp" line="9"/>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>Add Record For %1</source>
        <translation>Legg til oppføring for %1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="78"/>
        <source>Select Date &amp; Time</source>
        <translation>Velg dato og tid</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="129"/>
        <source>Enter SYS</source>
        <translation>Skriv inn SYS</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="148"/>
        <source>Enter DIA</source>
        <translation>Skriv inn DIA</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="167"/>
        <source>Enter BPM</source>
        <translation>Skriv inn SPM</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="102"/>
        <source>Irregular Heartbeat</source>
        <translation>Uregelmessig hjertefrekvens</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="186"/>
        <source>Movement</source>
        <translation>Bevegelse</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="239"/>
        <source>Enter Optional Comment</source>
        <translation>Tast inn Valgfri Kommentar</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="269"/>
        <source>Show message on successful create / delete / modify record</source>
        <translation>Vis melding ved opprettelse, sletting, og endring av oppføring</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="287"/>
        <source>Delete</source>
        <translation>Slett</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="304"/>
        <source>Create</source>
        <translation>Opprett</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="324"/>
        <source>Close</source>
        <translation>Lukk</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="9"/>
        <source>User 1</source>
        <translation>Bruker 1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>User 2</source>
        <translation>Bruker 2</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="34"/>
        <location filename="../DialogRecord.cpp" line="187"/>
        <source>Modify</source>
        <translation>Endre</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="132"/>
        <source>The data record could not be deleted!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>Dataoppføringen kunne ikke slettes.

Det finnes ingen oppføring for denne datoen og tiden.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="138"/>
        <source>Data record successfully deleted.</source>
        <translation>Dataoppføring slettet.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="150"/>
        <source>Please enter a valid value for &quot;SYS&quot; first!</source>
        <translation>Skriv inn gyldig verdi for «SYS» først!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="160"/>
        <source>Please enter a valid value for &quot;DIA&quot; first!</source>
        <translation>Skriv inn en gyldig verdi for «DIA» først!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="169"/>
        <source>Please enter a valid value for &quot;BPM&quot; first!</source>
        <translation>Skriv inn en gyldig verdi for «SPM» først!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="191"/>
        <source>The data record could not be modified!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>Dataoppføringen kunne ikke endres.

Det finnes ingen oppføring for denne datoen og tiden.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="197"/>
        <source>Data Record successfully modified.</source>
        <translation>Dataoppføring endret.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="206"/>
        <source>The data record could not be created!

An entry for this date &amp; time already exist.</source>
        <translation>Dataoppføringen kunne ikke opprettes.

Det finnes ingen oppføring for denne datoen og tiden.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="212"/>
        <source>Data Record successfully created.</source>
        <translation>Dataoppføring opprettet.</translation>
    </message>
</context>
<context>
    <name>DialogSettings</name>
    <message>
        <location filename="../DialogSettings.cpp" line="183"/>
        <source>Choose Database Location</source>
        <translation>Velg databaseplassering</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="193"/>
        <source>Choose Database Backup Location</source>
        <translation type="unfinished">Velg sted for lagring av databasesikkerhetskopi</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="380"/>
        <source>The selected database already exists.

Please choose the preferred action.</source>
        <translation type="unfinished">Valgt database finnes allerede.

Velg fortrukket handling.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="382"/>
        <source>Overwrite</source>
        <translation>Overskriv</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="383"/>
        <source>Merge</source>
        <translation>Flett</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="384"/>
        <source>Swap</source>
        <translation>Bytt</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="386"/>
        <source>Overwrite:

Overwrites the selected database with the current database.

Merge:

Merges the selected database into the current database.

Swap:

Deletes the current database and use the selected database.</source>
        <translation>Overskriv:

Overskriver valgt database med nåværende database.

Flett:

Fletter valgt database inn i nåværende database.

Bytt:

Sletter valgt database og bruker valgt database.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="394"/>
        <source>The current database is empty.

The selected database will be erased on exit, if no data will be added.</source>
        <translation type="unfinished">Nåværende database er tom.

Valgt database vil bli slettet når programmet lukkes, hvis ingen data skal legges til.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="425"/>
        <source>SQL encryption can&apos;t be enabled without password and will be disabled!</source>
        <translation>SQL-kryptering kan ikke skrus på uten passord, og vil bli skrudd av!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="432"/>
        <source>The database backup should be located on a different hard disk, partition or directory.</source>
        <translation type="unfinished">Databasesikkerhetskopien bør ligge på en annen fysisk enhet.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="442"/>
        <source>Please enter valid values for additional information of user 1!</source>
        <translation>Skriv inn gyldig ytterligere info for bruker 1!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="449"/>
        <source>Please enter valid values for additional information of user 2!</source>
        <translation>Skriv inn gyldig ytterligere info for bruker 2.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="456"/>
        <source>Entered age doesn&apos;t match selected age group for user 1!</source>
        <translation>Innskrevet alder samsvarer ikke med valgt aldersgruppe for bruker 1.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="463"/>
        <source>Entered age doesn&apos;t match selected age group for user 2!</source>
        <translation>Innskrevet alder samsvarer ikke med valgt aldersgruppe for bruker 2!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="471"/>
        <source>Please enable symbols or lines for chart!</source>
        <translation type="unfinished">Skru på symboler eller linjer for diagrammet.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="478"/>
        <source>Please enter a valid e-mail address!</source>
        <translation type="unfinished">Skriv inn en gyldig e-postadresse.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="485"/>
        <source>Please enter a e-mail subject!</source>
        <translation type="unfinished">Velg et emne.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="492"/>
        <source>E-Mail message must contain $CHART, $TABLE and/or $STATS!</source>
        <translation type="unfinished">E-postmeldingen må inneholde $CHART, $TABLE, og/eller $STATS.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="591"/>
        <source>The auto hide setting has been changed.

Should this now be applied to all existing measurements?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="596"/>
        <source>Auto hide measurements:

     User 1 : %1 %3
     User 2 : %2 %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="596"/>
        <source>hidden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="596"/>
        <source>shown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="647"/>
        <source>User 1</source>
        <translation>Bruker 1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="661"/>
        <source>User 2</source>
        <translation>Bruker 2</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="785"/>
        <source>Blood Pressure Report</source>
        <translation>Blodtrykksrapport</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="786"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Kjære lege,

Vedlagt finner du blodtrykksdata fra meg for denne måneden.

Hilsen,
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="838"/>
        <source>Abort setup and discard all changes?</source>
        <translation>Avbryt oppsett og forkast alle endringer?</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="20"/>
        <source>Settings</source>
        <translation>Innstillinger</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="44"/>
        <source>Database</source>
        <translation>Database</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="75"/>
        <source>Location</source>
        <translation>Plassering</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="81"/>
        <source>Current Location</source>
        <translation>Nåværende plassering</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="97"/>
        <source>Change Location</source>
        <translation>Endre plassering</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="123"/>
        <source>Encrypt With SQLCipher</source>
        <translation>Krypter med SQLCipher</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="126"/>
        <source>Encryption</source>
        <translation>Kryptering</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="138"/>
        <source>Password</source>
        <translation>Passord</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="151"/>
        <source>Show Password</source>
        <translation>Vis passord</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="454"/>
        <source>User</source>
        <translation>Bruker</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="489"/>
        <location filename="../DialogSettings.ui" line="752"/>
        <source>Male</source>
        <translation>Mann</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="518"/>
        <location filename="../DialogSettings.ui" line="781"/>
        <source>Female</source>
        <translation>Kvinne</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="614"/>
        <location filename="../DialogSettings.ui" line="886"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="483"/>
        <location filename="../DialogSettings.ui" line="746"/>
        <source>Mandatory Information</source>
        <translation>Obligatorisk info</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="544"/>
        <location filename="../DialogSettings.ui" line="816"/>
        <source>Age Group</source>
        <translation>Aldersgruppe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="630"/>
        <location filename="../DialogSettings.ui" line="902"/>
        <source>Additional Information</source>
        <translation>Ytterligere info</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="690"/>
        <location filename="../DialogSettings.ui" line="962"/>
        <source>Height</source>
        <translation>Høyde</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="719"/>
        <location filename="../DialogSettings.ui" line="991"/>
        <source>Weight</source>
        <translation>Vekt</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1016"/>
        <source>Device</source>
        <translation>Enhet</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1022"/>
        <location filename="../DialogSettings.cpp" line="77"/>
        <source>Import Plugins [ %1 ]</source>
        <translation>Importer programtillegg [ %1 ]</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1070"/>
        <source>Open Website</source>
        <translation>Åpne nettside</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1083"/>
        <source>Maintainer</source>
        <translation>Vedlikeholder</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1111"/>
        <source>Version</source>
        <translation>Versjon</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1118"/>
        <source>Send E-Mail</source>
        <translation>Send e-post</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1131"/>
        <source>Model</source>
        <translation>Modell</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1138"/>
        <source>Producer</source>
        <translation>Produsent</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1155"/>
        <source>Chart</source>
        <translation>Diagram</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1282"/>
        <source>X-Axis Range</source>
        <translation>X-akseverdier</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1288"/>
        <source>Dynamic Scaling</source>
        <translation>Dynamisk skalering</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1269"/>
        <source>Healthy Ranges</source>
        <translation>Sunne verdispenn</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1186"/>
        <source>Symbols</source>
        <translation>Symboler</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1263"/>
        <source>Colored Areas</source>
        <translation>Fargelagte områder</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1035"/>
        <source>Please choose Device Plugin…</source>
        <translation>Velg enhetsprogramtillegg …</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="338"/>
        <location filename="../DialogSettings.ui" line="1410"/>
        <location filename="../DialogSettings.ui" line="1685"/>
        <source>Systolic</source>
        <translation>Systolisk</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="347"/>
        <location filename="../DialogSettings.ui" line="1498"/>
        <location filename="../DialogSettings.ui" line="1773"/>
        <source>Diastolic</source>
        <translation>Diastolisk</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="356"/>
        <location filename="../DialogSettings.ui" line="1586"/>
        <location filename="../DialogSettings.ui" line="1861"/>
        <source>Heartrate</source>
        <translation>Puls</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1958"/>
        <source>Table</source>
        <translation>Tabell</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2043"/>
        <location filename="../DialogSettings.ui" line="2226"/>
        <source>Systolic Warnlevel</source>
        <translation>Systolisk advarselsnivå</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1989"/>
        <location filename="../DialogSettings.ui" line="2286"/>
        <source>Diastolic Warnlevel</source>
        <translation>Diastolisk advarselsnivå</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="176"/>
        <source>Automatic Backup</source>
        <translation>Automatisk sikkerhetskopi</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="179"/>
        <source>Backup</source>
        <translation type="unfinished">Sikkerhetskopi</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="196"/>
        <source>Current Backup Location</source>
        <translation type="unfinished">Nåværende sted for lagring av sikkerhetskopi</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="209"/>
        <source>Change Backup Location</source>
        <translation type="unfinished">Endre sted for lagring av sikkerhetskopi</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="236"/>
        <source>Backup Mode</source>
        <translation>Sikkerhetskopimodus</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="246"/>
        <source>Daily</source>
        <translation type="unfinished">Daglig</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="255"/>
        <source>Weekly</source>
        <translation type="unfinished">Ukentlig</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="264"/>
        <source>Monthly</source>
        <translation type="unfinished">Månedlig</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="276"/>
        <source>Keep Copies</source>
        <translation>Behold kopier</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="311"/>
        <source>Auto Hide Measurements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="322"/>
        <source>Detection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="329"/>
        <source>Sum of All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="371"/>
        <source>Hide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="378"/>
        <source>Higher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="387"/>
        <source>Lower</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="404"/>
        <source>Time Span</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="658"/>
        <location filename="../DialogSettings.ui" line="930"/>
        <source>Birthday</source>
        <translation>Geburtsdag</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1195"/>
        <source>Color</source>
        <translation>Farge</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1219"/>
        <location filename="../DialogSettings.ui" line="1247"/>
        <source>Size</source>
        <translation>Størrelse</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1301"/>
        <source>Lines</source>
        <translation>Linjer</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1310"/>
        <location filename="../DialogSettings.ui" line="1338"/>
        <source>Width</source>
        <translation>Bredde</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1367"/>
        <source>Show Heartrate</source>
        <translation>Vis puls</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1373"/>
        <source>Show Heartrate in Chart View</source>
        <translation type="unfinished">Vis puls i diagramvisning</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1383"/>
        <source>Print Heartrate</source>
        <translation>Skriv ut puls</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1389"/>
        <source>Print Heartrate on separate Sheet</source>
        <translation>Skriv puls på eget ark</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2103"/>
        <location filename="../DialogSettings.ui" line="2340"/>
        <source>P.Pressure Warnlevel</source>
        <translation>Pulstrykk-advarselsnivå</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2157"/>
        <location filename="../DialogSettings.ui" line="2394"/>
        <source>Heartrate Warnlevel</source>
        <translation>Puls-advarselsnivå</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2459"/>
        <source>Statistic</source>
        <translation>Statistikk</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2465"/>
        <source>Bar Type</source>
        <translation>Stolpetype</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2471"/>
        <source>Show Median instead of Average Bars</source>
        <translation>Vis median istedenfor gjennonsnittsstopler</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2481"/>
        <source>Legend Type</source>
        <translation>Verditype</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2487"/>
        <source>Show Values as Legend instead of Descriptions</source>
        <translation>Vis verdier og verdityper istedenfor beskrivelser</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2502"/>
        <source>Print</source>
        <translation type="unfinished">Skriv ut</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2510"/>
        <source>Additional Colums</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2516"/>
        <source>Pulse Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2539"/>
        <source>Irregular Heartbeat</source>
        <translation type="unfinished">Uregelmessig hjertefrekvens</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2562"/>
        <source>Movement</source>
        <translation type="unfinished">Bevegelse</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2588"/>
        <source>Page Orientation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2594"/>
        <source>Portrait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2604"/>
        <source>Landscape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2616"/>
        <source>Header Appearance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2622"/>
        <source>Show Column Headers as Graphics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2640"/>
        <source>E-Mail</source>
        <translation type="unfinished">E-post</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2648"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2667"/>
        <source>Subject</source>
        <translation>Emne</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2685"/>
        <source>Message</source>
        <translation>Melding</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2706"/>
        <source>Plugin</source>
        <translation>Programtillegg</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2712"/>
        <source>General</source>
        <translation>Generelt</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2718"/>
        <source>Log Device Communication to File</source>
        <translation>Loggfør enhetskommunikasjon til fil</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2725"/>
        <source>Import Measurements automatically</source>
        <translation>Importer målinger automatisk</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2735"/>
        <source>Bluetooth</source>
        <translation>Blåtann</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2741"/>
        <source>Discover Device automatically</source>
        <translation>Oppdag enhet automatisk</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2748"/>
        <source>Connect Device automatically</source>
        <translation>Koble til enhet automatisk</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2763"/>
        <source>Update</source>
        <translation>Oppgradering</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2769"/>
        <source>Autostart</source>
        <translation>Autostart</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2775"/>
        <source>Check for Online Updates at Program Startup</source>
        <translation>Se etter nye versjoner på nett ved programoppstart</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2788"/>
        <source>Notification</source>
        <translation>Merknad</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2794"/>
        <source>Always show Result after Online Update Check</source>
        <translation>Alltid vis resultat for sjekk av nye versjoner</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2809"/>
        <source>Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2817"/>
        <source>Date / Short</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2843"/>
        <source>Date / Long</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2873"/>
        <source>Time / Short</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2899"/>
        <source>Time / Long</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2933"/>
        <source>Save</source>
        <translation>Lagre</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2950"/>
        <source>Reset</source>
        <translation>Tilbakestill</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2967"/>
        <source>Close</source>
        <translation>Lukk</translation>
    </message>
</context>
<context>
    <name>DialogUpdate</name>
    <message>
        <location filename="../DialogUpdate.ui" line="14"/>
        <source>Online Update</source>
        <translation>Nettbasert oppgradering</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="27"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="80"/>
        <source>Available Version</source>
        <translation>Tilgjengelig versjon</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="90"/>
        <source>Update File Size</source>
        <translation>Oppgraderings-filstørrelse</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="104"/>
        <source>Installed Version</source>
        <translation>Installert versjon</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="156"/>
        <source>Details</source>
        <translation>Detaljer</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="181"/>
        <location filename="../DialogUpdate.ui" line="202"/>
        <source>Download</source>
        <translation>Last ned</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="222"/>
        <source>Ignore</source>
        <translation>Ignorer</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="183"/>
        <source>No new version found.</source>
        <translation>Fant ingen ny versjon.</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogUpdate.cpp" line="42"/>
        <source>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problem(s):

%1
Do you wish to continue anyway?</source>
        <translation>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="73"/>
        <source>Downloading update failed!

%1</source>
        <translation>Kunne ikke laste ned oppgradering.

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="73"/>
        <source>Checking update failed!

%1</source>
        <translation>Kunne ikke se etter nye versjoner.

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="156"/>
        <source>Unexpected response from update server!</source>
        <translation>Uventet svar fra oppgraderingstjener.</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="173"/>
        <source>*%1 not found</source>
        <translation type="unfinished">*Fant ikke %1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="203"/>
        <source>Update doesn&apos;t have expected size!

%L1 : %L2

Retry download…</source>
        <translation>Den nye versjonen har ikke forventet størrelse.

%L1 : %L2

Prøver å laste ned igjen …</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="207"/>
        <source>Update saved to %1.

Start new version now?</source>
        <translation>Oppgradering lagret til %1.

Start ny versjon nå?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="220"/>
        <source>Could not start new version!</source>
        <translation>Kunne ikke starte ny versjon!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="247"/>
        <source>The program was installed by Flatpak.

Please update using the Flatpak internal update function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="253"/>
        <source>The program was installed by Snap.

Please update using the Snap internal update function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="259"/>
        <source>The program was installed from Windows store.

Please update using the store internal update function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="265"/>
        <source>The program was installed from distribution.

Please update using the operating systems internal update function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="271"/>
        <source>The program was installed from source code.

Please update the sources, rebuild and reinstall manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="297"/>
        <source>Really abort download?</source>
        <translation>Avbryt nedlastingen?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="227"/>
        <source>Could not save update to %1!

%2</source>
        <translation>Kunne ikke lagre oppdatering til %1.

%2</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="14"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Universell blodtrykkshåndterer</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="30"/>
        <source>Show Previous Period</source>
        <translation type="unfinished">Vis forrige tidsperiode</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="104"/>
        <source>Show Next Period</source>
        <translation type="unfinished">Vis neste tidsperiode</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="143"/>
        <source>Chart View</source>
        <translation>Diagramvisning</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="171"/>
        <source>Table View</source>
        <translation>Tabellvisning</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="211"/>
        <location filename="../MainWindow.cpp" line="2608"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="220"/>
        <location filename="../MainWindow.cpp" line="2609"/>
        <source>Time</source>
        <translation>Tid</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="229"/>
        <location filename="../MainWindow.cpp" line="159"/>
        <location filename="../MainWindow.cpp" line="5049"/>
        <source>Systolic</source>
        <translation>Systolisk</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="238"/>
        <location filename="../MainWindow.cpp" line="160"/>
        <location filename="../MainWindow.cpp" line="5050"/>
        <source>Diastolic</source>
        <translation>Diastolisk</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="247"/>
        <source>P.Pressure</source>
        <translation>Pulstrykk</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="265"/>
        <source>Irregular</source>
        <translation>Uregelmessig (hjertearytmi)</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="274"/>
        <source>Movement</source>
        <translation>Bevegelse</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="283"/>
        <source>Invisible</source>
        <translation>Usynlig</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="292"/>
        <location filename="../MainWindow.cpp" line="2628"/>
        <location filename="../MainWindow.cpp" line="2641"/>
        <location filename="../MainWindow.cpp" line="2655"/>
        <location filename="../MainWindow.cpp" line="2669"/>
        <location filename="../MainWindow.cpp" line="2683"/>
        <location filename="../MainWindow.cpp" line="2698"/>
        <location filename="../MainWindow.cpp" line="2714"/>
        <location filename="../MainWindow.cpp" line="2728"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="309"/>
        <source>Statistic View</source>
        <translation>Statisk visning</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="404"/>
        <source>¼ Hourly</source>
        <translation>Hvert kvarter</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="439"/>
        <source>½ Hourly</source>
        <translation>Hver halvtime</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="471"/>
        <source>Hourly</source>
        <translation>Hver time</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="506"/>
        <source>¼ Daily</source>
        <translation>¼-daglig</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="538"/>
        <source>½ Daily</source>
        <translation>Halvdaglig</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="570"/>
        <source>Daily</source>
        <translation>Daglig</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="605"/>
        <source>3 Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="637"/>
        <source>Weekly</source>
        <translation>Ukentlig</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="669"/>
        <source>Monthly</source>
        <translation>Månedlig</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="701"/>
        <source>Quarterly</source>
        <translation>Kvartalsvis</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="733"/>
        <source>½ Yearly</source>
        <translation>Halvårlig</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="765"/>
        <source>Yearly</source>
        <translation>Årlig</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="825"/>
        <source>Last 3 Days</source>
        <translation type="unfinished">Siste 28 dager {3 ?}</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="857"/>
        <source>Last 7 Days</source>
        <translation>Siste 7 dager</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="889"/>
        <source>Last 14 Days</source>
        <translation>Siste 14 dager</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="921"/>
        <source>Last 21 Days</source>
        <translation>Siste 21 dager</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="953"/>
        <source>Last 28 Days</source>
        <translation>Siste 28 dager</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="985"/>
        <source>Last 3 Months</source>
        <translation>Siste 3 måneder</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1017"/>
        <source>Last 6 Months</source>
        <translation>Siste 6 måneder</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1049"/>
        <source>Last 9 Months</source>
        <translation>Siste 9 måneder</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1081"/>
        <source>Last 12 Months</source>
        <translation>Siste 12 måneder</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1113"/>
        <source>All Records</source>
        <translation>Alle oppføringer</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1160"/>
        <source>File</source>
        <translation>Fil</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1164"/>
        <source>Print</source>
        <translation>Skriv ut</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1186"/>
        <source>Help</source>
        <translation>Hjelp</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1203"/>
        <source>Configuration</source>
        <translation>Oppsett</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1207"/>
        <source>Theme</source>
        <translation>Drakt</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1216"/>
        <source>Language</source>
        <translation>Språk</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1225"/>
        <source>Style</source>
        <translation>Stil</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1234"/>
        <source>Charts</source>
        <translation>Diagrammer</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1259"/>
        <source>Database</source>
        <translation>Database</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1263"/>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1275"/>
        <source>Export</source>
        <translation>Eksporter</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1290"/>
        <source>Clear</source>
        <translation>Tøm</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1379"/>
        <source>Quit</source>
        <translation>Avslutt</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1382"/>
        <location filename="../MainWindow.ui" line="1385"/>
        <source>Quit Program</source>
        <translation>Avslutt programmet</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1397"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1400"/>
        <location filename="../MainWindow.ui" line="1403"/>
        <source>About Program</source>
        <translation>Om programmet</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1412"/>
        <source>Guide</source>
        <translation>Veiledning</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1415"/>
        <location filename="../MainWindow.ui" line="1418"/>
        <source>Show Guide</source>
        <translation>Vis veiledning</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1427"/>
        <source>Update</source>
        <translation>Oppgradering</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1430"/>
        <location filename="../MainWindow.ui" line="1433"/>
        <source>Check Update</source>
        <translation>Se etter oppgradring</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1592"/>
        <source>To PDF</source>
        <translation>Til PDF</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1595"/>
        <location filename="../MainWindow.ui" line="1598"/>
        <source>Export To PDF</source>
        <translation>Eksporter som PDF</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1607"/>
        <source>Migration</source>
        <translation>Flytting</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1610"/>
        <location filename="../MainWindow.ui" line="1613"/>
        <source>Migrate from Vendor</source>
        <translation>Flytt fra fabrikant</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1857"/>
        <source>Translation</source>
        <translation>Oversettelse</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1860"/>
        <location filename="../MainWindow.ui" line="1863"/>
        <source>Contribute Translation</source>
        <translation>Bistå oversettelsen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1872"/>
        <source>E-Mail</source>
        <translation type="unfinished">E-post</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1875"/>
        <location filename="../MainWindow.ui" line="1878"/>
        <source>Send E-Mail</source>
        <translation>Send e-post</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1890"/>
        <source>Icons</source>
        <translation>Ikoner</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1893"/>
        <location filename="../MainWindow.ui" line="1896"/>
        <source>Change Icon Color</source>
        <translation>Endre ikonfarge</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1907"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1910"/>
        <location filename="../MainWindow.ui" line="1913"/>
        <source>System Colors</source>
        <translation>Systemfarger</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1921"/>
        <source>Light</source>
        <translation type="unfinished">Lyse</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1924"/>
        <location filename="../MainWindow.ui" line="1927"/>
        <source>Light Colors</source>
        <translation>Lyse farger</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1935"/>
        <source>Dark</source>
        <translation type="unfinished">Mørke</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1938"/>
        <location filename="../MainWindow.ui" line="1941"/>
        <source>Dark Colors</source>
        <translation>Mørke farger</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1950"/>
        <source>Donation</source>
        <translation>Donasjon</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1965"/>
        <location filename="../MainWindow.cpp" line="3840"/>
        <source>Distribution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1968"/>
        <location filename="../MainWindow.ui" line="1971"/>
        <source>Show Distribution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1983"/>
        <location filename="../MainWindow.ui" line="1986"/>
        <source>Save</source>
        <translation type="unfinished">Lagre</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1995"/>
        <source>Wiki</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1998"/>
        <location filename="../MainWindow.ui" line="2001"/>
        <source>Read Wiki</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="256"/>
        <location filename="../MainWindow.cpp" line="161"/>
        <location filename="../MainWindow.cpp" line="5051"/>
        <source>Heartrate</source>
        <translation>Puls</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1953"/>
        <location filename="../MainWindow.ui" line="1956"/>
        <source>Make Donation</source>
        <translation>Send donasjon</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1445"/>
        <source>Bugreport</source>
        <translation>Feilrapport</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1448"/>
        <location filename="../MainWindow.ui" line="1451"/>
        <source>Send Bugreport</source>
        <translation>Send feilrapport</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1460"/>
        <source>Settings</source>
        <translation>Innstillinger</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1463"/>
        <location filename="../MainWindow.ui" line="1466"/>
        <source>Change Settings</source>
        <translation>Endre innstillinger</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1481"/>
        <source>From Device</source>
        <translation>Fra enhet</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1484"/>
        <location filename="../MainWindow.ui" line="1487"/>
        <source>Import From Device</source>
        <translation>Importer fra enhet</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1499"/>
        <source>From File</source>
        <translation>Fra fil</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1502"/>
        <location filename="../MainWindow.ui" line="1505"/>
        <source>Import From File</source>
        <translation>Importer fra fil</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1514"/>
        <source>From Input</source>
        <translation>Fra inndata</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1517"/>
        <location filename="../MainWindow.ui" line="1520"/>
        <source>Import From Input</source>
        <translation>Importer fra inndata</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1529"/>
        <source>To CSV</source>
        <translation>Til CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1532"/>
        <location filename="../MainWindow.ui" line="1535"/>
        <source>Export To CSV</source>
        <translation>Eksporter til CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1547"/>
        <source>To XML</source>
        <translation>Til XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1550"/>
        <location filename="../MainWindow.ui" line="1553"/>
        <source>Export To XML</source>
        <translation>Eksporter til XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1562"/>
        <source>To JSON</source>
        <translation>Til JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1565"/>
        <location filename="../MainWindow.ui" line="1568"/>
        <source>Export To JSON</source>
        <translation>Eksporter til JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1577"/>
        <source>To SQL</source>
        <translation>Til SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1580"/>
        <location filename="../MainWindow.ui" line="1583"/>
        <source>Export To SQL</source>
        <translation>Eksporter til SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1625"/>
        <source>Print Chart</source>
        <translation>Skriv ut diagram</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1628"/>
        <location filename="../MainWindow.ui" line="1631"/>
        <source>Print Chart View</source>
        <translation>Skriv ut diagramvisning</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1640"/>
        <source>Print Table</source>
        <translation>Skriv ut tabell</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1643"/>
        <location filename="../MainWindow.ui" line="1646"/>
        <source>Print Table View</source>
        <translation>Skriv ut tabellvisning</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1655"/>
        <source>Print Statistic</source>
        <translation>Skriv ut statistikk</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1658"/>
        <location filename="../MainWindow.ui" line="1661"/>
        <source>Print Statistic View</source>
        <translation>Skriv ut statistikkvisning</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1670"/>
        <source>Preview Chart</source>
        <translation>Forhåndsvis diagram</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1673"/>
        <location filename="../MainWindow.ui" line="1676"/>
        <source>Preview Chart View</source>
        <translation type="unfinished">Forhåndsvis diagramvisning</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1685"/>
        <source>Preview Table</source>
        <translation>Forhåndsvis tabell</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1688"/>
        <location filename="../MainWindow.ui" line="1691"/>
        <source>Preview Table View</source>
        <translation>Forhåndsvis tabellvisning</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1700"/>
        <source>Preview Statistic</source>
        <translation>Forhåndsvis statistikk</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1703"/>
        <location filename="../MainWindow.ui" line="1706"/>
        <source>Preview Statistic View</source>
        <translation>Forhåndsvis statistikkvisning</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1715"/>
        <location filename="../MainWindow.ui" line="1718"/>
        <location filename="../MainWindow.ui" line="1721"/>
        <source>Clear All</source>
        <translation>Tøm alt</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1733"/>
        <location filename="../MainWindow.ui" line="1736"/>
        <location filename="../MainWindow.ui" line="1739"/>
        <source>Clear User 1</source>
        <translation>Tøm bruker 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1748"/>
        <location filename="../MainWindow.ui" line="1751"/>
        <location filename="../MainWindow.ui" line="1754"/>
        <source>Clear User 2</source>
        <translation>Tøm bruker 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1814"/>
        <source>Analysis</source>
        <translation>Analyse</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1817"/>
        <location filename="../MainWindow.ui" line="1820"/>
        <source>Analyze Records</source>
        <translation>Analyser oppføringer</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1839"/>
        <location filename="../MainWindow.ui" line="1842"/>
        <location filename="../MainWindow.ui" line="1845"/>
        <source>Time Mode</source>
        <translation>Tidsmodus</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="108"/>
        <location filename="../MainWindow.cpp" line="109"/>
        <location filename="../MainWindow.cpp" line="5019"/>
        <location filename="../MainWindow.cpp" line="5020"/>
        <source>Records For Selected User</source>
        <translation>Oppføringer for valgt bruker</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="116"/>
        <location filename="../MainWindow.cpp" line="117"/>
        <location filename="../MainWindow.cpp" line="5022"/>
        <location filename="../MainWindow.cpp" line="5023"/>
        <source>Select Date &amp; Time</source>
        <translation>Velg dato og tid</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="169"/>
        <location filename="../MainWindow.cpp" line="5053"/>
        <source>Systolic - Value Range</source>
        <translation>Systolisk – verdispenn</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="170"/>
        <location filename="../MainWindow.cpp" line="5054"/>
        <source>Diastolic - Value Range</source>
        <translation>Diastolisk – verdispenn</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="173"/>
        <location filename="../MainWindow.cpp" line="5056"/>
        <source>Systolic - Target Area</source>
        <translation>Systolisk – målområde</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="174"/>
        <location filename="../MainWindow.cpp" line="5057"/>
        <source>Diastolic - Target Area</source>
        <translation>Diastolisk – målområd</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1711"/>
        <source>Athlete</source>
        <translation>Atlet</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1712"/>
        <source>Excellent</source>
        <translation>Utmerket</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1712"/>
        <source>Optimal</source>
        <translation>Optimalt</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1713"/>
        <source>Great</source>
        <translation>Veldig bra</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1714"/>
        <source>Good</source>
        <translation>Bra</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1713"/>
        <source>Normal</source>
        <translation>Normalt</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1775"/>
        <location filename="../MainWindow.ui" line="1778"/>
        <location filename="../MainWindow.ui" line="1799"/>
        <location filename="../MainWindow.ui" line="1802"/>
        <location filename="../MainWindow.cpp" line="102"/>
        <location filename="../MainWindow.cpp" line="103"/>
        <location filename="../MainWindow.cpp" line="104"/>
        <location filename="../MainWindow.cpp" line="105"/>
        <location filename="../MainWindow.cpp" line="4737"/>
        <location filename="../MainWindow.cpp" line="4738"/>
        <location filename="../MainWindow.cpp" line="4739"/>
        <location filename="../MainWindow.cpp" line="4740"/>
        <location filename="../MainWindow.cpp" line="5083"/>
        <location filename="../MainWindow.cpp" line="5084"/>
        <location filename="../MainWindow.cpp" line="5085"/>
        <location filename="../MainWindow.cpp" line="5086"/>
        <source>Switch To %1</source>
        <translation>Bytt til %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="102"/>
        <location filename="../MainWindow.cpp" line="103"/>
        <location filename="../MainWindow.cpp" line="610"/>
        <location filename="../MainWindow.cpp" line="4737"/>
        <location filename="../MainWindow.cpp" line="4738"/>
        <location filename="../MainWindow.cpp" line="5083"/>
        <location filename="../MainWindow.cpp" line="5084"/>
        <source>User 1</source>
        <translation>Bruker 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="104"/>
        <location filename="../MainWindow.cpp" line="105"/>
        <location filename="../MainWindow.cpp" line="620"/>
        <location filename="../MainWindow.cpp" line="4739"/>
        <location filename="../MainWindow.cpp" line="4740"/>
        <location filename="../MainWindow.cpp" line="5085"/>
        <location filename="../MainWindow.cpp" line="5086"/>
        <source>User 2</source>
        <translation>Bruker 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="171"/>
        <location filename="../MainWindow.cpp" line="5055"/>
        <source>Heartrate - Value Range</source>
        <translation>Puls – verdispenn</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="175"/>
        <location filename="../MainWindow.cpp" line="5058"/>
        <source>Heartrate - Target Area</source>
        <translation>Puls – målområde</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="341"/>
        <source>No active device plugin, auto import aborted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="358"/>
        <source>Some plugins will not work without Bluetooth permission.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="461"/>
        <source>Could not create backup directory.

Please check backup location setting.</source>
        <translation type="unfinished">Kunne ikke opprette mappe for sikkerhetskopiering.

Sjekk innstillingen for hvor sikkerhetskopier lagres.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="490"/>
        <location filename="../MainWindow.cpp" line="513"/>
        <source>Could not backup database:

%1</source>
        <translation>Kunne ikke sikkerhetskopiere database:

%1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="503"/>
        <source>Could not delete outdated backup:

%1</source>
        <translation>Kunne ikke slette utdatert sikkerhetskopi:

%1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="530"/>
        <source>Could not register icons.</source>
        <translation>Kunne ikke registrere ikoner.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="681"/>
        <source>Blood Pressure Report</source>
        <translation>Blodtrykksrapport</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="682"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation type="unfinished">Kjære lege,

Vedlagt finner du blodtrykksdata fra meg for denne måneden.

Hilsen,
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1223"/>
        <source>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</source>
        <translation>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  SPM : Ø %3 / x̃ %6</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1228"/>
        <source>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</source>
        <translation>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  SPM : Ø 0 / x̃ 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1711"/>
        <source>Low</source>
        <translation>Lav</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1714"/>
        <source>High Normal</source>
        <translation>Høy normal</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1715"/>
        <location filename="../MainWindow.cpp" line="2192"/>
        <location filename="../MainWindow.cpp" line="5062"/>
        <location filename="../MainWindow.cpp" line="5066"/>
        <location filename="../MainWindow.cpp" line="5070"/>
        <location filename="../MainWindow.h" line="195"/>
        <location filename="../MainWindow.h" line="199"/>
        <location filename="../MainWindow.h" line="203"/>
        <source>Average</source>
        <translation>Gjennomsnitt</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1715"/>
        <source>Hyper 1</source>
        <translation>Hyper 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1716"/>
        <source>Below Average</source>
        <translation>Under gjennomsnitt</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1716"/>
        <source>Hyper 2</source>
        <translation>Hyper 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1717"/>
        <source>Poor</source>
        <translation>Dårlig</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1717"/>
        <source>Hyper 3</source>
        <translation>Hyper 3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1862"/>
        <source>Scanning import plugin &quot;%1&quot; failed!

%2</source>
        <translation>Kunne ikke skanne importerings-programtillegg «%1».

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1879"/>
        <location filename="../MainWindow.cpp" line="1906"/>
        <location filename="../MainWindow.cpp" line="5029"/>
        <source>Switch Language to %1</source>
        <translation>Bytt språk til %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1932"/>
        <location filename="../MainWindow.cpp" line="1947"/>
        <location filename="../MainWindow.cpp" line="5037"/>
        <source>Switch Theme to %1</source>
        <translation>Bytt drakt til %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1972"/>
        <location filename="../MainWindow.cpp" line="1992"/>
        <location filename="../MainWindow.cpp" line="5045"/>
        <source>Switch Style to %1</source>
        <translation>Bytt stil til %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2063"/>
        <location filename="../MainWindow.cpp" line="2088"/>
        <source>Edit record</source>
        <translation>Rediger oppføring</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2192"/>
        <location filename="../MainWindow.cpp" line="5063"/>
        <location filename="../MainWindow.cpp" line="5067"/>
        <location filename="../MainWindow.cpp" line="5071"/>
        <location filename="../MainWindow.h" line="196"/>
        <location filename="../MainWindow.h" line="200"/>
        <location filename="../MainWindow.h" line="204"/>
        <source>Median</source>
        <translation>Median</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2194"/>
        <source>Click to swap Average and Median</source>
        <translation>Klikk for å bytte gjennomsnitt og median</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2289"/>
        <source>Click to swap Legend and Label</source>
        <translation>Klikk for å bytte verditype og etikett</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2328"/>
        <source>No records to preview for selected time range!</source>
        <translation>Ingen oppføringer å vise for valgt tidsområde.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2360"/>
        <source>No records to print for selected time range!</source>
        <translation>Ingen oppføringer å skrive ut for valgt tidsområde.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2417"/>
        <source>User %1</source>
        <translation>Bruker %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3001"/>
        <source>Could not create e-mail because generating base64 for attachment &quot;%1&quot; failed!

%2</source>
        <translation type="unfinished">Kunne ikke opprette e-post fordi generering av Base64 for vedlegget «%1» mislyktes.

%2</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="3205"/>
        <location filename="../MainWindow.cpp" line="4491"/>
        <source>Skipped %n hidden record(s)!</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3844"/>
        <location filename="../MainWindow.cpp" line="4645"/>
        <source>Chart</source>
        <translation>Diagram</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3844"/>
        <location filename="../MainWindow.cpp" line="4662"/>
        <source>Table</source>
        <translation>Tabell</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3844"/>
        <location filename="../MainWindow.cpp" line="4679"/>
        <source>Statistic</source>
        <translation>Statistikk</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4603"/>
        <source>No records to analyse!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4615"/>
        <source>No records to display for selected time range!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4697"/>
        <source>Could not open e-mail &quot;%1&quot;!

%2</source>
        <translation type="unfinished">Kunne ikke åpne e-posten «%1».

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4707"/>
        <source>Could not start e-mail client!</source>
        <translation type="unfinished">Kunne ikke starte e-postklient.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4712"/>
        <source>No records to e-mail for selected time range!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4718"/>
        <source>Select Icon Color</source>
        <translation>Velg ikonfarge</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4724"/>
        <source>Restart application to apply new icon color.</source>
        <translation>Start programmet på ny for å bruke ny ikonfarge.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="902"/>
        <source>The database can not be saved.

Please choose another location in the settings or all data will be lost.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2412"/>
        <source>Created with UBPM for
Windows / Linux / macOS</source>
        <translation>Opprettet med UBPM for
Linux/Windows/macOS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2413"/>
        <source>Free and OpenSource
https://codeberg.org/lazyt/ubpm</source>
        <translation>Fri programvare
https://codeberg.org/lazyt/ubpm</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3153"/>
        <source>Import from CSV/XML/JSON/SQL</source>
        <translation>Importer fra CSV/XML/JSON/SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3153"/>
        <source>CSV File (*.csv);;XML File (*.xml);;JSON File (*.json);;SQL File (*.sql)</source>
        <translation>CSV-fil (*.csv);;XML File (*.xml);;JSON-fil (*.json);;SQL-fil (*.sql)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3216"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>Kunne ikke åpne «%1».

Grunn: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1222"/>
        <source>Measurements : %1  |  Irregular : %2  |  Movement : %3</source>
        <translation>Målinger: %1  |  Uregelmessig: %2  |  Bevegelse: %3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1227"/>
        <source>Measurements : 0  |  Irregular : 0  |  Movement : 0</source>
        <translation>Målinger: 0  |  Uregelmessig: 0  |  Bevegelse: 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2417"/>
        <source>%1
Age: %2, Height: %3 cm, Weight: %4 kg, BMI: %5</source>
        <translation>%1
Alder: %2, Høyde: %3 cm, Vekt: %4 kg, BMI: %5</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="3191"/>
        <location filename="../MainWindow.cpp" line="4482"/>
        <source>Successfully imported %n record(s) from %1.

     User 1 : %2
     User 2 : %3</source>
        <translation>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="3195"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="3200"/>
        <location filename="../MainWindow.cpp" line="4486"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3586"/>
        <source>Doesn&apos;t look like a UBPM database!

Maybe wrong encryption settings/password?</source>
        <translation>Ser ikke ut til å være en UBPM-database.

Kanskje du krypteringsinnstillingene eller passordet er galt?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3639"/>
        <location filename="../MainWindow.cpp" line="3643"/>
        <source>Export to %1</source>
        <translation>Eksporter til %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3639"/>
        <location filename="../MainWindow.cpp" line="3643"/>
        <source>%1 File (*.%2)</source>
        <translation>%1 fil (*.%2)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3679"/>
        <source>Could not create &quot;%1&quot;!

Reason: %2</source>
        <translation>Kunne ikke opprette «%1».

Grunn: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3685"/>
        <source>The database is empty, no records to export!</source>
        <translation>Databasen er tom. Ingen oppføringer å eksportere.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4052"/>
        <source>Morning</source>
        <translation>Morgen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4052"/>
        <source>Afternoon</source>
        <translation>Ettermiddag</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4108"/>
        <source>Week</source>
        <translation>Uke</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4163"/>
        <source>Quarter</source>
        <translation>Kvartal</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4192"/>
        <source>Half Year</source>
        <translation>Halvår</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4210"/>
        <source>Year</source>
        <translation>År</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4416"/>
        <source>Really delete all records for user %1?</source>
        <translation>Slett alle oppføringer for bruker %1?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4431"/>
        <source>All records for user %1 deleted and existing database saved to &quot;ubpm.sql.bak&quot;.</source>
        <translation>Alle oppføringer for bruker %1 er slettet, og eksisterende database lagret som «ubpm.sql.bak».</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4570"/>
        <source>Really delete all records?</source>
        <translation>Slett alle oppføringer?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4581"/>
        <source>All records deleted and existing database &quot;ubpm.sql&quot; moved to trash.</source>
        <translation>Alle oppføringer slettet og eksisterende database «ubpm.sql» flyttet til papirkurv.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4901"/>
        <source>- UBPM Application
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4916"/>
        <source>- UBPM Plugins
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5060"/>
        <location filename="../MainWindow.cpp" line="5064"/>
        <location filename="../MainWindow.cpp" line="5068"/>
        <location filename="../MainWindow.h" line="193"/>
        <location filename="../MainWindow.h" line="197"/>
        <location filename="../MainWindow.h" line="201"/>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5061"/>
        <location filename="../MainWindow.cpp" line="5065"/>
        <location filename="../MainWindow.cpp" line="5069"/>
        <location filename="../MainWindow.h" line="194"/>
        <location filename="../MainWindow.h" line="198"/>
        <location filename="../MainWindow.h" line="202"/>
        <source>Maximum</source>
        <translation>Maksimum</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5134"/>
        <source>Could not open theme &quot;%1&quot; file!

Reason: %2</source>
        <translation>Kunne ikke åpne draktfilen «%1».

Grunn: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2060"/>
        <location filename="../MainWindow.cpp" line="2077"/>
        <location filename="../MainWindow.cpp" line="6020"/>
        <source>Delete record</source>
        <translation>Slett oppføring</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5943"/>
        <source>Show Lines</source>
        <translation>Vis linjer</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5947"/>
        <source>Show Heartrate</source>
        <translation>Vis puls</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5983"/>
        <location filename="../MainWindow.cpp" line="5994"/>
        <source>Symbols and lines can&apos;t be disabled both!</source>
        <translation type="unfinished">Du kan ikke skru av både linjer og symboler.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="6022"/>
        <source>Show record</source>
        <translation>Vis oppføring</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2061"/>
        <location filename="../MainWindow.cpp" line="2084"/>
        <location filename="../MainWindow.cpp" line="6023"/>
        <source>Hide record</source>
        <translation>Skjul oppføring</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2079"/>
        <location filename="../MainWindow.cpp" line="6034"/>
        <source>Really delete selected record?</source>
        <translation>Slett valgt oppføring?</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="5013"/>
        <source>The following %n translation(s) for &quot;%1&quot; could not be loaded:

%2
Hide this warning on program startup?</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5916"/>
        <source>Scrolling has reached top/bottom, show next/previous period?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5939"/>
        <source>Dynamic Scaling</source>
        <translation>Dynamisk skalering</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5940"/>
        <source>Colored Stripes</source>
        <translation>Fargede striper</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5942"/>
        <source>Show Symbols</source>
        <translation>Vis symboler</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5945"/>
        <source>Colored Symbols</source>
        <translation>Fargede symboler</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="6055"/>
        <source>Show Median</source>
        <translation>Vis median</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="6056"/>
        <source>Show Values</source>
        <translation>Vis verdier</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="6111"/>
        <source>Really quit program?</source>
        <translation>Avslutt programmet?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../MainWindow.h" line="4"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Universell blodtrykkshåndterer</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="13"/>
        <source>The application is already running and multiple instances are not allowed.

Switch to the running instance or close it and try again.</source>
        <translation type="unfinished">Programmet kjører allerede, og kan kun eksistere i én versjon.

Bytt til programmet som kjører allerede, eller lukk det og prøv igjen.</translation>
    </message>
</context>
</TS>
