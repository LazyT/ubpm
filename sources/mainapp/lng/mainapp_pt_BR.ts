<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>DialogAbout</name>
    <message>
        <location filename="../DialogAbout.ui" line="14"/>
        <source>About UBPM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="41"/>
        <source>Version</source>
        <translation type="unfinished">Versão</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="53"/>
        <source>Universal Blood Pressure Manager</source>
        <translation type="unfinished">Gerenciador Universal de Pressão Arterial</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="91"/>
        <source>Qt Framework</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="129"/>
        <source>Operating System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="220"/>
        <source>Environment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="231"/>
        <source>Settings</source>
        <translation type="unfinished">Configurações</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="266"/>
        <source>Database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="301"/>
        <source>Cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="336"/>
        <source>Guides</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="371"/>
        <source>Languages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="406"/>
        <source>Plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="441"/>
        <source>Themes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="469"/>
        <source>Translators</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.cpp" line="14"/>
        <source>The program is provided as is with no warranty of any kind, including the warranty of design, merchantability and fitness for a particular purpose.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.cpp" line="37"/>
        <source>Could not open &quot;%1&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DialogAnalysis</name>
    <message>
        <location filename="../DialogAnalysis.ui" line="14"/>
        <source>Data Analysis</source>
        <translation>Análise de Dados</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="24"/>
        <source>Query</source>
        <translation>Consulta</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="43"/>
        <location filename="../DialogAnalysis.cpp" line="209"/>
        <source>Results</source>
        <translation>Resultados</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="82"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="91"/>
        <source>Time</source>
        <translation>Hora</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="100"/>
        <source>Systolic</source>
        <translation>Sistólica</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="109"/>
        <source>Diastolic</source>
        <translation>Diastólica</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="118"/>
        <source>P.Pressure</source>
        <translation>P.Pulso</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="127"/>
        <source>Heartrate</source>
        <translation>Pulso</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="136"/>
        <source>Irregular</source>
        <translation>Irregular</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="145"/>
        <source>Movement</source>
        <translation>Movimento</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="154"/>
        <source>Invisible</source>
        <translation>Invisível</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="163"/>
        <source>Comment</source>
        <translation>Comentário</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="193"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="51"/>
        <source>Could not create memory database!

%1</source>
        <translation>Não foi possível criar banco de dados de memória!

%1</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>No results for this query found!</source>
        <translation>Nenhum resultado encontrado para esta consulta!</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>%1 for %2 [ %3 %4 | %5 %6 | %7% ]</source>
        <translation>%1 para %2 [ %3 %4 | %5 %6 | %7%]</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Result(s)</source>
        <translation>
            <numerusform>Resultado</numerusform>
            <numerusform>Resultados</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Match(es)</source>
        <translation>
            <numerusform>Correspondência</numerusform>
            <numerusform>Correspondências</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Record(s)</source>
        <translation>
            <numerusform>Registro</numerusform>
            <numerusform>Registros</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>User %1</source>
        <translation>Usuário %1</translation>
    </message>
</context>
<context>
    <name>DialogDistribution</name>
    <message>
        <location filename="../DialogDistribution.ui" line="20"/>
        <source>Data Distribution</source>
        <translation>Distribuição de Dados</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="77"/>
        <source>Preview</source>
        <translation>Pré-visualização</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="94"/>
        <source>Print</source>
        <translation>Imprimir</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="128"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogDistribution.cpp" line="18"/>
        <source>Record(s)</source>
        <translation>
            <numerusform>Registro</numerusform>
            <numerusform>Registros</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="113"/>
        <source>Low</source>
        <translation>Baixo</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="115"/>
        <source>Optimal</source>
        <translation>Ideal</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="117"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="119"/>
        <source>High Normal</source>
        <translation>Normal Alto</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="121"/>
        <source>Hyper 1</source>
        <translation>Hiper 1</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="123"/>
        <source>Hyper 2</source>
        <translation>Hipertensão 2</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="125"/>
        <source>Hyper 3</source>
        <translation>Hiper 3</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="142"/>
        <source>SYS</source>
        <translation>SIS</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="154"/>
        <source>DIA</source>
        <translation>DIA</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="165"/>
        <source>Blood Pressure</source>
        <translation>Pressão Arterial</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="203"/>
        <source>Athlete</source>
        <translation>Atleta</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="205"/>
        <source>Excellent</source>
        <translation>Excelente</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="207"/>
        <source>Great</source>
        <translation>Ótimo</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="209"/>
        <source>Good</source>
        <translation>Bom</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="211"/>
        <source>Average</source>
        <translation>Regular</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="213"/>
        <source>Below Average</source>
        <translation>Abaixo da média</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="215"/>
        <source>Poor</source>
        <translation>Ruim</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="232"/>
        <source>BPM</source>
        <translation>BPM</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="258"/>
        <source>Heart Rate</source>
        <translation>Pulso</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="111"/>
        <location filename="../DialogDistribution.cpp" line="419"/>
        <source>Export To PDF</source>
        <translation type="unfinished">Exportar para PDF</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="419"/>
        <source>%1 File (*.%2)</source>
        <translation type="unfinished">%1 Arquivo (*.%2)</translation>
    </message>
</context>
<context>
    <name>DialogDonation</name>
    <message>
        <location filename="../DialogDonation.ui" line="14"/>
        <source>Donation</source>
        <translation>Doação</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="67"/>
        <source>Copy E-Mail and open Amazon</source>
        <translation>Copie o e-mail e abra a Amazon</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="154"/>
        <source>Open PayPal (+)</source>
        <translation>Abra o PayPal (+)</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="171"/>
        <source>Open PayPal (-)</source>
        <translation>Abra o PayPal (-)</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="46"/>
        <source>If you have an Amazon account, you can send money by gift card.

Click the button below to open the Amazon website in your browser and

- sign in to your account
- select or enter the amount you would like to send
- select e-mail delivery
- paste the e-mail address as the recipient
- add a message
- complete the donation

You can also scan the QR code with your mobile device.</source>
        <translation>Se você possui uma conta na Amazon, pode enviar dinheiro por cartão-presente.

Clique no botão abaixo para abrir o site da Amazon em seu navegador e

- Faça login em sua conta
- selecione ou insira o valor que deseja enviar
- selecione a entrega por e-mail
- cole o endereço de e-mail como destinatário
- Adiciona uma mensagem
- concluir a doação

Você também pode digitalizar o código QR com seu dispositivo móvel.</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="124"/>
        <source>If you have a PayPal account, you can send money using the &quot;Friends and Family&quot; feature.

Open your PayPal app and

- search for @LazyT (Thomas Löwe)
- enter the amount you would like to send
- add a message
- complete the donation

You can also click the first button below to do this in your browser without using the app.

If you do not have a PayPal account but do have a credit/debit card click the second button below to go to the PayPal donation page and

- select onetime or repeating donation
- select or enter the amount you would like to send
- optionally check the box to cover the fees
- complete the donation

You can also scan the QR code with your mobile device.</source>
        <translation>Se você possui uma conta do PayPal, pode enviar dinheiro usando o recurso &quot;Amigos e família&quot;.

Abra seu aplicativo do PayPal e

- procure por @LazyT (Thomas Löwe)
- insira o valor que deseja enviar
- Adiciona uma mensagem
- concluir a doação

Você também pode clicar no primeiro botão abaixo para fazer isso em seu navegador sem usar o aplicativo.

Se você não possui uma conta do PayPal, mas possui um cartão de crédito/débito, clique no segundo botão abaixo para acessar a página de doações do PayPal e

- selecione doação única ou repetida
- selecione ou insira o valor que deseja enviar
- opcionalmente marque a caixa para cobrir as taxas
- concluir a doação

Você também pode digitalizar o código QR com seu dispositivo móvel.</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="230"/>
        <source>If you are in the Euro Payments Area, you can transfer money from your bank account.

Click the buttons below to copy the name/IBAN/BIC to the clipboard and

- sign in to your bank account
- paste the name/IBAN/BIC into the transfer form
- enter the amount you would like to send
- complete the donation

You can also scan the QR code with your mobile device.</source>
        <translation>Se você estiver na área de pagamentos em euros, poderá transferir dinheiro de sua conta bancária.

Clique nos botões abaixo para copiar o nome/IBAN/BIC para a área de transferência e

- entre na sua conta bancária
- cole o nome/IBAN/BIC no formulário de transferência
- insira o valor que deseja enviar
- concluir a doação

Você também pode digitalizar o código QR com seu dispositivo móvel.</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="251"/>
        <source>Copy Name</source>
        <translation>Copiar Nome</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="268"/>
        <source>Copy IBAN</source>
        <translation>Copiar IBAN</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="285"/>
        <source>Copy BIC</source>
        <translation>Copiar BIC</translation>
    </message>
</context>
<context>
    <name>DialogHelp</name>
    <message>
        <location filename="../DialogHelp.ui" line="20"/>
        <source>Guide</source>
        <translation>Guia</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="61"/>
        <source>%1 guide not found, showing EN guide instead.</source>
        <translation>%1 guia não encontrado, mostrando o guia EN em seu lugar.</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="53"/>
        <source>%1 guide not found!</source>
        <translation>%1 guia não encontrado!</translation>
    </message>
</context>
<context>
    <name>DialogMigration</name>
    <message>
        <location filename="../DialogMigration.ui" line="14"/>
        <source>Data Migration</source>
        <translation>Migração de dados</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="24"/>
        <source>Source</source>
        <translation>Fonte</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="72"/>
        <source>Open Data Source</source>
        <translation>Abrir fonte de dados</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="97"/>
        <source>Parameters</source>
        <translation>Parâmetros</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="217"/>
        <source>Date Format</source>
        <translation>Formato de data</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="267"/>
        <source>Irregular Detection</source>
        <translation>Detecção Irregular</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="167"/>
        <source>Startline</source>
        <translation>Linha de largada</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="183"/>
        <source>Elements per Line</source>
        <translation>Elementos por Linha</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="111"/>
        <source>Delimiter</source>
        <translation>Delimitador</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="303"/>
        <source>Time Format</source>
        <translation>Formato de hora</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="199"/>
        <source>Movement Detection</source>
        <translation>Detecção de Movimento</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="239"/>
        <source>Date Language</source>
        <translation>Idioma da data</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="344"/>
        <source>Positions</source>
        <translation>Posições</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="488"/>
        <source>Irregular</source>
        <translation>Irregular</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="504"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="452"/>
        <source>Systolic</source>
        <translation>Sistólica</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="410"/>
        <source>Heartrate</source>
        <translation>Pulso</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="394"/>
        <source>Movement</source>
        <translation>Movimento</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="329"/>
        <source>Ignore Comment</source>
        <translation>Ignorar Comentário</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="368"/>
        <source>Time</source>
        <translation>Hora</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="436"/>
        <source>Diastolic</source>
        <translation>Diastólica</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="550"/>
        <source>Comment</source>
        <translation>Comentário</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="567"/>
        <source>Result</source>
        <translation>Resultado</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="786"/>
        <source>Migrate User 1</source>
        <translation>Migrar Usuário 1</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="815"/>
        <source>Migrate User 2</source>
        <translation>Migrar Usuário 2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="841"/>
        <source>Copy to Custom</source>
        <translation>Copiar para Personalizado</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="861"/>
        <source>Select predefined settings</source>
        <translation>Selecione as configurações predefinidas</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="895"/>
        <source>Custom</source>
        <translation>Personalizado</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="906"/>
        <source>Test Data Migration</source>
        <translation>Testar Migração de Dados</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="920"/>
        <source>Start Data Migration</source>
        <translation>Iniciar Migração de Dados</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="251"/>
        <source>Choose Data Source for Migration</source>
        <translation>Escolha a Fonte de Dados para Migração</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="251"/>
        <source>CSV File (*.csv)</source>
        <translation>Arquivo CSV (*.csv)</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="304"/>
        <source>%L1 Bytes</source>
        <translation>%L1 Bytes</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="305"/>
        <source>%1 Lines</source>
        <translation>%1 Linhas</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="319"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>Não foi possível abrir &quot;%1&quot;!

Motivo: %2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="341"/>
        <source>Startline can&apos;t be empty.</source>
        <translation>A primeira linha não pode estar vazia.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="347"/>
        <source>Startline must be smaller than the number of lines.</source>
        <translation>A primeira linha deve ser menor que o número de linhas.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="355"/>
        <source>Line %1 = %2</source>
        <translation>Linha %1 = %2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="360"/>
        <source>Delimiter can&apos;t be empty.</source>
        <translation>O delimitador não pode estar vazio.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="367"/>
        <source>Startline doesn&apos;t contain &quot;%1&quot; delimiter.</source>
        <translation>A linha de início não contém o delimitador &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="378"/>
        <source>Elements can&apos;t be empty.</source>
        <translation>Os elementos não podem estar vazios.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="384"/>
        <source>Elements can&apos;t be smaller than 4.</source>
        <translation>Os elementos não podem ser menores que 4.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="390"/>
        <source>Elements doesn&apos;t match (found %1).</source>
        <translation>Os elementos não correspondem (encontrado %1).</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="397"/>
        <source>Date format can&apos;t be empty.</source>
        <translation>O formato de data não pode estar vazio.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="403"/>
        <source>Single date format must also contain a time format.</source>
        <translation>O formato de data única deve também conter um formato de hora.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="410"/>
        <source>Date position can&apos;t be empty.</source>
        <translation>A posição da data não pode estar vazia.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="416"/>
        <source>Date position must be smaller than elements.</source>
        <translation>A posição da data deve ser menor que os elementos.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="423"/>
        <source>Time position requires also a time parameter.</source>
        <translation>A posição de tempo requer também um parâmetro de tempo.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="429"/>
        <source>Time format requires also a time position.</source>
        <translation>O formato de hora também requer uma posição de tempo.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="435"/>
        <source>Time position must be smaller than elements.</source>
        <translation>A posição no tempo deve ser menor que os elementos.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="442"/>
        <source>Systolic position can&apos;t be empty.</source>
        <translation>A posição sistólica não pode estar vazia.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="448"/>
        <source>Systolic position must be smaller than elements.</source>
        <translation>A posição sistólica deve ser menor que os elementos.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="455"/>
        <source>Diastolic position can&apos;t be empty.</source>
        <translation>A posição diastólica não pode estar vazia.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="461"/>
        <source>Diastolic position must be smaller than elements.</source>
        <translation>A posição diastólica deve ser menor que os elementos.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="468"/>
        <source>Heartrate position can&apos;t be empty.</source>
        <translation>A posição de frequência cardíaca não pode ficar vazia.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="474"/>
        <source>Heartrate position must be smaller than elements.</source>
        <translation>A posição da frequência cardíaca deve ser menor que os elementos.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="481"/>
        <source>Irregular position must be smaller than elements.</source>
        <translation>A posição irregular deve ser menor que os elementos.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="487"/>
        <source>Irregular position requires also an irregular parameter.</source>
        <translation>A posição irregular requer também um parâmetro irregular.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="494"/>
        <source>Movement position must be smaller than elements.</source>
        <translation>A posição do movimento deve ser menor que os elementos.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="500"/>
        <source>Movement position requires also a movement parameter.</source>
        <translation>A posição do movimento requer também um parâmetro de movimento.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="507"/>
        <source>Comment position must be smaller than elements.</source>
        <translation>A posição do comentário deve ser menor que os elementos.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="523"/>
        <source>Positions can&apos;t exist twice.</source>
        <translation>As posições não podem existir duas vezes.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="539"/>
        <source>Date format is invalid.</source>
        <translation>O formato da data é inválido.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="546"/>
        <source>Time format is invalid.</source>
        <translation>O formato da hora é inválido.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="556"/>
        <source>Date/Time format is invalid.</source>
        <translation>O formato de data/hora é inválido.</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="747"/>
        <source>Successfully migrated %n record(s) for user %1.</source>
        <translation>
            <numerusform>Migrou com sucesso %n registro para o usuário %1.</numerusform>
            <numerusform>Migrou com sucesso %n registros para o usuário %1.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="751"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>%n registro inválido ignorado!</numerusform>
            <numerusform>%n registros inválidos ignorados!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="756"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>%n registro duplicado ignorado!</numerusform>
            <numerusform>%n registros duplicados ignorados!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="761"/>
        <source>Skipped %n hidden record(s)!</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogRecord</name>
    <message>
        <location filename="../DialogRecord.ui" line="14"/>
        <source>Manual Record</source>
        <translation>Registro Manual</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="27"/>
        <source>Data Record</source>
        <translation>Registro de Dados</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="35"/>
        <location filename="../DialogRecord.ui" line="211"/>
        <location filename="../DialogRecord.cpp" line="9"/>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>Add Record For %1</source>
        <translation>Adicionar Registro para %1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="78"/>
        <source>Select Date &amp; Time</source>
        <translation>Selecione Data &amp; Hora</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="129"/>
        <source>Enter SYS</source>
        <translation>Digite SIS</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="148"/>
        <source>Enter DIA</source>
        <translation>Digite DIA</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="167"/>
        <source>Enter BPM</source>
        <translation>Digite BPM</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="102"/>
        <source>Irregular Heartbeat</source>
        <translation>Arritmia Cardíaca</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="186"/>
        <source>Movement</source>
        <translation>Movimento</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="239"/>
        <source>Enter Optional Comment</source>
        <translation>Insira um Comentário Opcional</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="269"/>
        <source>Show message on successful create / delete / modify record</source>
        <translation>Mostrar mensagem de sucesso ao criar / excluir / modificar o registro</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="287"/>
        <source>Delete</source>
        <translation>Deletar</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="304"/>
        <source>Create</source>
        <translation>Criar</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="324"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="9"/>
        <source>User 1</source>
        <translation>Usuário 1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>User 2</source>
        <translation>Usuário 2</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="34"/>
        <location filename="../DialogRecord.cpp" line="187"/>
        <source>Modify</source>
        <translation>Modificar</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="132"/>
        <source>The data record could not be deleted!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>O registro de dados não pôde ser excluído!

Não existe uma entrada para esta data e hora.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="138"/>
        <source>Data record successfully deleted.</source>
        <translation>Registro de dados excluído com sucesso.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="150"/>
        <source>Please enter a valid value for &quot;SYS&quot; first!</source>
        <translation>Por favor, insira um valor válido para &quot;SÍS&quot; primeiro!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="160"/>
        <source>Please enter a valid value for &quot;DIA&quot; first!</source>
        <translation>Por favor, insira um valor válido para &quot;DIÁ&quot; primeiro!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="169"/>
        <source>Please enter a valid value for &quot;BPM&quot; first!</source>
        <translation>Insira um valor válido para &quot;BPM&quot; primeiro!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="191"/>
        <source>The data record could not be modified!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>O registro de dados não pôde ser modificado!

Não existe uma entrada para esta data e hora.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="197"/>
        <source>Data Record successfully modified.</source>
        <translation>Registro de dados modificado com sucesso.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="206"/>
        <source>The data record could not be created!

An entry for this date &amp; time already exist.</source>
        <translation>O registro de dados não pôde ser criado!

Já existe uma entrada para esta data e hora.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="212"/>
        <source>Data Record successfully created.</source>
        <translation>Registro de dados criado com sucesso.</translation>
    </message>
</context>
<context>
    <name>DialogSettings</name>
    <message>
        <location filename="../DialogSettings.cpp" line="183"/>
        <source>Choose Database Location</source>
        <translation>Escolha a localização do banco de dados</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="193"/>
        <source>Choose Database Backup Location</source>
        <translation>Escolha o local de backup do banco de dados</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="380"/>
        <source>The selected database already exists.

Please choose the preferred action.</source>
        <translation>O banco de dados selecionado já existe.

Escolha a ação preferida.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="382"/>
        <source>Overwrite</source>
        <translation>Sobrescrever</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="383"/>
        <source>Merge</source>
        <translation>Mesclar</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="384"/>
        <source>Swap</source>
        <translation>Trocar</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="386"/>
        <source>Overwrite:

Overwrites the selected database with the current database.

Merge:

Merges the selected database into the current database.

Swap:

Deletes the current database and use the selected database.</source>
        <translation>Sobrescrever:

Substitui o banco de dados selecionado pelo banco de dados atual.

Mesclar:

Mescla o banco de dados selecionado no banco de dados atual.

Trocar:

Exclui o banco de dados atual e usa o banco de dados selecionado.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="394"/>
        <source>The current database is empty.

The selected database will be erased on exit, if no data will be added.</source>
        <translation>O banco de dados atual está vazio.

O banco de dados selecionado será apagado ao sair, se nenhum dado for adicionado.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="425"/>
        <source>SQL encryption can&apos;t be enabled without password and will be disabled!</source>
        <translation>A criptografia SQL não pode ser ativada sem senha e será desativada!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="432"/>
        <source>The database backup should be located on a different hard disk, partition or directory.</source>
        <translation>O backup do banco de dados deve estar localizado em um disco rígido, partição ou diretório diferente.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="442"/>
        <source>Please enter valid values for additional information of user 1!</source>
        <translation>Insira valores válidos para informações adicionais do usuário 1!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="449"/>
        <source>Please enter valid values for additional information of user 2!</source>
        <translation>Insira valores válidos para informações adicionais do usuário 2!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="456"/>
        <source>Entered age doesn&apos;t match selected age group for user 1!</source>
        <translation>A idade inserida não corresponde à faixa etária selecionada para o usuário 1!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="463"/>
        <source>Entered age doesn&apos;t match selected age group for user 2!</source>
        <translation>A idade inserida não corresponde à faixa etária selecionada para o usuário 2!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="471"/>
        <source>Please enable symbols or lines for chart!</source>
        <translation>Ative símbolos ou linhas para o gráfico!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="478"/>
        <source>Please enter a valid e-mail address!</source>
        <translation>Por favor insira um endereço de e-mail válido!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="485"/>
        <source>Please enter a e-mail subject!</source>
        <translation>Por favor, digite um assunto de e-mail!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="492"/>
        <source>E-Mail message must contain $CHART, $TABLE and/or $STATS!</source>
        <translation>A mensagem de e-mail deve conter $CHART, $TABLE e/ou $STATS!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="591"/>
        <source>The auto hide setting has been changed.

Should this now be applied to all existing measurements?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="596"/>
        <source>Auto hide measurements:

     User 1 : %1 %3
     User 2 : %2 %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="596"/>
        <source>hidden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="596"/>
        <source>shown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="647"/>
        <source>User 1</source>
        <translation>Usuário 1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="661"/>
        <source>User 2</source>
        <translation>Usuário 2</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="785"/>
        <source>Blood Pressure Report</source>
        <translation>Relatório de Pressão Arterial</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="786"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Prezado Dr. House,

por favor, encontre em anexo meus dados de pressão arterial deste mês.

Atenciosamente,
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="838"/>
        <source>Abort setup and discard all changes?</source>
        <translation>Abortar a configuração e descartar todas as alterações?</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="20"/>
        <source>Settings</source>
        <translation>Configurações</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="44"/>
        <source>Database</source>
        <translation>Base de dados</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="75"/>
        <source>Location</source>
        <translation>Destino</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="81"/>
        <source>Current Location</source>
        <translation>Destino atual</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="97"/>
        <source>Change Location</source>
        <translation>Mudar Destino</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="123"/>
        <source>Encrypt With SQLCipher</source>
        <translation>Criptografar com SQLCipher</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="126"/>
        <source>Encryption</source>
        <translation>Criptografia</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="138"/>
        <source>Password</source>
        <translation>Senha</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="151"/>
        <source>Show Password</source>
        <translation>Mostrar Senha</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="454"/>
        <source>User</source>
        <translation>Usuário</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="489"/>
        <location filename="../DialogSettings.ui" line="752"/>
        <source>Male</source>
        <translation>Homem</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="518"/>
        <location filename="../DialogSettings.ui" line="781"/>
        <source>Female</source>
        <translation>Mulher</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="614"/>
        <location filename="../DialogSettings.ui" line="886"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="483"/>
        <location filename="../DialogSettings.ui" line="746"/>
        <source>Mandatory Information</source>
        <translation>Informação Obrigatória</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="544"/>
        <location filename="../DialogSettings.ui" line="816"/>
        <source>Age Group</source>
        <translation>Grupo de Idade</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="630"/>
        <location filename="../DialogSettings.ui" line="902"/>
        <source>Additional Information</source>
        <translation>Informações Adicionais</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="690"/>
        <location filename="../DialogSettings.ui" line="962"/>
        <source>Height</source>
        <translation>Altura</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="719"/>
        <location filename="../DialogSettings.ui" line="991"/>
        <source>Weight</source>
        <translation>Peso</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1016"/>
        <source>Device</source>
        <translation>Dispositivo</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1022"/>
        <location filename="../DialogSettings.cpp" line="77"/>
        <source>Import Plugins [ %1 ]</source>
        <translation>Importar Plugins [ %1 ]</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1070"/>
        <source>Open Website</source>
        <translation>Abrir Site</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1083"/>
        <source>Maintainer</source>
        <translation>Mantenedor</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1111"/>
        <source>Version</source>
        <translation>Versão</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1118"/>
        <source>Send E-Mail</source>
        <translation>Enviar E-Mail</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1131"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1138"/>
        <source>Producer</source>
        <translation>Produtor</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1155"/>
        <source>Chart</source>
        <translation>Gráfico</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1282"/>
        <source>X-Axis Range</source>
        <translation>Alcance do Eixo X</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1288"/>
        <source>Dynamic Scaling</source>
        <translation>Escala Dinâmica</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1269"/>
        <source>Healthy Ranges</source>
        <translation>Intervalos Saudáveis</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1186"/>
        <source>Symbols</source>
        <translation>Símbolos</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1263"/>
        <source>Colored Areas</source>
        <translation>Áreas Coloridas</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1035"/>
        <source>Please choose Device Plugin…</source>
        <translation>Escolha o Plug-in do Dispositivo…</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="338"/>
        <location filename="../DialogSettings.ui" line="1410"/>
        <location filename="../DialogSettings.ui" line="1685"/>
        <source>Systolic</source>
        <translation>Sistólica</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="347"/>
        <location filename="../DialogSettings.ui" line="1498"/>
        <location filename="../DialogSettings.ui" line="1773"/>
        <source>Diastolic</source>
        <translation>Diastólica</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="356"/>
        <location filename="../DialogSettings.ui" line="1586"/>
        <location filename="../DialogSettings.ui" line="1861"/>
        <source>Heartrate</source>
        <translation>Pulso</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1958"/>
        <source>Table</source>
        <translation>Tabela</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2043"/>
        <location filename="../DialogSettings.ui" line="2226"/>
        <source>Systolic Warnlevel</source>
        <translation>Nível de Alerta Sistólico</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1989"/>
        <location filename="../DialogSettings.ui" line="2286"/>
        <source>Diastolic Warnlevel</source>
        <translation>Nível de Alerta Diastólico</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="176"/>
        <source>Automatic Backup</source>
        <translation>Backup Automático</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="179"/>
        <source>Backup</source>
        <translation>Backup</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="196"/>
        <source>Current Backup Location</source>
        <translation>Local de Backup Atual</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="209"/>
        <source>Change Backup Location</source>
        <translation>Alterar Local de Backup</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="236"/>
        <source>Backup Mode</source>
        <translation>Modo de Backup</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="246"/>
        <source>Daily</source>
        <translation>Diário</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="255"/>
        <source>Weekly</source>
        <translation>Semanal</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="264"/>
        <source>Monthly</source>
        <translation>Mensal</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="276"/>
        <source>Keep Copies</source>
        <translation>Manter Cópias</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="311"/>
        <source>Auto Hide Measurements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="322"/>
        <source>Detection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="329"/>
        <source>Sum of All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="371"/>
        <source>Hide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="378"/>
        <source>Higher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="387"/>
        <source>Lower</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="404"/>
        <source>Time Span</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="658"/>
        <location filename="../DialogSettings.ui" line="930"/>
        <source>Birthday</source>
        <translation>Nascimento</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1195"/>
        <source>Color</source>
        <translation>Cor</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1219"/>
        <location filename="../DialogSettings.ui" line="1247"/>
        <source>Size</source>
        <translation>Tamanho</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1301"/>
        <source>Lines</source>
        <translation>Linhas</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1310"/>
        <location filename="../DialogSettings.ui" line="1338"/>
        <source>Width</source>
        <translation>Largura</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1367"/>
        <source>Show Heartrate</source>
        <translation>Mostrar Freq. Cardíaca</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1373"/>
        <source>Show Heartrate in Chart View</source>
        <translation>Mostrar Freq. Cardíaca na Visualização do Gráfico</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1383"/>
        <source>Print Heartrate</source>
        <translation>Imprimir Freq. Cardíaca</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1389"/>
        <source>Print Heartrate on separate Sheet</source>
        <translation>Imprimir Frequência Cardíaca em uma Folha separada</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2103"/>
        <location filename="../DialogSettings.ui" line="2340"/>
        <source>P.Pressure Warnlevel</source>
        <translation>Nível de Alerta para Pressão de Pulso</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2157"/>
        <location filename="../DialogSettings.ui" line="2394"/>
        <source>Heartrate Warnlevel</source>
        <translation>Nível de Alerta para Frequência Cardíaca</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2459"/>
        <source>Statistic</source>
        <translation>Estatística</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2465"/>
        <source>Bar Type</source>
        <translation>Tipo de Barra</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2471"/>
        <source>Show Median instead of Average Bars</source>
        <translation>Mostrar Mediana em vez de Barras Médias</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2481"/>
        <source>Legend Type</source>
        <translation>Tipo de Legenda</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2487"/>
        <source>Show Values as Legend instead of Descriptions</source>
        <translation>Mostrar Valores como Legenda em vez de Descrições</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2502"/>
        <source>Print</source>
        <translation type="unfinished">Imprimir</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2510"/>
        <source>Additional Colums</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2516"/>
        <source>Pulse Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2539"/>
        <source>Irregular Heartbeat</source>
        <translation type="unfinished">Arritmia Cardíaca</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2562"/>
        <source>Movement</source>
        <translation type="unfinished">Movimento</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2588"/>
        <source>Page Orientation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2594"/>
        <source>Portrait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2604"/>
        <source>Landscape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2616"/>
        <source>Header Appearance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2622"/>
        <source>Show Column Headers as Graphics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2640"/>
        <source>E-Mail</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2648"/>
        <source>Address</source>
        <translation>Endereço</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2667"/>
        <source>Subject</source>
        <translation>Assunto</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2685"/>
        <source>Message</source>
        <translation>Mensagem</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2706"/>
        <source>Plugin</source>
        <translation>Plugin</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2712"/>
        <source>General</source>
        <translation>Geral</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2718"/>
        <source>Log Device Communication to File</source>
        <translation>Registrar Comunicação do Dispositivo no Arquivo</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2725"/>
        <source>Import Measurements automatically</source>
        <translation>Importar Medições Automaticamente</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2735"/>
        <source>Bluetooth</source>
        <translation>Bluetooth</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2741"/>
        <source>Discover Device automatically</source>
        <translation>Descubra o dispositivo automaticamente</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2748"/>
        <source>Connect Device automatically</source>
        <translation>Conectar Dispositivo Automaticamente</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2763"/>
        <source>Update</source>
        <translation>Atualizar</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2769"/>
        <source>Autostart</source>
        <translation>Início automático</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2775"/>
        <source>Check for Online Updates at Program Startup</source>
        <translation>Verificar Atualizações Online ao Iniciar o Programa</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2788"/>
        <source>Notification</source>
        <translation>Notificação</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2794"/>
        <source>Always show Result after Online Update Check</source>
        <translation>Sempre mostrar o Resultado após a Verificação de Atualização Online</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2809"/>
        <source>Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2817"/>
        <source>Date / Short</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2843"/>
        <source>Date / Long</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2873"/>
        <source>Time / Short</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2899"/>
        <source>Time / Long</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2933"/>
        <source>Save</source>
        <translation>Salvar</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2950"/>
        <source>Reset</source>
        <translation>Reiniciar</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2967"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
</context>
<context>
    <name>DialogUpdate</name>
    <message>
        <location filename="../DialogUpdate.ui" line="14"/>
        <source>Online Update</source>
        <translation>Atualização Online</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="27"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="80"/>
        <source>Available Version</source>
        <translation>Versão Disponível</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="90"/>
        <source>Update File Size</source>
        <translation>Novo Tamanho do Arquivo</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="104"/>
        <source>Installed Version</source>
        <translation>Versão Instalada</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="156"/>
        <source>Details</source>
        <translation>Detalhes</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="181"/>
        <location filename="../DialogUpdate.ui" line="202"/>
        <source>Download</source>
        <translation>Download</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="222"/>
        <source>Ignore</source>
        <translation>Ignorar</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="183"/>
        <source>No new version found.</source>
        <translation>Nenhuma nova versão encontrada.</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogUpdate.cpp" line="42"/>
        <source>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problem(s):

%1
Do you wish to continue anyway?</source>
        <translation>
            <numerusform>!!! AVISO SSL - LEIA ATENTAMENTE!!!

Problema de conexão de rede:

%1
Deseja continuar mesmo assim?</numerusform>
            <numerusform>!!! AVISO SSL - LEIA ATENTAMENTE!!!

Problemas de conexão de rede:

%1
Deseja continuar mesmo assim?</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="73"/>
        <source>Downloading update failed!

%1</source>
        <translation>O download da atualização falhou!

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="73"/>
        <source>Checking update failed!

%1</source>
        <translation>A verificação da atualização falhou!

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="156"/>
        <source>Unexpected response from update server!</source>
        <translation>Resposta inesperada do servidor de atualização!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="173"/>
        <source>*%1 not found</source>
        <translation>*%1 não encontrado</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="203"/>
        <source>Update doesn&apos;t have expected size!

%L1 : %L2

Retry download…</source>
        <translation>A atualização não tem o tamanho esperado!

%L1 : %L2

Tente baixar novamente…</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="207"/>
        <source>Update saved to %1.

Start new version now?</source>
        <translation>Atualização salva em %1.

Iniciar nova versão agora?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="220"/>
        <source>Could not start new version!</source>
        <translation>Não foi possível iniciar a nova versão!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="247"/>
        <source>The program was installed by Flatpak.

Please update using the Flatpak internal update function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="253"/>
        <source>The program was installed by Snap.

Please update using the Snap internal update function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="259"/>
        <source>The program was installed from Windows store.

Please update using the store internal update function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="265"/>
        <source>The program was installed from distribution.

Please update using the operating systems internal update function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="271"/>
        <source>The program was installed from source code.

Please update the sources, rebuild and reinstall manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="297"/>
        <source>Really abort download?</source>
        <translation>Realmente abortar o download?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="227"/>
        <source>Could not save update to %1!

%2</source>
        <translation>Não foi possível salvar a atualização em %1!

%2</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="14"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Gerenciador Universal de Pressão Arterial</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="30"/>
        <source>Show Previous Period</source>
        <translation>Mostrar período anterior</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="104"/>
        <source>Show Next Period</source>
        <translation>Mostrar próximo período</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="143"/>
        <source>Chart View</source>
        <translation>Visualizar Gráfico</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="171"/>
        <source>Table View</source>
        <translation>Visualizar Tabela</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="211"/>
        <location filename="../MainWindow.cpp" line="2608"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="220"/>
        <location filename="../MainWindow.cpp" line="2609"/>
        <source>Time</source>
        <translation>Hora</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="229"/>
        <location filename="../MainWindow.cpp" line="159"/>
        <location filename="../MainWindow.cpp" line="5049"/>
        <source>Systolic</source>
        <translation>Sistólica</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="238"/>
        <location filename="../MainWindow.cpp" line="160"/>
        <location filename="../MainWindow.cpp" line="5050"/>
        <source>Diastolic</source>
        <translation>Diastólica</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="247"/>
        <source>P.Pressure</source>
        <translation>Pressão Pulso</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="265"/>
        <source>Irregular</source>
        <translation>Irregular</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="274"/>
        <source>Movement</source>
        <translation>Movimento</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="283"/>
        <source>Invisible</source>
        <translation>Invisível</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="292"/>
        <location filename="../MainWindow.cpp" line="2628"/>
        <location filename="../MainWindow.cpp" line="2641"/>
        <location filename="../MainWindow.cpp" line="2655"/>
        <location filename="../MainWindow.cpp" line="2669"/>
        <location filename="../MainWindow.cpp" line="2683"/>
        <location filename="../MainWindow.cpp" line="2698"/>
        <location filename="../MainWindow.cpp" line="2714"/>
        <location filename="../MainWindow.cpp" line="2728"/>
        <source>Comment</source>
        <translation>Comentário</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="309"/>
        <source>Statistic View</source>
        <translation>Visualizar Estatística</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="404"/>
        <source>¼ Hourly</source>
        <translation>¼ Hora</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="439"/>
        <source>½ Hourly</source>
        <translation>¼ Hora</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="471"/>
        <source>Hourly</source>
        <translation>Por Hora</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="506"/>
        <source>¼ Daily</source>
        <translation>¼ Dia</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="538"/>
        <source>½ Daily</source>
        <translation>½ Dia</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="570"/>
        <source>Daily</source>
        <translation>Diário</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="605"/>
        <source>3 Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="637"/>
        <source>Weekly</source>
        <translation>Semanal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="669"/>
        <source>Monthly</source>
        <translation>Mensal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="701"/>
        <source>Quarterly</source>
        <translation>Trimestral</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="733"/>
        <source>½ Yearly</source>
        <translation>½ Ano</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="765"/>
        <source>Yearly</source>
        <translation>Anual</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="825"/>
        <source>Last 3 Days</source>
        <translation type="unfinished">Últimos 28 Dias {3 ?}</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="857"/>
        <source>Last 7 Days</source>
        <translation>Últimos 7 Dias</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="889"/>
        <source>Last 14 Days</source>
        <translation>Últimos 14 Dias</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="921"/>
        <source>Last 21 Days</source>
        <translation>Últimos 21 Dias</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="953"/>
        <source>Last 28 Days</source>
        <translation>Últimos 28 Dias</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="985"/>
        <source>Last 3 Months</source>
        <translation>Últimos 3 Meses</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1017"/>
        <source>Last 6 Months</source>
        <translation>Últimos 6 Meses</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1049"/>
        <source>Last 9 Months</source>
        <translation>Últimos 9 Meses</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1081"/>
        <source>Last 12 Months</source>
        <translation>Últimos 12 Meses</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1113"/>
        <source>All Records</source>
        <translation>Todos os Registros</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1160"/>
        <source>File</source>
        <translation>Arquivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1164"/>
        <source>Print</source>
        <translation>Imprimir</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1186"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1203"/>
        <source>Configuration</source>
        <translation>Configuração</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1207"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1216"/>
        <source>Language</source>
        <translation>Linguagem</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1225"/>
        <source>Style</source>
        <translation>Estilo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1234"/>
        <source>Charts</source>
        <translation>Gráficos</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1259"/>
        <source>Database</source>
        <translation>Banco de Dados</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1263"/>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1275"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1290"/>
        <source>Clear</source>
        <translation>Limpar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1379"/>
        <source>Quit</source>
        <translation>Sair</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1382"/>
        <location filename="../MainWindow.ui" line="1385"/>
        <source>Quit Program</source>
        <translation>Sair do Programa</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1397"/>
        <source>About</source>
        <translation>Sobre</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1400"/>
        <location filename="../MainWindow.ui" line="1403"/>
        <source>About Program</source>
        <translation>Sobre o Programa</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1412"/>
        <source>Guide</source>
        <translation>Guia</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1415"/>
        <location filename="../MainWindow.ui" line="1418"/>
        <source>Show Guide</source>
        <translation>Mostrar Guia</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1427"/>
        <source>Update</source>
        <translation>Atualizar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1430"/>
        <location filename="../MainWindow.ui" line="1433"/>
        <source>Check Update</source>
        <translation>Verificar Atualização</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1592"/>
        <source>To PDF</source>
        <translation>Para PDF</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1595"/>
        <location filename="../MainWindow.ui" line="1598"/>
        <source>Export To PDF</source>
        <translation>Exportar para PDF</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1607"/>
        <source>Migration</source>
        <translation>Migração</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1610"/>
        <location filename="../MainWindow.ui" line="1613"/>
        <source>Migrate from Vendor</source>
        <translation>Migrar do Fornecedor</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1857"/>
        <source>Translation</source>
        <translation>Tradução</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1860"/>
        <location filename="../MainWindow.ui" line="1863"/>
        <source>Contribute Translation</source>
        <translation>Contribuir com tradução</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1872"/>
        <source>E-Mail</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1875"/>
        <location filename="../MainWindow.ui" line="1878"/>
        <source>Send E-Mail</source>
        <translation>Enviar E-Mail</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1890"/>
        <source>Icons</source>
        <translation>Ícones</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1893"/>
        <location filename="../MainWindow.ui" line="1896"/>
        <source>Change Icon Color</source>
        <translation>Mudar Cor do Ícone</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1907"/>
        <source>System</source>
        <translation>Sistema</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1910"/>
        <location filename="../MainWindow.ui" line="1913"/>
        <source>System Colors</source>
        <translation>Cores do Sistema</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1921"/>
        <source>Light</source>
        <translation>Claro</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1924"/>
        <location filename="../MainWindow.ui" line="1927"/>
        <source>Light Colors</source>
        <translation>Cores Claras</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1935"/>
        <source>Dark</source>
        <translation>Escuro</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1938"/>
        <location filename="../MainWindow.ui" line="1941"/>
        <source>Dark Colors</source>
        <translation>Cores Escuras</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1950"/>
        <source>Donation</source>
        <translation>Doação</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1965"/>
        <location filename="../MainWindow.cpp" line="3840"/>
        <source>Distribution</source>
        <translation>Distribuição</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1968"/>
        <location filename="../MainWindow.ui" line="1971"/>
        <source>Show Distribution</source>
        <translation>Mostrar Distribuição</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1983"/>
        <location filename="../MainWindow.ui" line="1986"/>
        <source>Save</source>
        <translation type="unfinished">Salvar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1995"/>
        <source>Wiki</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1998"/>
        <location filename="../MainWindow.ui" line="2001"/>
        <source>Read Wiki</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="256"/>
        <location filename="../MainWindow.cpp" line="161"/>
        <location filename="../MainWindow.cpp" line="5051"/>
        <source>Heartrate</source>
        <translation>Pulso</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1953"/>
        <location filename="../MainWindow.ui" line="1956"/>
        <source>Make Donation</source>
        <translation>Fazer uma Doação</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1445"/>
        <source>Bugreport</source>
        <translation>Relatório de erro</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1448"/>
        <location filename="../MainWindow.ui" line="1451"/>
        <source>Send Bugreport</source>
        <translation>Enviar relatório de bug</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1460"/>
        <source>Settings</source>
        <translation>Configurações</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1463"/>
        <location filename="../MainWindow.ui" line="1466"/>
        <source>Change Settings</source>
        <translation>Mudar Configurações</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1481"/>
        <source>From Device</source>
        <translation>Do Dispositivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1484"/>
        <location filename="../MainWindow.ui" line="1487"/>
        <source>Import From Device</source>
        <translation>Importar do Dispositivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1499"/>
        <source>From File</source>
        <translation>Do Arquivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1502"/>
        <location filename="../MainWindow.ui" line="1505"/>
        <source>Import From File</source>
        <translation>Importar do Arquivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1514"/>
        <source>From Input</source>
        <translation>Da Entrada</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1517"/>
        <location filename="../MainWindow.ui" line="1520"/>
        <source>Import From Input</source>
        <translation>Importar da Entrada</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1529"/>
        <source>To CSV</source>
        <translation>Para CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1532"/>
        <location filename="../MainWindow.ui" line="1535"/>
        <source>Export To CSV</source>
        <translation>Exportar para CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1547"/>
        <source>To XML</source>
        <translation>Para XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1550"/>
        <location filename="../MainWindow.ui" line="1553"/>
        <source>Export To XML</source>
        <translation>Exportar para XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1562"/>
        <source>To JSON</source>
        <translation>Para JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1565"/>
        <location filename="../MainWindow.ui" line="1568"/>
        <source>Export To JSON</source>
        <translation>Exportar para JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1577"/>
        <source>To SQL</source>
        <translation>Para SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1580"/>
        <location filename="../MainWindow.ui" line="1583"/>
        <source>Export To SQL</source>
        <translation>Exportar para SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1625"/>
        <source>Print Chart</source>
        <translation>Imprimir Gráfico</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1628"/>
        <location filename="../MainWindow.ui" line="1631"/>
        <source>Print Chart View</source>
        <translation>Imprimir Visualização do Gráfico</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1640"/>
        <source>Print Table</source>
        <translation>Imprimir Tabela</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1643"/>
        <location filename="../MainWindow.ui" line="1646"/>
        <source>Print Table View</source>
        <translation>Imprimir Visualização da Tabela</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1655"/>
        <source>Print Statistic</source>
        <translation>Imprimir Estatística</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1658"/>
        <location filename="../MainWindow.ui" line="1661"/>
        <source>Print Statistic View</source>
        <translation>Imprimir Visualização da Estatística</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1670"/>
        <source>Preview Chart</source>
        <translation>Pré-visualizar Gráfico</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1673"/>
        <location filename="../MainWindow.ui" line="1676"/>
        <source>Preview Chart View</source>
        <translation>Visualizar a Prévia do Gráfico</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1685"/>
        <source>Preview Table</source>
        <translation>Pré-visualizar Tabela</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1688"/>
        <location filename="../MainWindow.ui" line="1691"/>
        <source>Preview Table View</source>
        <translation>Visualizar a Prévia da Tabela</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1700"/>
        <source>Preview Statistic</source>
        <translation>Pré-visualizar Estatística</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1703"/>
        <location filename="../MainWindow.ui" line="1706"/>
        <source>Preview Statistic View</source>
        <translation>Visualizar a Prévia da Estatística</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1715"/>
        <location filename="../MainWindow.ui" line="1718"/>
        <location filename="../MainWindow.ui" line="1721"/>
        <source>Clear All</source>
        <translation>Limpar Tudo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1733"/>
        <location filename="../MainWindow.ui" line="1736"/>
        <location filename="../MainWindow.ui" line="1739"/>
        <source>Clear User 1</source>
        <translation>Limpar Usuário 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1748"/>
        <location filename="../MainWindow.ui" line="1751"/>
        <location filename="../MainWindow.ui" line="1754"/>
        <source>Clear User 2</source>
        <translation>Limpar Usuário 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1814"/>
        <source>Analysis</source>
        <translation>Análise</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1817"/>
        <location filename="../MainWindow.ui" line="1820"/>
        <source>Analyze Records</source>
        <translation>Analisar Registros</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1839"/>
        <location filename="../MainWindow.ui" line="1842"/>
        <location filename="../MainWindow.ui" line="1845"/>
        <source>Time Mode</source>
        <translation>Modo de Tempo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="108"/>
        <location filename="../MainWindow.cpp" line="109"/>
        <location filename="../MainWindow.cpp" line="5019"/>
        <location filename="../MainWindow.cpp" line="5020"/>
        <source>Records For Selected User</source>
        <translation>Registros para Usuário Selecionado</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="116"/>
        <location filename="../MainWindow.cpp" line="117"/>
        <location filename="../MainWindow.cpp" line="5022"/>
        <location filename="../MainWindow.cpp" line="5023"/>
        <source>Select Date &amp; Time</source>
        <translation>Selecione Data e Hora</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="169"/>
        <location filename="../MainWindow.cpp" line="5053"/>
        <source>Systolic - Value Range</source>
        <translation>Sistólica - Intervalo de Valores</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="170"/>
        <location filename="../MainWindow.cpp" line="5054"/>
        <source>Diastolic - Value Range</source>
        <translation>Diastólica - Intervalo de Valores</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="173"/>
        <location filename="../MainWindow.cpp" line="5056"/>
        <source>Systolic - Target Area</source>
        <translation>Sistólica - Área Alvo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="174"/>
        <location filename="../MainWindow.cpp" line="5057"/>
        <source>Diastolic - Target Area</source>
        <translation>Diastólica - Área Alvo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1711"/>
        <source>Athlete</source>
        <translation>Atleta</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1712"/>
        <source>Excellent</source>
        <translation>Excelente</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1712"/>
        <source>Optimal</source>
        <translation>Ideal</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1713"/>
        <source>Great</source>
        <translation>Ótimo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1714"/>
        <source>Good</source>
        <translation>Bom</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1713"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1775"/>
        <location filename="../MainWindow.ui" line="1778"/>
        <location filename="../MainWindow.ui" line="1799"/>
        <location filename="../MainWindow.ui" line="1802"/>
        <location filename="../MainWindow.cpp" line="102"/>
        <location filename="../MainWindow.cpp" line="103"/>
        <location filename="../MainWindow.cpp" line="104"/>
        <location filename="../MainWindow.cpp" line="105"/>
        <location filename="../MainWindow.cpp" line="4737"/>
        <location filename="../MainWindow.cpp" line="4738"/>
        <location filename="../MainWindow.cpp" line="4739"/>
        <location filename="../MainWindow.cpp" line="4740"/>
        <location filename="../MainWindow.cpp" line="5083"/>
        <location filename="../MainWindow.cpp" line="5084"/>
        <location filename="../MainWindow.cpp" line="5085"/>
        <location filename="../MainWindow.cpp" line="5086"/>
        <source>Switch To %1</source>
        <translation>Mudar Para %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="102"/>
        <location filename="../MainWindow.cpp" line="103"/>
        <location filename="../MainWindow.cpp" line="610"/>
        <location filename="../MainWindow.cpp" line="4737"/>
        <location filename="../MainWindow.cpp" line="4738"/>
        <location filename="../MainWindow.cpp" line="5083"/>
        <location filename="../MainWindow.cpp" line="5084"/>
        <source>User 1</source>
        <translation>Usuário 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="104"/>
        <location filename="../MainWindow.cpp" line="105"/>
        <location filename="../MainWindow.cpp" line="620"/>
        <location filename="../MainWindow.cpp" line="4739"/>
        <location filename="../MainWindow.cpp" line="4740"/>
        <location filename="../MainWindow.cpp" line="5085"/>
        <location filename="../MainWindow.cpp" line="5086"/>
        <source>User 2</source>
        <translation>Usuário 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="171"/>
        <location filename="../MainWindow.cpp" line="5055"/>
        <source>Heartrate - Value Range</source>
        <translation>Freq. Cardíaca - Intervalo de Valores</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="175"/>
        <location filename="../MainWindow.cpp" line="5058"/>
        <source>Heartrate - Target Area</source>
        <translation>Freq. Cardíaca - Área Alvo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="341"/>
        <source>No active device plugin, auto import aborted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="358"/>
        <source>Some plugins will not work without Bluetooth permission.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="461"/>
        <source>Could not create backup directory.

Please check backup location setting.</source>
        <translation>Não foi possível criar o diretório de backup.

Verifique a configuração do local de backup.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="490"/>
        <location filename="../MainWindow.cpp" line="513"/>
        <source>Could not backup database:

%1</source>
        <translation>Não foi possível fazer backup do banco de dados:

%1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="503"/>
        <source>Could not delete outdated backup:

%1</source>
        <translation>Não foi possível excluir o backup desatualizado:

%1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="530"/>
        <source>Could not register icons.</source>
        <translation>Não foi possível registrar os ícones.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="681"/>
        <source>Blood Pressure Report</source>
        <translation>Relatório de Pressão Arterial</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1223"/>
        <source>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</source>
        <translation>SYS : Ø %1 / x̃ %4 | DIA : Ø %2 / x̃ %5 | BPM: Ø %3 / x̃ %6</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1228"/>
        <source>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</source>
        <translation>SYS : Ø 0 / x̃ 0 | DIA : Ø 0 / x̃ 0 | BPM: Ø 0 / x̃ 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1711"/>
        <source>Low</source>
        <translation>Baixa</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1714"/>
        <source>High Normal</source>
        <translation>Normal Elevada</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1715"/>
        <location filename="../MainWindow.cpp" line="2192"/>
        <location filename="../MainWindow.cpp" line="5062"/>
        <location filename="../MainWindow.cpp" line="5066"/>
        <location filename="../MainWindow.cpp" line="5070"/>
        <location filename="../MainWindow.h" line="195"/>
        <location filename="../MainWindow.h" line="199"/>
        <location filename="../MainWindow.h" line="203"/>
        <source>Average</source>
        <translation>Regular</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1715"/>
        <source>Hyper 1</source>
        <translation>Hipertensão 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1716"/>
        <source>Below Average</source>
        <translation>Abaixo da Média</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1716"/>
        <source>Hyper 2</source>
        <translation>Hipertensão 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1717"/>
        <source>Poor</source>
        <translation>Ruim</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1717"/>
        <source>Hyper 3</source>
        <translation>Hipertensão 3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1862"/>
        <source>Scanning import plugin &quot;%1&quot; failed!

%2</source>
        <translation>A verificação do plugin de importação &quot;%1&quot; falhou!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1879"/>
        <location filename="../MainWindow.cpp" line="1906"/>
        <location filename="../MainWindow.cpp" line="5029"/>
        <source>Switch Language to %1</source>
        <translation>Mudar o idioma para %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1932"/>
        <location filename="../MainWindow.cpp" line="1947"/>
        <location filename="../MainWindow.cpp" line="5037"/>
        <source>Switch Theme to %1</source>
        <translation>Mudar Tema para %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1972"/>
        <location filename="../MainWindow.cpp" line="1992"/>
        <location filename="../MainWindow.cpp" line="5045"/>
        <source>Switch Style to %1</source>
        <translation>Mudar Estilo para %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2063"/>
        <location filename="../MainWindow.cpp" line="2088"/>
        <source>Edit record</source>
        <translation>Editar registro</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2192"/>
        <location filename="../MainWindow.cpp" line="5063"/>
        <location filename="../MainWindow.cpp" line="5067"/>
        <location filename="../MainWindow.cpp" line="5071"/>
        <location filename="../MainWindow.h" line="196"/>
        <location filename="../MainWindow.h" line="200"/>
        <location filename="../MainWindow.h" line="204"/>
        <source>Median</source>
        <translation>Mediana</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2194"/>
        <source>Click to swap Average and Median</source>
        <translation>Clique para trocar Média e Mediana</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2289"/>
        <source>Click to swap Legend and Label</source>
        <translation>Clique para trocar Legenda e Rótulo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2328"/>
        <source>No records to preview for selected time range!</source>
        <translation>Nenhum registro para visualizar no intervalo de tempo selecionado!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2360"/>
        <source>No records to print for selected time range!</source>
        <translation>Nenhum registro para imprimir para o intervalo de tempo selecionado!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2417"/>
        <source>User %1</source>
        <translation>Usuário %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3001"/>
        <source>Could not create e-mail because generating base64 for attachment &quot;%1&quot; failed!

%2</source>
        <translation>Não foi possível criar o e-mail porque a geração de base64 para o anexo &quot;%1&quot; falhou!

%2</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="3205"/>
        <location filename="../MainWindow.cpp" line="4491"/>
        <source>Skipped %n hidden record(s)!</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3844"/>
        <location filename="../MainWindow.cpp" line="4645"/>
        <source>Chart</source>
        <translation>Gráfico</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3844"/>
        <location filename="../MainWindow.cpp" line="4662"/>
        <source>Table</source>
        <translation>Tabela</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3844"/>
        <location filename="../MainWindow.cpp" line="4679"/>
        <source>Statistic</source>
        <translation>Estatística</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4603"/>
        <source>No records to analyse!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4615"/>
        <source>No records to display for selected time range!</source>
        <translation>Nenhum registro a ser exibido para o intervalo de tempo selecionado!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4697"/>
        <source>Could not open e-mail &quot;%1&quot;!

%2</source>
        <translation>Não foi possível abrir o e-mail &quot;%1&quot;!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4707"/>
        <source>Could not start e-mail client!</source>
        <translation>Não foi possível iniciar o cliente de e-mail!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4712"/>
        <source>No records to e-mail for selected time range!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4718"/>
        <source>Select Icon Color</source>
        <translation>Selecione a Cor do Ícone</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4724"/>
        <source>Restart application to apply new icon color.</source>
        <translation>Reinicie o aplicativo para aplicar a nova cor do ícone.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="902"/>
        <source>The database can not be saved.

Please choose another location in the settings or all data will be lost.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2412"/>
        <source>Created with UBPM for
Windows / Linux / macOS</source>
        <translation>Gerado por UBPM para
Windows / Linux / macOS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2413"/>
        <source>Free and OpenSource
https://codeberg.org/lazyt/ubpm</source>
        <translation>Livre e Código Aberto
https://codeberg.org/lazyt/ubpm</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3153"/>
        <source>Import from CSV/XML/JSON/SQL</source>
        <translation>Importar de CSV/XML/JSON/SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3153"/>
        <source>CSV File (*.csv);;XML File (*.xml);;JSON File (*.json);;SQL File (*.sql)</source>
        <translation>Arquivo CSV (*.csv);;Arquivo XML (*.xml);;Arquivo JSON (*.json);;Arquivo SQL (*.sql)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3216"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>Não foi possível abrir &quot;%1&quot;!

Motivo: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1222"/>
        <source>Measurements : %1  |  Irregular : %2  |  Movement : %3</source>
        <translation>Medições : %1 | Irregular: %2 | Movimento: %3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="682"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Prezado Dr. House,

por favor, encontre em anexo meus dados de pressão arterial para este mês.

Atenciosamente,
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1227"/>
        <source>Measurements : 0  |  Irregular : 0  |  Movement : 0</source>
        <translation>Medidas: 0 | Irregular : 0 | Movimento: 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2417"/>
        <source>%1
Age: %2, Height: %3 cm, Weight: %4 kg, BMI: %5</source>
        <translation>%1
Idade: %2, Altura: %3 cm, Peso: %4 kg, IMC: %5</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="3191"/>
        <location filename="../MainWindow.cpp" line="4482"/>
        <source>Successfully imported %n record(s) from %1.

     User 1 : %2
     User 2 : %3</source>
        <translation>
            <numerusform>%n registro importado com sucesso de %1.

      Usuário 1: %2
      Usuário 2: %3</numerusform>
            <numerusform>%n registros importados com sucesso de %1.

      Usuário 1: %2
      Usuário 2: %3</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="3195"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>%n registro inválido ignorado!</numerusform>
            <numerusform>%n registros inválidos ignorados!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="3200"/>
        <location filename="../MainWindow.cpp" line="4486"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>%n registro duplicado ignorado!</numerusform>
            <numerusform>%n registros duplicados ignorados!</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3586"/>
        <source>Doesn&apos;t look like a UBPM database!

Maybe wrong encryption settings/password?</source>
        <translation>Não parece um banco de dados UBPM!

Talvez configurações de criptografia/senha incorretas?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3639"/>
        <location filename="../MainWindow.cpp" line="3643"/>
        <source>Export to %1</source>
        <translation>Exportar para %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3639"/>
        <location filename="../MainWindow.cpp" line="3643"/>
        <source>%1 File (*.%2)</source>
        <translation>%1 Arquivo (*.%2)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3679"/>
        <source>Could not create &quot;%1&quot;!

Reason: %2</source>
        <translation>Não foi possível criar &quot;%1&quot;!

Motivo: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3685"/>
        <source>The database is empty, no records to export!</source>
        <translation>O banco de dados está vazio, sem registros para exportar!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4052"/>
        <source>Morning</source>
        <translation>Manhã</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4052"/>
        <source>Afternoon</source>
        <translation>Tarde</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4108"/>
        <source>Week</source>
        <translation>Semana</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4163"/>
        <source>Quarter</source>
        <translation>Trimestre</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4192"/>
        <source>Half Year</source>
        <translation>Semestral</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4210"/>
        <source>Year</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4416"/>
        <source>Really delete all records for user %1?</source>
        <translation>Deseja realmente excluir todos os registros do usuário %1?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4431"/>
        <source>All records for user %1 deleted and existing database saved to &quot;ubpm.sql.bak&quot;.</source>
        <translation>Todos os registros do usuário %1 excluídos e o banco de dados existente salvo em &quot;ubpm.sql.bak&quot;.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4570"/>
        <source>Really delete all records?</source>
        <translation>Realmente excluir todos os registros?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4581"/>
        <source>All records deleted and existing database &quot;ubpm.sql&quot; moved to trash.</source>
        <translation>Todos os registros excluídos e o banco de dados existente &quot;ubpm.sql&quot; movido para a lixeira.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4901"/>
        <source>- UBPM Application
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4916"/>
        <source>- UBPM Plugins
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5060"/>
        <location filename="../MainWindow.cpp" line="5064"/>
        <location filename="../MainWindow.cpp" line="5068"/>
        <location filename="../MainWindow.h" line="193"/>
        <location filename="../MainWindow.h" line="197"/>
        <location filename="../MainWindow.h" line="201"/>
        <source>Minimum</source>
        <translation>Mínimo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5061"/>
        <location filename="../MainWindow.cpp" line="5065"/>
        <location filename="../MainWindow.cpp" line="5069"/>
        <location filename="../MainWindow.h" line="194"/>
        <location filename="../MainWindow.h" line="198"/>
        <location filename="../MainWindow.h" line="202"/>
        <source>Maximum</source>
        <translation>Máximo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5134"/>
        <source>Could not open theme &quot;%1&quot; file!

Reason: %2</source>
        <translation>Não foi possível abrir o arquivo de tema &quot;%1&quot;!

Motivo: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2060"/>
        <location filename="../MainWindow.cpp" line="2077"/>
        <location filename="../MainWindow.cpp" line="6020"/>
        <source>Delete record</source>
        <translation>Apagar registro</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5947"/>
        <source>Show Heartrate</source>
        <translation>Mostrar Freq. Cardíaca</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5983"/>
        <location filename="../MainWindow.cpp" line="5994"/>
        <source>Symbols and lines can&apos;t be disabled both!</source>
        <translation>Símbolos e linhas não podem ser desabilitados!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="6022"/>
        <source>Show record</source>
        <translation>Mostrar registro</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2061"/>
        <location filename="../MainWindow.cpp" line="2084"/>
        <location filename="../MainWindow.cpp" line="6023"/>
        <source>Hide record</source>
        <translation>Ocultar registro</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2079"/>
        <location filename="../MainWindow.cpp" line="6034"/>
        <source>Really delete selected record?</source>
        <translation>Realmente excluir o registro selecionado?</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="5013"/>
        <source>The following %n translation(s) for &quot;%1&quot; could not be loaded:

%2
Hide this warning on program startup?</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5916"/>
        <source>Scrolling has reached top/bottom, show next/previous period?</source>
        <translation>A rolagem atingiu a parte superior/inferior, mostrar o período seguinte/anterior?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5939"/>
        <source>Dynamic Scaling</source>
        <translation>Escala Dinâmica</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5940"/>
        <source>Colored Stripes</source>
        <translation>Listras Coloridas</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5942"/>
        <source>Show Symbols</source>
        <translation>Mostrar Símbolos</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5943"/>
        <source>Show Lines</source>
        <translation>Mostrar Linhas</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5945"/>
        <source>Colored Symbols</source>
        <translation>Símbolos coloridos</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="6055"/>
        <source>Show Median</source>
        <translation>Mostrar Mediana</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="6056"/>
        <source>Show Values</source>
        <translation>Mostrar valores</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="6111"/>
        <source>Really quit program?</source>
        <translation>Realmente sair do programa?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../MainWindow.h" line="4"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Gerenciador Universal de Pressão Arterial</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="13"/>
        <source>The application is already running and multiple instances are not allowed.

Switch to the running instance or close it and try again.</source>
        <translation>O aplicativo já está em execução e várias instâncias não são permitidas.

Alterne para a instância em execução ou feche-a e tente novamente.</translation>
    </message>
</context>
</TS>
