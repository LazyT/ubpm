<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>DialogAbout</name>
    <message>
        <location filename="../DialogAbout.ui" line="14"/>
        <source>About UBPM</source>
        <translation>À propos de UBPM</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="41"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="53"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Universal Blood Pressure Manager</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="91"/>
        <source>Qt Framework</source>
        <translation>Framework Qt</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="129"/>
        <source>Operating System</source>
        <translation>Système d&apos;exploitation</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="220"/>
        <source>Environment</source>
        <translation>Environnement</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="231"/>
        <source>Settings</source>
        <translation>Réglages</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="266"/>
        <source>Database</source>
        <translation>Base de données</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="301"/>
        <source>Cache</source>
        <translation>Cache</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="336"/>
        <source>Guides</source>
        <translation>Guides</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="371"/>
        <source>Languages</source>
        <translation>Langues</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="406"/>
        <source>Plugins</source>
        <translation>Extensions</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="441"/>
        <source>Themes</source>
        <translation>Thèmes</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="469"/>
        <source>Translators</source>
        <translation>Traducteurs</translation>
    </message>
    <message>
        <location filename="../DialogAbout.cpp" line="14"/>
        <source>The program is provided as is with no warranty of any kind, including the warranty of design, merchantability and fitness for a particular purpose.</source>
        <translation>Le programme est fourni comme n&apos;a aucune garantie, y compris la garantie du design, de la négociabilité et de la condition physique pour un but particulier.</translation>
    </message>
    <message>
        <location filename="../DialogAbout.cpp" line="37"/>
        <source>Could not open &quot;%1&quot;.</source>
        <translation>Impossible d&apos;ouvrir «&#xa0;%1&#xa0;»</translation>
    </message>
</context>
<context>
    <name>DialogAnalysis</name>
    <message>
        <location filename="../DialogAnalysis.ui" line="14"/>
        <source>Data Analysis</source>
        <translation>Analyse des données</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="24"/>
        <source>Query</source>
        <translation>Requête</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="43"/>
        <location filename="../DialogAnalysis.cpp" line="209"/>
        <source>Results</source>
        <translation>Résultats</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="82"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="91"/>
        <source>Time</source>
        <translation>Heure</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="100"/>
        <source>Systolic</source>
        <translation>Systolique</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="109"/>
        <source>Diastolic</source>
        <translation>Distolique</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="118"/>
        <source>P.Pressure</source>
        <translation>Pression pulsée</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="127"/>
        <source>Heartrate</source>
        <translation>Fréquence cardiaque</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="136"/>
        <source>Irregular</source>
        <translation>Irrégulier</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="145"/>
        <source>Movement</source>
        <translation>Mouvement</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="154"/>
        <source>Invisible</source>
        <translation>Invisible</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="163"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="193"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="51"/>
        <source>Could not create memory database!

%1</source>
        <translation>Impossible de créer une base de données mémoire&#xa0;!

%1</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>No results for this query found!</source>
        <translation>Aucun résultat pour cette requête n&apos;a été trouvé&#xa0;!</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>%1 for %2 [ %3 %4 | %5 %6 | %7% ]</source>
        <translation>%1 for %2 [ %3 %4 | %5 %6 | %7% ]</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Result(s)</source>
        <translation>
            <numerusform>Résultat</numerusform>
            <numerusform>Résultats</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Match(es)</source>
        <translation>
            <numerusform>Correspondance</numerusform>
            <numerusform>Correspondances</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Record(s)</source>
        <translation>
            <numerusform>Enregistrement</numerusform>
            <numerusform>Enregistrements</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>User %1</source>
        <translation>Utilisateur %1</translation>
    </message>
</context>
<context>
    <name>DialogDistribution</name>
    <message>
        <location filename="../DialogDistribution.ui" line="20"/>
        <source>Data Distribution</source>
        <translation>Répartition des données</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="77"/>
        <source>Preview</source>
        <translation>Aperçu</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="94"/>
        <source>Print</source>
        <translation>Imprimer</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="128"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogDistribution.cpp" line="18"/>
        <source>Record(s)</source>
        <translation>
            <numerusform>Enregistrement</numerusform>
            <numerusform>Enregistrements</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="113"/>
        <source>Low</source>
        <translation>Basse</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="115"/>
        <source>Optimal</source>
        <translation>Optimale</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="117"/>
        <source>Normal</source>
        <translation>Normale</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="119"/>
        <source>High Normal</source>
        <translation>Haute normale</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="121"/>
        <source>Hyper 1</source>
        <translation>Hyper 1</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="123"/>
        <source>Hyper 2</source>
        <translation>Hyper 2</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="125"/>
        <source>Hyper 3</source>
        <translation>Hyper 3</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="142"/>
        <source>SYS</source>
        <translation>SYS</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="154"/>
        <source>DIA</source>
        <translation>DIA</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="165"/>
        <source>Blood Pressure</source>
        <translation>Pression artérielle</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="203"/>
        <source>Athlete</source>
        <translation>Athlète</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="205"/>
        <source>Excellent</source>
        <translation>Excellente</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="207"/>
        <source>Great</source>
        <translation>Superbe</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="209"/>
        <source>Good</source>
        <translation>Bonne</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="211"/>
        <source>Average</source>
        <translation>Moyenne</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="213"/>
        <source>Below Average</source>
        <translation>Sous la moyenne</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="215"/>
        <source>Poor</source>
        <translation>Pauvre</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="232"/>
        <source>BPM</source>
        <translation>BPM</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="258"/>
        <source>Heart Rate</source>
        <translation>Fréquence cardiaque</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="111"/>
        <location filename="../DialogDistribution.cpp" line="419"/>
        <source>Export To PDF</source>
        <translation>Exporter vers PDF</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="419"/>
        <source>%1 File (*.%2)</source>
        <translation>%1 Fichier (*.%2)</translation>
    </message>
</context>
<context>
    <name>DialogDonation</name>
    <message>
        <location filename="../DialogDonation.ui" line="14"/>
        <source>Donation</source>
        <translation>Don</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="67"/>
        <source>Copy E-Mail and open Amazon</source>
        <translation>Copier le courriel et ouvrir Amazon</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="154"/>
        <source>Open PayPal (+)</source>
        <translation>Ouvrir PayPal (+)</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="171"/>
        <source>Open PayPal (-)</source>
        <translation>Ouvrir PayPal (-)</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="46"/>
        <source>If you have an Amazon account, you can send money by gift card.

Click the button below to open the Amazon website in your browser and

- sign in to your account
- select or enter the amount you would like to send
- select e-mail delivery
- paste the e-mail address as the recipient
- add a message
- complete the donation

You can also scan the QR code with your mobile device.</source>
        <translation>Si vous avez un compte Amazon, vous pouvez envoyer de l&apos;argent par carte-cadeau.

Cliquez sur le bouton ci-dessous pour ouvrir le site web d&apos;Amazon dans votre navigateur et

- connectez-vous à votre compte
- sélectionnez ou saisissez le montant que vous souhaitez envoyer
- sélectionnez l&apos;envoi par courrier électronique
- collez l&apos;adresse électronique en tant que destinataire
- ajouter un message
- compléter le don

Vous pouvez également scanner le code QR avec votre appareil mobile.</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="124"/>
        <source>If you have a PayPal account, you can send money using the &quot;Friends and Family&quot; feature.

Open your PayPal app and

- search for @LazyT (Thomas Löwe)
- enter the amount you would like to send
- add a message
- complete the donation

You can also click the first button below to do this in your browser without using the app.

If you do not have a PayPal account but do have a credit/debit card click the second button below to go to the PayPal donation page and

- select onetime or repeating donation
- select or enter the amount you would like to send
- optionally check the box to cover the fees
- complete the donation

You can also scan the QR code with your mobile device.</source>
        <translation>Si vous avez un compte PayPal, vous pouvez envoyer de l&apos;argent en utilisant la fonction « Amis et famille ».

Ouvrez votre application PayPal et

- recherchez @LazyT (Thomas Löwe)
- entrez le montant que vous souhaitez envoyer
- ajoutez un message
- compléter le don

Vous pouvez également cliquer sur le premier bouton ci-dessous pour effectuer cette opération dans votre navigateur sans utiliser l&apos;application.

Si vous n&apos;avez pas de compte PayPal mais que vous possédez une carte de crédit/débit, cliquez sur le deuxième bouton ci-dessous pour accéder à la page de don PayPal et

- sélectionnez un don unique ou répétitif
- sélectionnez ou entrez le montant que vous souhaitez envoyer
- cochez éventuellement la case pour couvrir les frais
- compléter le don

Vous pouvez également scanner le code QR avec votre appareil mobile.</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="230"/>
        <source>If you are in the Euro Payments Area, you can transfer money from your bank account.

Click the buttons below to copy the name/IBAN/BIC to the clipboard and

- sign in to your bank account
- paste the name/IBAN/BIC into the transfer form
- enter the amount you would like to send
- complete the donation

You can also scan the QR code with your mobile device.</source>
        <translation>Si vous vous trouvez dans l&apos;espace de paiement en euros, vous pouvez transférer de l&apos;argent à partir de votre compte bancaire.

Cliquez sur les boutons ci-dessous pour copier le nom/IBAN/BIC dans le presse-papiers et

- vous connecter à votre compte bancaire
- coller le nom/IBAN/BIC dans le formulaire de transfert
- saisir le montant que vous souhaitez envoyer
- compléter le don

Vous pouvez également scanner le code QR avec votre appareil mobile.</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="251"/>
        <source>Copy Name</source>
        <translation>Copier le nom</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="268"/>
        <source>Copy IBAN</source>
        <translation>Copier l&apos;IBAN</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="285"/>
        <source>Copy BIC</source>
        <translation>Copier le BIC</translation>
    </message>
</context>
<context>
    <name>DialogHelp</name>
    <message>
        <location filename="../DialogHelp.ui" line="20"/>
        <source>Guide</source>
        <translation>Manuel utilisateur</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="61"/>
        <source>%1 guide not found, showing EN guide instead.</source>
        <translation>Le Manuel Utilisateur %1 n&apos;a pas été trouvé, Regarder pour le moment de guide EN</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="53"/>
        <source>%1 guide not found!</source>
        <translation>%1 guide pas trouvé !</translation>
    </message>
</context>
<context>
    <name>DialogMigration</name>
    <message>
        <location filename="../DialogMigration.ui" line="14"/>
        <source>Data Migration</source>
        <translation>Migration des données</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="24"/>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="72"/>
        <source>Open Data Source</source>
        <translation>Ouvrir la source des données</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="97"/>
        <source>Parameters</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="217"/>
        <source>Date Format</source>
        <translation>Format de la date</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="267"/>
        <source>Irregular Detection</source>
        <translation>Détection d&apos;irrégularités</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="167"/>
        <source>Startline</source>
        <translation>Ligne de départ</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="183"/>
        <source>Elements per Line</source>
        <translation>Éléments par ligne</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="111"/>
        <source>Delimiter</source>
        <translation>Délimiteur</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="303"/>
        <source>Time Format</source>
        <translation>Format de l’heure</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="199"/>
        <source>Movement Detection</source>
        <translation>Détection des mouvements</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="239"/>
        <source>Date Language</source>
        <translation>Langue de la date</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="344"/>
        <source>Positions</source>
        <translation>Positions</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="488"/>
        <source>Irregular</source>
        <translation>Irrégulier</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="504"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="452"/>
        <source>Systolic</source>
        <translation>Systolique</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="410"/>
        <source>Heartrate</source>
        <translation>Fréquence cardiaque</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="394"/>
        <source>Movement</source>
        <translation>Mouvement</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="329"/>
        <source>Ignore Comment</source>
        <translation>Ignorer le commentaire</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="368"/>
        <source>Time</source>
        <translation>Heure</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="436"/>
        <source>Diastolic</source>
        <translation>Diastolique</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="550"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="567"/>
        <source>Result</source>
        <translation>Résultat</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="786"/>
        <source>Migrate User 1</source>
        <translation>Migrer l&apos;Utilisateur 1</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="815"/>
        <source>Migrate User 2</source>
        <translation>Migrer l&apos;Utilisateur 2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="841"/>
        <source>Copy to Custom</source>
        <translation>Copier dans Personnalisé</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="861"/>
        <source>Select predefined settings</source>
        <translation>Sélectionner les réglages prédéfinis</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="895"/>
        <source>Custom</source>
        <translation>Personnalisé</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="906"/>
        <source>Test Data Migration</source>
        <translation>Test de la migration des données</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="920"/>
        <source>Start Data Migration</source>
        <translation>Démarrer la migration des données</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="251"/>
        <source>Choose Data Source for Migration</source>
        <translation>Choisir la source de données pour la migration</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="251"/>
        <source>CSV File (*.csv)</source>
        <translation>Fichier CSV (*.csv)</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="304"/>
        <source>%L1 Bytes</source>
        <translation>%L1 octets</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="305"/>
        <source>%1 Lines</source>
        <translation>%1 lignes</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="319"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>Impossible d&apos;ouvrir «&#xa0;%1&#xa0;»&#xa0;!

Raison&#xa0;: %2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="341"/>
        <source>Startline can&apos;t be empty.</source>
        <translation>La ligne de début ne peut pas être vide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="347"/>
        <source>Startline must be smaller than the number of lines.</source>
        <translation>La ligne de début doit être plus petite que le nombre de lignes.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="355"/>
        <source>Line %1 = %2</source>
        <translation>Ligne %1 = %2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="360"/>
        <source>Delimiter can&apos;t be empty.</source>
        <translation>Le délimiteur ne peut pas être vide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="367"/>
        <source>Startline doesn&apos;t contain &quot;%1&quot; delimiter.</source>
        <translation>La ligne de début ne contient pas le délimiteur «&#xa0;%1&#xa0;».</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="378"/>
        <source>Elements can&apos;t be empty.</source>
        <translation>Les éléments ne peuvent pas être vides.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="384"/>
        <source>Elements can&apos;t be smaller than 4.</source>
        <translation>Les éléments ne peuvent être inférieurs à 4.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="390"/>
        <source>Elements doesn&apos;t match (found %1).</source>
        <translation>Les éléments ne correspondent pas (trouvé %1).</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="397"/>
        <source>Date format can&apos;t be empty.</source>
        <translation>Le format de la date ne peut pas être vide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="403"/>
        <source>Single date format must also contain a time format.</source>
        <translation>Un format de date unique doit également contenir un format d&apos;heure.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="410"/>
        <source>Date position can&apos;t be empty.</source>
        <translation>La position de la date ne peut pas être vide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="416"/>
        <source>Date position must be smaller than elements.</source>
        <translation>La position de la date doit être plus petite que les éléments.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="423"/>
        <source>Time position requires also a time parameter.</source>
        <translation>La position temporelle nécessite également un paramètre temporel.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="429"/>
        <source>Time format requires also a time position.</source>
        <translation>Le format horaire nécessite également une position horaire.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="435"/>
        <source>Time position must be smaller than elements.</source>
        <translation>La position temporelle doit être inférieure aux éléments.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="442"/>
        <source>Systolic position can&apos;t be empty.</source>
        <translation>La position systolique ne peut pas être vide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="448"/>
        <source>Systolic position must be smaller than elements.</source>
        <translation>La position systolique doit être inférieure aux éléments.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="455"/>
        <source>Diastolic position can&apos;t be empty.</source>
        <translation>La position diastolique ne peut pas être vide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="461"/>
        <source>Diastolic position must be smaller than elements.</source>
        <translation>La position diastolique doit être inférieure aux éléments.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="468"/>
        <source>Heartrate position can&apos;t be empty.</source>
        <translation>La position de la fréquence cardiaque ne peut pas être vide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="474"/>
        <source>Heartrate position must be smaller than elements.</source>
        <translation>La position de la fréquence cardiaque doit être inférieure aux éléments.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="481"/>
        <source>Irregular position must be smaller than elements.</source>
        <translation>La position irrégulière doit être plus petite que les éléments.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="487"/>
        <source>Irregular position requires also an irregular parameter.</source>
        <translation>Une position irrégulière nécessite également un paramètre irrégulier.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="494"/>
        <source>Movement position must be smaller than elements.</source>
        <translation>La position du mouvement doit être plus petite que les éléments.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="500"/>
        <source>Movement position requires also a movement parameter.</source>
        <translation>La position du mouvement nécessite également un paramètre de mouvement.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="507"/>
        <source>Comment position must be smaller than elements.</source>
        <translation>La position du commentaire doit être plus petite que celle des éléments.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="523"/>
        <source>Positions can&apos;t exist twice.</source>
        <translation>Les positions ne peuvent pas exister deux fois.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="539"/>
        <source>Date format is invalid.</source>
        <translation>Le format de la date est invalide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="546"/>
        <source>Time format is invalid.</source>
        <translation>Le format de l&apos;heure est invalide.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="556"/>
        <source>Date/Time format is invalid.</source>
        <translation>Le format de la date et de l&apos;heure est invalide.</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="747"/>
        <source>Successfully migrated %n record(s) for user %1.</source>
        <translation>
            <numerusform>La migration de %n enregistrement pour l&apos;utilisateur %1 a réussi.</numerusform>
            <numerusform>La migration de %n enregistrements pour l&apos;utilisateur %1 a réussi.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="751"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>Saut de %n enregistrement invalide&#xa0;!</numerusform>
            <numerusform>Saut de %n enregistrements invalides&#xa0;!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="756"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>Sauter %n enregistrement en double&#xa0;!</numerusform>
            <numerusform>Sauter %n enregistrements en double&#xa0;!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="761"/>
        <source>Skipped %n hidden record(s)!</source>
        <translation>
            <numerusform>%n enregistrement caché ignoré&#x202f;!</numerusform>
            <numerusform>%n enregistrements cachés ignorés&#x202f;!</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogRecord</name>
    <message>
        <location filename="../DialogRecord.ui" line="14"/>
        <source>Manual Record</source>
        <translation>Enregistrement Manuel</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="27"/>
        <source>Data Record</source>
        <translation>Enregistrement de données</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="35"/>
        <location filename="../DialogRecord.ui" line="211"/>
        <location filename="../DialogRecord.cpp" line="9"/>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>Add Record For %1</source>
        <translation>Ajouter un enregistrement pour %1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="78"/>
        <source>Select Date &amp; Time</source>
        <translation>Sélectionner la date et la heure</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="129"/>
        <source>Enter SYS</source>
        <translation>Entrer SYS</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="148"/>
        <source>Enter DIA</source>
        <translation>Entrer DIA</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="167"/>
        <source>Enter BPM</source>
        <translation>Entrer BPM</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="102"/>
        <source>Irregular Heartbeat</source>
        <translation>Rythme cardiaque irrégulier</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="186"/>
        <source>Movement</source>
        <translation>Mouvement</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="239"/>
        <source>Enter Optional Comment</source>
        <translation>Entrez un commentaire facultatif</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="269"/>
        <source>Show message on successful create / delete / modify record</source>
        <translation>Afficher un message pour une création / suppression / modification d&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="287"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="304"/>
        <source>Create</source>
        <translation>Créer</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="324"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="9"/>
        <source>User 1</source>
        <translation>Utilisateur 1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>User 2</source>
        <translation>Utilisateur 2</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="34"/>
        <location filename="../DialogRecord.cpp" line="187"/>
        <source>Modify</source>
        <translation>Modifier</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="132"/>
        <source>The data record could not be deleted!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>L&apos;Enregistrement n&apos;a pas pu être supprimé

Aucune Entrée pour cette date et heure n&apos;existe</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="138"/>
        <source>Data record successfully deleted.</source>
        <translation>L&apos;enregistrement a été effacé avec succès</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="150"/>
        <source>Please enter a valid value for &quot;SYS&quot; first!</source>
        <translation>Veuillez d&apos;abord entrer une valeur valide pour &quot;SYS&quot; !</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="160"/>
        <source>Please enter a valid value for &quot;DIA&quot; first!</source>
        <translation>Veuillez d&apos;abord entrer une valeur valide pour &quot;DIA&quot; !</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="169"/>
        <source>Please enter a valid value for &quot;BPM&quot; first!</source>
        <translation>Veuillez d&apos;abord entrer une valeur valide pour &quot;BPM&quot; !</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="191"/>
        <source>The data record could not be modified!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>L&apos;Enregistrement n&apos;a pas pu être modifié.

Aucune entrée pour cette date et cette heure existe.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="197"/>
        <source>Data Record successfully modified.</source>
        <translation>Les Données de l&apos;Enregistrement ont été modifiées avec Succès.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="206"/>
        <source>The data record could not be created!

An entry for this date &amp; time already exist.</source>
        <translation>Les Données de l&apos;Enregistrement n&apos;ont pas pu être crées !

Une entrée avec la Date et l&apos;Heure existe déjà.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="212"/>
        <source>Data Record successfully created.</source>
        <translation>L&apos;Enregistrement de la Données est crée avec Succès.</translation>
    </message>
</context>
<context>
    <name>DialogSettings</name>
    <message>
        <location filename="../DialogSettings.cpp" line="183"/>
        <source>Choose Database Location</source>
        <translation>Choisir l&apos;Emplacement de la Base De Données</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="193"/>
        <source>Choose Database Backup Location</source>
        <translation>Choisir l&apos;emplacement de la sauvegarde de la base de données</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="380"/>
        <source>The selected database already exists.

Please choose the preferred action.</source>
        <translation>La base de données sélectionnée existe déjà.

Veuillez choisir l&apos;action préférée.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="382"/>
        <source>Overwrite</source>
        <translation>Écraser</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="383"/>
        <source>Merge</source>
        <translation>Fusionner</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="384"/>
        <source>Swap</source>
        <translation>Permuter</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="386"/>
        <source>Overwrite:

Overwrites the selected database with the current database.

Merge:

Merges the selected database into the current database.

Swap:

Deletes the current database and use the selected database.</source>
        <translation>Écraser :

Écrase la base de données sélectionnée avec la base de données actuelle.

Fusionner :

Fusionne la base de données sélectionnée avec la base de données actuelle.

Permuter :

Supprime la base de données actuelle et utilise la base de données sélectionnée.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="394"/>
        <source>The current database is empty.

The selected database will be erased on exit, if no data will be added.</source>
        <translation>La base de données actuelle est vide.

La base de données sélectionnée sera effacée à la sortie, si aucune donnée n&apos;est ajoutée.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="425"/>
        <source>SQL encryption can&apos;t be enabled without password and will be disabled!</source>
        <translation>Le cryptage SQL ne peut être activé sans mot de passe et sera désactivé !</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="432"/>
        <source>The database backup should be located on a different hard disk, partition or directory.</source>
        <translation>La sauvegarde de la base de données doit être située sur un disque dur, une partition ou un répertoire différent.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="442"/>
        <source>Please enter valid values for additional information of user 1!</source>
        <translation>Veuillez entrer des valeurs valides pour les informations supplémentaires de l&apos;utilisateur 1 !</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="449"/>
        <source>Please enter valid values for additional information of user 2!</source>
        <translation>Veuillez entrer des valeurs valides pour les informations supplémentaires de l&apos;utilisateur 2 !</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="456"/>
        <source>Entered age doesn&apos;t match selected age group for user 1!</source>
        <translation>L&apos;age saisi ne correspond pas à la tranche d&apos;age sélectionné !</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="463"/>
        <source>Entered age doesn&apos;t match selected age group for user 2!</source>
        <translation>L&apos;age saisi ne correspond pas à la tranche d&apos;age sélectionné pour l&apos;utilisateur 2 !</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="471"/>
        <source>Please enable symbols or lines for chart!</source>
        <translation>Veuillez activer les symboles ou les lignes pour les graphiques !</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="478"/>
        <source>Please enter a valid e-mail address!</source>
        <translation>Merci d&apos;entrer un adresse électronique valide&#xa0;!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="485"/>
        <source>Please enter a e-mail subject!</source>
        <translation>Merci d&apos;entrer un objet à votre courriel&#xa0;!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="492"/>
        <source>E-Mail message must contain $CHART, $TABLE and/or $STATS!</source>
        <translation>un message doit contenir $CHART, $TABLE et/ou $STATS !</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="591"/>
        <source>The auto hide setting has been changed.

Should this now be applied to all existing measurements?</source>
        <translation>Le réglage de cache automatique a été changé.

Devrait-on maintenant appliquer à toutes les mesures existantes&#x202f;?</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="596"/>
        <source>Auto hide measurements:

     User 1 : %1 %3
     User 2 : %2 %3</source>
        <translation>Masquer automatiquement les mesures:

     Utilisateur 1&#xa0;: %1 %3
     Utilisateur 2&#xa0;: %2 %3</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="596"/>
        <source>hidden</source>
        <translation>masqué</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="596"/>
        <source>shown</source>
        <translation>affiché</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="647"/>
        <source>User 1</source>
        <translation>Utilisateur 1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="661"/>
        <source>User 2</source>
        <translation>Utilisateur 2</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="785"/>
        <source>Blood Pressure Report</source>
        <translation>Rapport sur la tension artérielle</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="786"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Cher Dr NomduPraticien,

veuillez trouver ci-joint les données de ma tension artérielle pour ce mois-ci.

Cordialement,
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="838"/>
        <source>Abort setup and discard all changes?</source>
        <translation>Abandonner la configuration et abandonner toutes les modifications ?</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="20"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="44"/>
        <source>Database</source>
        <translation>Base de Données</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="75"/>
        <source>Location</source>
        <translation>Emplacement</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="81"/>
        <source>Current Location</source>
        <translation>Emplacement Courant</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="97"/>
        <source>Change Location</source>
        <translation>Changer Emplacement</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="123"/>
        <source>Encrypt With SQLCipher</source>
        <translation>Crypter avec SQLCipher</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="126"/>
        <source>Encryption</source>
        <translation>Crypter</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="138"/>
        <source>Password</source>
        <translation>Mot de Passe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="151"/>
        <source>Show Password</source>
        <translation>Afficher Mot de Passe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="454"/>
        <source>User</source>
        <translation>Utilisateur</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="489"/>
        <location filename="../DialogSettings.ui" line="752"/>
        <source>Male</source>
        <translation>Homme</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="518"/>
        <location filename="../DialogSettings.ui" line="781"/>
        <source>Female</source>
        <translation>Femme</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="614"/>
        <location filename="../DialogSettings.ui" line="886"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="483"/>
        <location filename="../DialogSettings.ui" line="746"/>
        <source>Mandatory Information</source>
        <translation>Informations Obligatoires</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="544"/>
        <location filename="../DialogSettings.ui" line="816"/>
        <source>Age Group</source>
        <translation>Tranche d&apos;Age</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="630"/>
        <location filename="../DialogSettings.ui" line="902"/>
        <source>Additional Information</source>
        <translation>Informations Complémentaires</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="690"/>
        <location filename="../DialogSettings.ui" line="962"/>
        <source>Height</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="719"/>
        <location filename="../DialogSettings.ui" line="991"/>
        <source>Weight</source>
        <translation>Poids</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1016"/>
        <source>Device</source>
        <translation>Appareil</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1022"/>
        <location filename="../DialogSettings.cpp" line="77"/>
        <source>Import Plugins [ %1 ]</source>
        <translation>Importer des greffons [ %1 ]</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1070"/>
        <source>Open Website</source>
        <translation>Aller au site Web</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1083"/>
        <source>Maintainer</source>
        <translation>Mainteneur</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1111"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1118"/>
        <source>Send E-Mail</source>
        <translation>Envoyer un courriel</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1131"/>
        <source>Model</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1138"/>
        <source>Producer</source>
        <translation>Fabricant</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1155"/>
        <source>Chart</source>
        <translation>Graphique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1282"/>
        <source>X-Axis Range</source>
        <translation>Plage de l&apos;axe X</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1288"/>
        <source>Dynamic Scaling</source>
        <translation>Échelle dynamique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1269"/>
        <source>Healthy Ranges</source>
        <translation>Plages saines</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1186"/>
        <source>Symbols</source>
        <translation>Symboles</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1263"/>
        <source>Colored Areas</source>
        <translation>Zones Colorées</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1035"/>
        <source>Please choose Device Plugin…</source>
        <translation>Veuillez choisir le greffon pour l&apos;appareil…</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="338"/>
        <location filename="../DialogSettings.ui" line="1410"/>
        <location filename="../DialogSettings.ui" line="1685"/>
        <source>Systolic</source>
        <translation>Systolique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="347"/>
        <location filename="../DialogSettings.ui" line="1498"/>
        <location filename="../DialogSettings.ui" line="1773"/>
        <source>Diastolic</source>
        <translation>Diastolique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="356"/>
        <location filename="../DialogSettings.ui" line="1586"/>
        <location filename="../DialogSettings.ui" line="1861"/>
        <source>Heartrate</source>
        <translation>Fréquence Cardiaque</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1958"/>
        <source>Table</source>
        <translation>Tableau</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2043"/>
        <location filename="../DialogSettings.ui" line="2226"/>
        <source>Systolic Warnlevel</source>
        <translation>Niveau d&apos;Alerte Systolique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1989"/>
        <location filename="../DialogSettings.ui" line="2286"/>
        <source>Diastolic Warnlevel</source>
        <translation>Niveau d&apos;Alerte Diastolique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="176"/>
        <source>Automatic Backup</source>
        <translation>Sauvegarde automatique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="179"/>
        <source>Backup</source>
        <translation>Sauvegarde</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="196"/>
        <source>Current Backup Location</source>
        <translation>Emplacement actuel de la sauvegarde</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="209"/>
        <source>Change Backup Location</source>
        <translation>Modifier l&apos;emplacement de la sauvegarde</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="236"/>
        <source>Backup Mode</source>
        <translation>Mode de sauvegarde</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="246"/>
        <source>Daily</source>
        <translation>Quotidien</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="255"/>
        <source>Weekly</source>
        <translation>Hebdomadaire</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="264"/>
        <source>Monthly</source>
        <translation>Mensuel</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="276"/>
        <source>Keep Copies</source>
        <translation>Conserver des copies</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="311"/>
        <source>Auto Hide Measurements</source>
        <translation>Masquer les mesures automatiquement</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="322"/>
        <source>Detection</source>
        <translation>Détection</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="329"/>
        <source>Sum of All</source>
        <translation>Somme de tout</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="371"/>
        <source>Hide</source>
        <translation>Masquer</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="378"/>
        <source>Higher</source>
        <translation>Plus élevé</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="387"/>
        <source>Lower</source>
        <translation>Plus bas</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="404"/>
        <source>Time Span</source>
        <translation>Période de temps</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="658"/>
        <location filename="../DialogSettings.ui" line="930"/>
        <source>Birthday</source>
        <translation>Anniversaire</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1195"/>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1219"/>
        <location filename="../DialogSettings.ui" line="1247"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1301"/>
        <source>Lines</source>
        <translation>Lignes</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1310"/>
        <location filename="../DialogSettings.ui" line="1338"/>
        <source>Width</source>
        <translation>Largeur</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1367"/>
        <source>Show Heartrate</source>
        <translation>Afficher la Fréquence Cardiaque</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1373"/>
        <source>Show Heartrate in Chart View</source>
        <translation>Afficher La Fréquence Cardiaque en Grahpique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1383"/>
        <source>Print Heartrate</source>
        <translation>Imprimer Fréquence Cardiaque</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1389"/>
        <source>Print Heartrate on separate Sheet</source>
        <translation>Imprimer la Fréquence Cardiaque sur une feuille séparée</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2103"/>
        <location filename="../DialogSettings.ui" line="2340"/>
        <source>P.Pressure Warnlevel</source>
        <translation>Avertissement de la Pression Cardiaque</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2157"/>
        <location filename="../DialogSettings.ui" line="2394"/>
        <source>Heartrate Warnlevel</source>
        <translation>Avertissement de la Fréquence Cardiaque</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2459"/>
        <source>Statistic</source>
        <translation>Statistique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2465"/>
        <source>Bar Type</source>
        <translation>Type de Barre</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2471"/>
        <source>Show Median instead of Average Bars</source>
        <translation>Afficher les Barres Médianes au lieu des Barres Moyennes</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2481"/>
        <source>Legend Type</source>
        <translation>Type de Légende</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2487"/>
        <source>Show Values as Legend instead of Descriptions</source>
        <translation>Afficher les Valeurs en tant que Légende à la place des Descriptions</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2502"/>
        <source>Print</source>
        <translation>Imprimer</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2510"/>
        <source>Additional Colums</source>
        <translation>Colonnes supplémentaires</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2516"/>
        <source>Pulse Pressure</source>
        <translation>Pression artérielle</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2539"/>
        <source>Irregular Heartbeat</source>
        <translation>Rythme cardiaque irrégulier</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2562"/>
        <source>Movement</source>
        <translation>Mouvement</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2588"/>
        <source>Page Orientation</source>
        <translation>Orientation de la page</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2594"/>
        <source>Portrait</source>
        <translation>Portrait</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2604"/>
        <source>Landscape</source>
        <translation>Paysage</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2616"/>
        <source>Header Appearance</source>
        <translation>Apparence de l&apos;en-tête</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2622"/>
        <source>Show Column Headers as Graphics</source>
        <translation>Afficher les en-têtes de colonne sous forme de graphiques</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2640"/>
        <source>E-Mail</source>
        <translation>Courriel</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2648"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2667"/>
        <source>Subject</source>
        <translation>Sujet</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2685"/>
        <source>Message</source>
        <translation>Message</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2706"/>
        <source>Plugin</source>
        <translation>Greffon</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2712"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2718"/>
        <source>Log Device Communication to File</source>
        <translation>Enregistrer la communication du dispositif dans un fichier</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2725"/>
        <source>Import Measurements automatically</source>
        <translation>Importer automatiquement les mesures</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2735"/>
        <source>Bluetooth</source>
        <translation>Bluetooth</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2741"/>
        <source>Discover Device automatically</source>
        <translation>Découvrir le dispositif automatiquement</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2748"/>
        <source>Connect Device automatically</source>
        <translation>Connecter le dispositif automatiquement</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2763"/>
        <source>Update</source>
        <translation>Mise à Jour</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2769"/>
        <source>Autostart</source>
        <translation>Démmarage Automatique</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2775"/>
        <source>Check for Online Updates at Program Startup</source>
        <translation>Vérifier si une Mise à Jour au Démarrage du Programme</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2788"/>
        <source>Notification</source>
        <translation>Notification</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2794"/>
        <source>Always show Result after Online Update Check</source>
        <translation>Toujours montrer le Résultat après une vérification de Mise à Jour</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2809"/>
        <source>Format</source>
        <translation>Format</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2817"/>
        <source>Date / Short</source>
        <translation>Date / Courte</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2843"/>
        <source>Date / Long</source>
        <translation>Date / Longue</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2873"/>
        <source>Time / Short</source>
        <translation>Heure / Courte</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2899"/>
        <source>Time / Long</source>
        <translation>Heure / Longue</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2933"/>
        <source>Save</source>
        <translation>Sauvegarder</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2950"/>
        <source>Reset</source>
        <translation>Réinitialiser</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2967"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>DialogUpdate</name>
    <message>
        <location filename="../DialogUpdate.ui" line="14"/>
        <source>Online Update</source>
        <translation>Mise à jour en Ligne</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="27"/>
        <source>Info</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="80"/>
        <source>Available Version</source>
        <translation>Version Disponible</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="90"/>
        <source>Update File Size</source>
        <translation>Taille du Fichier de Mise à Jour</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="104"/>
        <source>Installed Version</source>
        <translation>Version Installée</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="156"/>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="181"/>
        <location filename="../DialogUpdate.ui" line="202"/>
        <source>Download</source>
        <translation>Télécharger</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="222"/>
        <source>Ignore</source>
        <translation>Ignorer</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="183"/>
        <source>No new version found.</source>
        <translation>Pas de nouvelle version trouvée.</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogUpdate.cpp" line="42"/>
        <source>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problem(s):

%1
Do you wish to continue anyway?</source>
        <translation>
            <numerusform>!&#x202f;!&#x202f;! AVERTISSEMENT SSL - LIRE ATTENTIVEMENT&#x202f;!&#x202f;!&#x202f;!

Problème de connexion réseau&#xa0;:

%1
Voulez-vous quand même continuer&#x202f;?</numerusform>
            <numerusform>!&#x202f;!&#x202f;! AVERTISSEMENT SSL - LIRE ATTENTIVEMENT&#x202f;!&#x202f;!&#x202f;!

Problèmes de connexion réseau&#xa0;:

%1
Voulez-vous quand même continuer&#x202f;?</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="73"/>
        <source>Downloading update failed!

%1</source>
        <translation>Le Téléchargement de la Mise à Jour a échoué !

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="73"/>
        <source>Checking update failed!

%1</source>
        <translation>La Vérification de la Mise à Jour a échouée !

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="156"/>
        <source>Unexpected response from update server!</source>
        <translation>Réponse inattendu du serveur de mise à jour !</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="173"/>
        <source>*%1 not found</source>
        <translation>*%1 pas trouvé</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="203"/>
        <source>Update doesn&apos;t have expected size!

%L1 : %L2

Retry download…</source>
        <translation>La mise à jour n&apos;a pas la taille attendue !

%L1 : %L2

Réessayer de Télécharger …</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="207"/>
        <source>Update saved to %1.

Start new version now?</source>
        <translation>La mise à jour a été enregistrée dans %1.

Lancer la nouvelle version maintenant ?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="220"/>
        <source>Could not start new version!</source>
        <translation>Impossible de lancer la nouvelle version !</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="247"/>
        <source>The program was installed by Flatpak.

Please update using the Flatpak internal update function.</source>
        <translation>Le programme a été installé par Flatpak.

Veuillez mettre à jour en utilisant la fonction de mise à jour interne Flatpak.</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="253"/>
        <source>The program was installed by Snap.

Please update using the Snap internal update function.</source>
        <translation>Le programme a été installé par Snap.

Veuillez mettre à jour en utilisant la fonction de mise à jour interne Snap.</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="259"/>
        <source>The program was installed from Windows store.

Please update using the store internal update function.</source>
        <translation>Le programme a été installé depuis le Windows Store.

Veuillez le mettre à jour en utilisant la fonction de mise à jour interne au magasin.</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="265"/>
        <source>The program was installed from distribution.

Please update using the operating systems internal update function.</source>
        <translation>Le programme a été installé à partir de la distribution.

Veuillez mettre à jour en utilisant la fonction de mise à jour interne des systèmes d&apos;exploitation.</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="271"/>
        <source>The program was installed from source code.

Please update the sources, rebuild and reinstall manually.</source>
        <translation>Le programme a été installé à partir du code source.

Veuillez mettre à jour les sources, reconstruire et réinstaller manuellement.</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="297"/>
        <source>Really abort download?</source>
        <translation>Interrompre vraiment le téléchargement ?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="227"/>
        <source>Could not save update to %1!

%2</source>
        <translation>Impossible d&apos;enregistrer la mise à jour dans %1 !

%2</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="14"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Gestionnaire Universel de la Pression Artérielle</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="30"/>
        <source>Show Previous Period</source>
        <translation>Montrer Période Précédente</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="104"/>
        <source>Show Next Period</source>
        <translation>Montrer Période Suivante</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="143"/>
        <source>Chart View</source>
        <translation>Montrer Graphique</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="171"/>
        <source>Table View</source>
        <translation>Montrer Tableau</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="211"/>
        <location filename="../MainWindow.cpp" line="2608"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="220"/>
        <location filename="../MainWindow.cpp" line="2609"/>
        <source>Time</source>
        <translation>Heure</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="229"/>
        <location filename="../MainWindow.cpp" line="159"/>
        <location filename="../MainWindow.cpp" line="5049"/>
        <source>Systolic</source>
        <translation>Systolique</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="238"/>
        <location filename="../MainWindow.cpp" line="160"/>
        <location filename="../MainWindow.cpp" line="5050"/>
        <source>Diastolic</source>
        <translation>Diastolique</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="247"/>
        <source>P.Pressure</source>
        <translation>Pression Pulsée</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="265"/>
        <source>Irregular</source>
        <translation>Irrégulier</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="274"/>
        <source>Movement</source>
        <translation>Mouvement</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="283"/>
        <source>Invisible</source>
        <translation>Invisible</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="292"/>
        <location filename="../MainWindow.cpp" line="2628"/>
        <location filename="../MainWindow.cpp" line="2641"/>
        <location filename="../MainWindow.cpp" line="2655"/>
        <location filename="../MainWindow.cpp" line="2669"/>
        <location filename="../MainWindow.cpp" line="2683"/>
        <location filename="../MainWindow.cpp" line="2698"/>
        <location filename="../MainWindow.cpp" line="2714"/>
        <location filename="../MainWindow.cpp" line="2728"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="309"/>
        <source>Statistic View</source>
        <translation>Vue Statistique</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="404"/>
        <source>¼ Hourly</source>
        <translation>¼ Heure</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="439"/>
        <source>½ Hourly</source>
        <translation>½ Heure</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="471"/>
        <source>Hourly</source>
        <translation>Heure</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="506"/>
        <source>¼ Daily</source>
        <translation>6 Heures</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="538"/>
        <source>½ Daily</source>
        <translation>12 Heures</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="570"/>
        <source>Daily</source>
        <translation>Jour</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="605"/>
        <source>3 Days</source>
        <translation>3 jours</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="637"/>
        <source>Weekly</source>
        <translation>Hebdomadaire</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="669"/>
        <source>Monthly</source>
        <translation>Mensuel</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="701"/>
        <source>Quarterly</source>
        <translation>Trimestrielle</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="733"/>
        <source>½ Yearly</source>
        <translation>Semestrielle</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="765"/>
        <source>Yearly</source>
        <translation>Annuelle</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="825"/>
        <source>Last 3 Days</source>
        <translation>Les 3 derniers jours</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="857"/>
        <source>Last 7 Days</source>
        <translation>Les 7 Derniers Jours</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="889"/>
        <source>Last 14 Days</source>
        <translation>Les 14 Derniers Jours</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="921"/>
        <source>Last 21 Days</source>
        <translation>Les 21 Derniers Jours</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="953"/>
        <source>Last 28 Days</source>
        <translation>Les 28 Derniers Jours</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="985"/>
        <source>Last 3 Months</source>
        <translation>Les 3 Derniers Mois</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1017"/>
        <source>Last 6 Months</source>
        <translation>Les 6 Derniers Mois</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1049"/>
        <source>Last 9 Months</source>
        <translation>Les 9 Derniers Mois</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1081"/>
        <source>Last 12 Months</source>
        <translation>Les 12 Derniers Mois</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1113"/>
        <source>All Records</source>
        <translation>Tous les Enregistrements</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1160"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1164"/>
        <source>Print</source>
        <translation>Imprimer</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1186"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1203"/>
        <source>Configuration</source>
        <translation>Configuration</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1207"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1216"/>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1225"/>
        <source>Style</source>
        <translation>Style</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1234"/>
        <source>Charts</source>
        <translation>Graphiques</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1259"/>
        <source>Database</source>
        <translation>Basse de Donnée</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1263"/>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1275"/>
        <source>Export</source>
        <translation>Exporter</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1290"/>
        <source>Clear</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1379"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1382"/>
        <location filename="../MainWindow.ui" line="1385"/>
        <source>Quit Program</source>
        <translation>Quitter le programme</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1397"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1400"/>
        <location filename="../MainWindow.ui" line="1403"/>
        <source>About Program</source>
        <translation>À propos du programme</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1412"/>
        <source>Guide</source>
        <translation>Guide</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1415"/>
        <location filename="../MainWindow.ui" line="1418"/>
        <source>Show Guide</source>
        <translation>Afficher le guide</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1427"/>
        <source>Update</source>
        <translation>Mise à jour</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1430"/>
        <location filename="../MainWindow.ui" line="1433"/>
        <source>Check Update</source>
        <translation>Vérifier les mises à jour</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1592"/>
        <source>To PDF</source>
        <translation>En PDF</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1595"/>
        <location filename="../MainWindow.ui" line="1598"/>
        <source>Export To PDF</source>
        <translation>Exporter comme PDF</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1607"/>
        <source>Migration</source>
        <translation>Migration</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1610"/>
        <location filename="../MainWindow.ui" line="1613"/>
        <source>Migrate from Vendor</source>
        <translation>Migrer d&apos;un fournisseur</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1857"/>
        <source>Translation</source>
        <translation>Traduction</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1860"/>
        <location filename="../MainWindow.ui" line="1863"/>
        <source>Contribute Translation</source>
        <translation>Contribuer à la traduction</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1872"/>
        <source>E-Mail</source>
        <translation>Courriel</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1875"/>
        <location filename="../MainWindow.ui" line="1878"/>
        <source>Send E-Mail</source>
        <translation>Envoyer le courriel</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1890"/>
        <source>Icons</source>
        <translation>Icônes</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1893"/>
        <location filename="../MainWindow.ui" line="1896"/>
        <source>Change Icon Color</source>
        <translation>Changer la couleur de l&apos;icône</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1907"/>
        <source>System</source>
        <translation>Système</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1910"/>
        <location filename="../MainWindow.ui" line="1913"/>
        <source>System Colors</source>
        <translation>Couleurs du système</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1921"/>
        <source>Light</source>
        <translation>Clair</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1924"/>
        <location filename="../MainWindow.ui" line="1927"/>
        <source>Light Colors</source>
        <translation>Couleurs claires</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1935"/>
        <source>Dark</source>
        <translation>Sombre</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1938"/>
        <location filename="../MainWindow.ui" line="1941"/>
        <source>Dark Colors</source>
        <translation>Couleurs sombres</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1950"/>
        <source>Donation</source>
        <translation>Faire un don</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1965"/>
        <location filename="../MainWindow.cpp" line="3840"/>
        <source>Distribution</source>
        <translation>Distribution</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1968"/>
        <location filename="../MainWindow.ui" line="1971"/>
        <source>Show Distribution</source>
        <translation>Afficher la distribution</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1983"/>
        <location filename="../MainWindow.ui" line="1986"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1995"/>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1998"/>
        <location filename="../MainWindow.ui" line="2001"/>
        <source>Read Wiki</source>
        <translation>Lire le wiki</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="256"/>
        <location filename="../MainWindow.cpp" line="161"/>
        <location filename="../MainWindow.cpp" line="5051"/>
        <source>Heartrate</source>
        <translation>Fréquence cardiaque</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1953"/>
        <location filename="../MainWindow.ui" line="1956"/>
        <source>Make Donation</source>
        <translation>Faire un don</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1445"/>
        <source>Bugreport</source>
        <translation>Rapport d&apos;erreur</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1448"/>
        <location filename="../MainWindow.ui" line="1451"/>
        <source>Send Bugreport</source>
        <translation>Envoyer un rapport d&apos;erreur</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1460"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1463"/>
        <location filename="../MainWindow.ui" line="1466"/>
        <source>Change Settings</source>
        <translation>Charger paramètres</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1481"/>
        <source>From Device</source>
        <translation>Depuis l&apos;appareil</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1484"/>
        <location filename="../MainWindow.ui" line="1487"/>
        <source>Import From Device</source>
        <translation>Importer depuis l&apos;appareil</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1499"/>
        <source>From File</source>
        <translation>Depuis un fichier</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1502"/>
        <location filename="../MainWindow.ui" line="1505"/>
        <source>Import From File</source>
        <translation>Importer depuis un fichier</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1514"/>
        <source>From Input</source>
        <translation>Depuis l&apos;entrée</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1517"/>
        <location filename="../MainWindow.ui" line="1520"/>
        <source>Import From Input</source>
        <translation>Importer depuis l&apos;entrée</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1529"/>
        <source>To CSV</source>
        <translation>En CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1532"/>
        <location filename="../MainWindow.ui" line="1535"/>
        <source>Export To CSV</source>
        <translation>Exporter en CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1547"/>
        <source>To XML</source>
        <translation>En XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1550"/>
        <location filename="../MainWindow.ui" line="1553"/>
        <source>Export To XML</source>
        <translation>Exporter en XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1562"/>
        <source>To JSON</source>
        <translation>En JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1565"/>
        <location filename="../MainWindow.ui" line="1568"/>
        <source>Export To JSON</source>
        <translation>Exporter en JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1577"/>
        <source>To SQL</source>
        <translation>En SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1580"/>
        <location filename="../MainWindow.ui" line="1583"/>
        <source>Export To SQL</source>
        <translation>Exporter en SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1625"/>
        <source>Print Chart</source>
        <translation>Imprimer le gaphique</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1628"/>
        <location filename="../MainWindow.ui" line="1631"/>
        <source>Print Chart View</source>
        <translation>Aperçu de l&apos;impression graphique</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1640"/>
        <source>Print Table</source>
        <translation>Imprimer le tableau</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1643"/>
        <location filename="../MainWindow.ui" line="1646"/>
        <source>Print Table View</source>
        <translation>Aperçu de l&apos;impression tableau</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1655"/>
        <source>Print Statistic</source>
        <translation>Imprimer les statistiques</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1658"/>
        <location filename="../MainWindow.ui" line="1661"/>
        <source>Print Statistic View</source>
        <translation>Aperçu de l&apos;impression statistiques</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1670"/>
        <source>Preview Chart</source>
        <translation>Aperçu du graphique</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1673"/>
        <location filename="../MainWindow.ui" line="1676"/>
        <source>Preview Chart View</source>
        <translation>Aperçu de la vue graphique</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1685"/>
        <source>Preview Table</source>
        <translation>Aperçu tableau</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1688"/>
        <location filename="../MainWindow.ui" line="1691"/>
        <source>Preview Table View</source>
        <translation>Aperçu de la vue tableau</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1700"/>
        <source>Preview Statistic</source>
        <translation>Aperçu des statistiques</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1703"/>
        <location filename="../MainWindow.ui" line="1706"/>
        <source>Preview Statistic View</source>
        <translation>Aperçu de la vue statistiques</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1715"/>
        <location filename="../MainWindow.ui" line="1718"/>
        <location filename="../MainWindow.ui" line="1721"/>
        <source>Clear All</source>
        <translation>Effacer tout</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1733"/>
        <location filename="../MainWindow.ui" line="1736"/>
        <location filename="../MainWindow.ui" line="1739"/>
        <source>Clear User 1</source>
        <translation>Effacer Utilisateur 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1748"/>
        <location filename="../MainWindow.ui" line="1751"/>
        <location filename="../MainWindow.ui" line="1754"/>
        <source>Clear User 2</source>
        <translation>Effacer Utilisateur 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1814"/>
        <source>Analysis</source>
        <translation>Analyser</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1817"/>
        <location filename="../MainWindow.ui" line="1820"/>
        <source>Analyze Records</source>
        <translation>Analyser les enregistrements</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1839"/>
        <location filename="../MainWindow.ui" line="1842"/>
        <location filename="../MainWindow.ui" line="1845"/>
        <source>Time Mode</source>
        <translation>Mode Heure</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="108"/>
        <location filename="../MainWindow.cpp" line="109"/>
        <location filename="../MainWindow.cpp" line="5019"/>
        <location filename="../MainWindow.cpp" line="5020"/>
        <source>Records For Selected User</source>
        <translation>Enregistrements pour l&apos;utilisateur sélectionné</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="116"/>
        <location filename="../MainWindow.cpp" line="117"/>
        <location filename="../MainWindow.cpp" line="5022"/>
        <location filename="../MainWindow.cpp" line="5023"/>
        <source>Select Date &amp; Time</source>
        <translation>Sélectionner la date et l&apos;heure</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="169"/>
        <location filename="../MainWindow.cpp" line="5053"/>
        <source>Systolic - Value Range</source>
        <translation>Systolique - Plage de valeurs</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="170"/>
        <location filename="../MainWindow.cpp" line="5054"/>
        <source>Diastolic - Value Range</source>
        <translation>Diastolique - Plage de valeurs</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="173"/>
        <location filename="../MainWindow.cpp" line="5056"/>
        <source>Systolic - Target Area</source>
        <translation>Systolique - Zone cible</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="174"/>
        <location filename="../MainWindow.cpp" line="5057"/>
        <source>Diastolic - Target Area</source>
        <translation>Diastolique - Zone cible</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1711"/>
        <source>Athlete</source>
        <translation>Athlète</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1712"/>
        <source>Excellent</source>
        <translation>Excellente</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1712"/>
        <source>Optimal</source>
        <translation>Optimale</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1713"/>
        <source>Great</source>
        <translation>Superbe</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1714"/>
        <source>Good</source>
        <translation>Bonne</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1713"/>
        <source>Normal</source>
        <translation>Normale</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1775"/>
        <location filename="../MainWindow.ui" line="1778"/>
        <location filename="../MainWindow.ui" line="1799"/>
        <location filename="../MainWindow.ui" line="1802"/>
        <location filename="../MainWindow.cpp" line="102"/>
        <location filename="../MainWindow.cpp" line="103"/>
        <location filename="../MainWindow.cpp" line="104"/>
        <location filename="../MainWindow.cpp" line="105"/>
        <location filename="../MainWindow.cpp" line="4737"/>
        <location filename="../MainWindow.cpp" line="4738"/>
        <location filename="../MainWindow.cpp" line="4739"/>
        <location filename="../MainWindow.cpp" line="4740"/>
        <location filename="../MainWindow.cpp" line="5083"/>
        <location filename="../MainWindow.cpp" line="5084"/>
        <location filename="../MainWindow.cpp" line="5085"/>
        <location filename="../MainWindow.cpp" line="5086"/>
        <source>Switch To %1</source>
        <translation>Basculer vers %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="102"/>
        <location filename="../MainWindow.cpp" line="103"/>
        <location filename="../MainWindow.cpp" line="610"/>
        <location filename="../MainWindow.cpp" line="4737"/>
        <location filename="../MainWindow.cpp" line="4738"/>
        <location filename="../MainWindow.cpp" line="5083"/>
        <location filename="../MainWindow.cpp" line="5084"/>
        <source>User 1</source>
        <translation>Utilisateur 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="104"/>
        <location filename="../MainWindow.cpp" line="105"/>
        <location filename="../MainWindow.cpp" line="620"/>
        <location filename="../MainWindow.cpp" line="4739"/>
        <location filename="../MainWindow.cpp" line="4740"/>
        <location filename="../MainWindow.cpp" line="5085"/>
        <location filename="../MainWindow.cpp" line="5086"/>
        <source>User 2</source>
        <translation>Utilisateur 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="171"/>
        <location filename="../MainWindow.cpp" line="5055"/>
        <source>Heartrate - Value Range</source>
        <translation>Fréquence cardiaque - Plage de valeurs</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="175"/>
        <location filename="../MainWindow.cpp" line="5058"/>
        <source>Heartrate - Target Area</source>
        <translation>Fréquence cardiaque - Zone cible</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="341"/>
        <source>No active device plugin, auto import aborted.</source>
        <translation>Pas d&apos;extension de périphérique actif, l&apos;importation automatique annulée.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="358"/>
        <source>Some plugins will not work without Bluetooth permission.</source>
        <translation>Certaines extensions ne fonctionneront pas sans autorisation Bluetooth.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="461"/>
        <source>Could not create backup directory.

Please check backup location setting.</source>
        <translation>Impossible de créer le répertoire de sauvegarde.

Veuillez vérifier les paramètres de l&apos;emplacement de sauvegarde.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="490"/>
        <location filename="../MainWindow.cpp" line="513"/>
        <source>Could not backup database:

%1</source>
        <translation>Impossible de sauvegarder la base de données&#xa0;:

%1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="503"/>
        <source>Could not delete outdated backup:

%1</source>
        <translation>Impossible de supprimer une sauvegarde périmée&#xa0;:

%1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="530"/>
        <source>Could not register icons.</source>
        <translation>Impossible d&apos;enregistrer les icônes.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="681"/>
        <source>Blood Pressure Report</source>
        <translation>Rapport pression artérielle</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1223"/>
        <source>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</source>
        <translation>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1228"/>
        <source>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</source>
        <translation>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1711"/>
        <source>Low</source>
        <translation>Basse</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1714"/>
        <source>High Normal</source>
        <translation>Haute normale</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1715"/>
        <location filename="../MainWindow.cpp" line="2192"/>
        <location filename="../MainWindow.cpp" line="5062"/>
        <location filename="../MainWindow.cpp" line="5066"/>
        <location filename="../MainWindow.cpp" line="5070"/>
        <location filename="../MainWindow.h" line="195"/>
        <location filename="../MainWindow.h" line="199"/>
        <location filename="../MainWindow.h" line="203"/>
        <source>Average</source>
        <translation>Moyenne</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1715"/>
        <source>Hyper 1</source>
        <translation>Hyper 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1716"/>
        <source>Below Average</source>
        <translation>Sous la moyenne</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1716"/>
        <source>Hyper 2</source>
        <translation>Hyper 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1717"/>
        <source>Poor</source>
        <translation>Pauvre</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1717"/>
        <source>Hyper 3</source>
        <translation>Hyper 3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1862"/>
        <source>Scanning import plugin &quot;%1&quot; failed!

%2</source>
        <translation>l&apos;Analyse du plugin d&apos;importation «&#xa0;%1&#xa0;» a échoué&#xa0;!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1879"/>
        <location filename="../MainWindow.cpp" line="1906"/>
        <location filename="../MainWindow.cpp" line="5029"/>
        <source>Switch Language to %1</source>
        <translation>Changer la langue en %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1932"/>
        <location filename="../MainWindow.cpp" line="1947"/>
        <location filename="../MainWindow.cpp" line="5037"/>
        <source>Switch Theme to %1</source>
        <translation>Changer le thème en %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1972"/>
        <location filename="../MainWindow.cpp" line="1992"/>
        <location filename="../MainWindow.cpp" line="5045"/>
        <source>Switch Style to %1</source>
        <translation>Changer le style en %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2063"/>
        <location filename="../MainWindow.cpp" line="2088"/>
        <source>Edit record</source>
        <translation>Modifier l&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2192"/>
        <location filename="../MainWindow.cpp" line="5063"/>
        <location filename="../MainWindow.cpp" line="5067"/>
        <location filename="../MainWindow.cpp" line="5071"/>
        <location filename="../MainWindow.h" line="196"/>
        <location filename="../MainWindow.h" line="200"/>
        <location filename="../MainWindow.h" line="204"/>
        <source>Median</source>
        <translation>Médian</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2194"/>
        <source>Click to swap Average and Median</source>
        <translation>Cliquer pour échanger moyenne et médiane</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2289"/>
        <source>Click to swap Legend and Label</source>
        <translation>Cliquer pour échanger la légende et le label</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2328"/>
        <source>No records to preview for selected time range!</source>
        <translation>Aucun enregistrement à prévisualiser pour la plage de temps sélectionnée&#xa0;!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2360"/>
        <source>No records to print for selected time range!</source>
        <translation>Aucun enregistrement à imprimer pour la plage de temps sélectionnée&#xa0;!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2417"/>
        <source>User %1</source>
        <translation>Utilisateur %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3001"/>
        <source>Could not create e-mail because generating base64 for attachment &quot;%1&quot; failed!

%2</source>
        <translation>Impossible de créer un courriel car la génération de la base64 pour la pièce jointe «&#xa0;%1&#xa0;» a échoué&#xa0;!

%2</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="3205"/>
        <location filename="../MainWindow.cpp" line="4491"/>
        <source>Skipped %n hidden record(s)!</source>
        <translation>
            <numerusform>%n enregistrement caché ignoré&#x202f;!</numerusform>
            <numerusform>%n enregistrements cachés ignorés&#x202f;!</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3844"/>
        <location filename="../MainWindow.cpp" line="4645"/>
        <source>Chart</source>
        <translation>Graphique</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3844"/>
        <location filename="../MainWindow.cpp" line="4662"/>
        <source>Table</source>
        <translation>Tableau</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3844"/>
        <location filename="../MainWindow.cpp" line="4679"/>
        <source>Statistic</source>
        <translation>Statistique</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4603"/>
        <source>No records to analyse!</source>
        <translation>Aucun enregistrement à analyser&#xa0;!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4615"/>
        <source>No records to display for selected time range!</source>
        <translation>Aucun enregistrement à afficher pour une plage de temps sélectionnée&#xa0;!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4697"/>
        <source>Could not open e-mail &quot;%1&quot;!

%2</source>
        <translation>Impossible d&apos;ouvrir le courriel «&#xa0;%1&#xa0;»&#xa0;!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4707"/>
        <source>Could not start e-mail client!</source>
        <translation>Impossible de lancer le client de messagerie&#xa0;!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4712"/>
        <source>No records to e-mail for selected time range!</source>
        <translation>Aucun enregistrement vers courriel pour une plage de temps sélectionnée&#xa0;!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4718"/>
        <source>Select Icon Color</source>
        <translation>Sélectionnez la couleur de l&apos;icône</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4724"/>
        <source>Restart application to apply new icon color.</source>
        <translation>Redémarrez l&apos;application pour appliquer la nouvelle couleur de l&apos;icône.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="902"/>
        <source>The database can not be saved.

Please choose another location in the settings or all data will be lost.</source>
        <translation>La base de données ne peut pas être sauvegardée.

Veuillez choisir un autre emplacement dans les paramètres ou toutes les données seront perdues.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2412"/>
        <source>Created with UBPM for
Windows / Linux / macOS</source>
        <translation>Crée avec UBPM pour
Windows / Linux / macOS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2413"/>
        <source>Free and OpenSource
https://codeberg.org/lazyt/ubpm</source>
        <translation>Logiciel libre et gratuit
https://codeberg.org/lazyt/ubpm</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3153"/>
        <source>Import from CSV/XML/JSON/SQL</source>
        <translation>Importer depuis CSV/XML/JSON/SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3153"/>
        <source>CSV File (*.csv);;XML File (*.xml);;JSON File (*.json);;SQL File (*.sql)</source>
        <translation>Fichier CSV (*.csv);;Fichier XML (*.xml);;FichierJSON (*.json);;Fichier SQL (*.sql)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3216"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>Impossible d&apos;ouvrir «&#xa0;%1&#xa0;»&#xa0;!

Raison&#xa0;: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1222"/>
        <source>Measurements : %1  |  Irregular : %2  |  Movement : %3</source>
        <translation>Mesures&#xa0;: %1 | Irrégulier&#xa0;: %2 | Mouvement&#xa0;: %3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="682"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Cher Dr NomduPraticien,

veuillez trouver ci-joint les données de ma tension artérielle pour ce mois-ci.

Meilleures salutations,
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1227"/>
        <source>Measurements : 0  |  Irregular : 0  |  Movement : 0</source>
        <translation>Mesures : 0 | Irrégulier : 0 | Mouvement : 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2417"/>
        <source>%1
Age: %2, Height: %3 cm, Weight: %4 kg, BMI: %5</source>
        <translation>%1
Âge : %2, Taille : %3 cm, Poids : %4 kg, BMI : %5</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="3191"/>
        <location filename="../MainWindow.cpp" line="4482"/>
        <source>Successfully imported %n record(s) from %1.

     User 1 : %2
     User 2 : %3</source>
        <translation>
            <numerusform>Importation réussie de %n enregistrement depuis %1.

     Utilisateur 1 : %2
     Utilisateur 2 : %3</numerusform>
            <numerusform>Importation réussie de %n enregistrements depuis %1.

     Utilisateur 1 : %2
     Utilisateur 2 : %3</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="3195"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>Saut de %n enregistrement invalide&#xa0;!</numerusform>
            <numerusform>Saut de %n enregistrements invalides&#xa0;!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="3200"/>
        <location filename="../MainWindow.cpp" line="4486"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>Sauter %n enregistrement en double&#xa0;!</numerusform>
            <numerusform>Sauter %n enregistrements en double&#xa0;!</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3586"/>
        <source>Doesn&apos;t look like a UBPM database!

Maybe wrong encryption settings/password?</source>
        <translation>Cela ne ressemble pas à une base de données UBPM&#xa0;!

Peut-être de mauvais paramètres de cryptage/mot de passe&#xa0;?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3639"/>
        <location filename="../MainWindow.cpp" line="3643"/>
        <source>Export to %1</source>
        <translation>Exporter vers %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3639"/>
        <location filename="../MainWindow.cpp" line="3643"/>
        <source>%1 File (*.%2)</source>
        <translation>%1 Fichier (*.%2)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3679"/>
        <source>Could not create &quot;%1&quot;!

Reason: %2</source>
        <translation>Impossible de créer &quot;%1&quot; !

Motif : %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3685"/>
        <source>The database is empty, no records to export!</source>
        <translation>La base de données est vide, pas d&apos;enregistrement à exporter&#xa0;!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4052"/>
        <source>Morning</source>
        <translation>Matin</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4052"/>
        <source>Afternoon</source>
        <translation>Après-midi</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4108"/>
        <source>Week</source>
        <translation>Semaine</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4163"/>
        <source>Quarter</source>
        <translation>Trimestre</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4192"/>
        <source>Half Year</source>
        <translation>Semestre</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4210"/>
        <source>Year</source>
        <translation>Année</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4416"/>
        <source>Really delete all records for user %1?</source>
        <translation>Supprimer réellement tous les enregistrements de l&apos;utilisateur %1 ?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4431"/>
        <source>All records for user %1 deleted and existing database saved to &quot;ubpm.sql.bak&quot;.</source>
        <translation>Tous les enregistrements de l&apos;utilisateur %1 sont supprimés et la base de données existante est sauvegardée dans ubpm.sql.bak.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4570"/>
        <source>Really delete all records?</source>
        <translation>Vraiment effacer tous les enregistrements&#xa0;?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4581"/>
        <source>All records deleted and existing database &quot;ubpm.sql&quot; moved to trash.</source>
        <translation>Tous les enregistrements sont supprimés et la base de données existante ubpm.sql est mise à la poubelle.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4901"/>
        <source>- UBPM Application
</source>
        <translation>- Application UBPM
</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4916"/>
        <source>- UBPM Plugins
</source>
        <translation>- Extensions UBPM
</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5060"/>
        <location filename="../MainWindow.cpp" line="5064"/>
        <location filename="../MainWindow.cpp" line="5068"/>
        <location filename="../MainWindow.h" line="193"/>
        <location filename="../MainWindow.h" line="197"/>
        <location filename="../MainWindow.h" line="201"/>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5061"/>
        <location filename="../MainWindow.cpp" line="5065"/>
        <location filename="../MainWindow.cpp" line="5069"/>
        <location filename="../MainWindow.h" line="194"/>
        <location filename="../MainWindow.h" line="198"/>
        <location filename="../MainWindow.h" line="202"/>
        <source>Maximum</source>
        <translation>Maximum</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5134"/>
        <source>Could not open theme &quot;%1&quot; file!

Reason: %2</source>
        <translation>Impossible d&apos;ouvrir le fichier du thème «&#xa0;%1&#xa0;»&#xa0;!

Motif&#xa0;: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2060"/>
        <location filename="../MainWindow.cpp" line="2077"/>
        <location filename="../MainWindow.cpp" line="6020"/>
        <source>Delete record</source>
        <translation>Effacer l&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5947"/>
        <source>Show Heartrate</source>
        <translation>Afficher la fréquence cardiaque</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5983"/>
        <location filename="../MainWindow.cpp" line="5994"/>
        <source>Symbols and lines can&apos;t be disabled both!</source>
        <translation>Les symboles et les lignes ne peuvent pas être désactivés tous les deux&#xa0;!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="6022"/>
        <source>Show record</source>
        <translation>Afficher l&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2061"/>
        <location filename="../MainWindow.cpp" line="2084"/>
        <location filename="../MainWindow.cpp" line="6023"/>
        <source>Hide record</source>
        <translation>Masquer l&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2079"/>
        <location filename="../MainWindow.cpp" line="6034"/>
        <source>Really delete selected record?</source>
        <translation>Vraiment effacer les enregistrements sélectionnés&#xa0;?</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="5013"/>
        <source>The following %n translation(s) for &quot;%1&quot; could not be loaded:

%2
Hide this warning on program startup?</source>
        <translation>
            <numerusform>La traduction %n suivante pour «&#xa0;%1&#xa0;» n&apos;a pas pu être chargée&#xa0;:

%2
Cacher cet avertissement sur le démarrage du programme&#xa0;?</numerusform>
            <numerusform>Les traductions %n suivantes pour «&#xa0;%1&#xa0;» n&apos;a pas pu être chargées&#xa0;:

%2
Cacher cet avertissement sur le démarrage du programme&#xa0;?</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5916"/>
        <source>Scrolling has reached top/bottom, show next/previous period?</source>
        <translation>Le défilement a atteint le haut/bas, afficher la période suivante/précédente&#xa0;?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5939"/>
        <source>Dynamic Scaling</source>
        <translation>Mise à l&apos;échelle dynamique</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5940"/>
        <source>Colored Stripes</source>
        <translation>Rayures colorées</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5942"/>
        <source>Show Symbols</source>
        <translation>Afficher les symboles</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5943"/>
        <source>Show Lines</source>
        <translation>Afficher les lignes</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5945"/>
        <source>Colored Symbols</source>
        <translation>Symboles colorés</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="6055"/>
        <source>Show Median</source>
        <translation>Afficher la médiane</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="6056"/>
        <source>Show Values</source>
        <translation>Afficher les valeurs</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="6111"/>
        <source>Really quit program?</source>
        <translation>Vraiment quitter le programme&#xa0;?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../MainWindow.h" line="4"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Gestionnaire universel de la pression artérielle</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="13"/>
        <source>The application is already running and multiple instances are not allowed.

Switch to the running instance or close it and try again.</source>
        <translation>L&apos;application est déjà en cours d&apos;exécution et les instances multiples ne sont pas autorisées.

Passez à l&apos;instance en cours ou fermez-la et réessayez.</translation>
    </message>
</context>
</TS>
