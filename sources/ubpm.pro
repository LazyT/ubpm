TEMPLATE = subdirs
SUBDIRS  = plugins mainapp

mainapp.depends = plugins

appbundle.target  = appbundle
appbundle.CONFIG  = recursive
appbundle.recurse = mainapp plugins
appbundle.recurse_target = appbundle

flatpak.target  = flatpak
flatpak.CONFIG  = recursive
flatpak.recurse = mainapp plugins
flatpak.recurse_target = flatpak

snap.target  = snap
snap.CONFIG  = recursive
snap.recurse = mainapp plugins
snap.recurse_target = snap

msix.target  = msix
msix.CONFIG  = recursive
msix.recurse = mainapp plugins
msxi.recurse_target = msix

QMAKE_EXTRA_TARGETS += appbundle flatpak snap msix
